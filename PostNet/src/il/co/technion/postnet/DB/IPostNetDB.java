package il.co.technion.postnet.DB;

import java.util.Date;

import il.co.technion.postnet.Objects.Interfaces.IUser;

/**
 * 
 * PostNet database.
 *
 */
public interface IPostNetDB {

	/**
	 * @return get current time
	 */
  Date getCurrentTime();
	/**
	 * @return get current tume rounded 2 minutes up
	 */
  Date getCurrentTimeRounded2MinutesUp();

}
