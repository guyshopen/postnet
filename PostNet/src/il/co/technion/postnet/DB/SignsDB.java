package il.co.technion.postnet.DB;

import il.co.technion.postnet.Objects.Sign;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Types.SignContent;
import il.co.technion.postnet.Types.SignType;

import java.io.File;
import java.util.ArrayList;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

// This class stores the collection of signs,
// and initialises signs from 'signs.xml'

public class SignsDB implements ISignsDB {

	ArrayList<ISign> signs; // All signs
	String filepath = "signs.xml";
	private final File file;
	DocumentBuilderFactory docFactory;
	DocumentBuilder docBuilder;
	Document doc;

	public SignsDB() {

		signs = new ArrayList<ISign>();

		// Opening XML

		file = new File(filepath);
		docFactory = DocumentBuilderFactory.newInstance();
		try {
			docBuilder = docFactory.newDocumentBuilder();
			doc = docBuilder.parse(file);
		} catch (Exception e) {
			// TODO: Handle
		}
		doc.getDocumentElement().normalize();

		// Initialising DB from XML

		NodeList signNodeList = doc.getElementsByTagName("sign");

		for (int i = 0; i < signNodeList.getLength(); ++i) {

			Node signNode = signNodeList.item(i);
			Element signElement = (Element) signNode;
			String name = signElement.getAttribute("name");
			boolean active = signElement.getAttribute("active").equals("true") ? true
					: false;
			SignType type = signElement.getAttribute("type").equals("CITY") ? SignType.CITY
					: SignType.INTERCITY;
			SignContent content = null;

			switch (signElement.getAttribute("content")) {
			case "TEXT":
				content = SignContent.TEXT;
				break;
			case "PICTURE":
				content = SignContent.IMAGE;
				break;
			case "BOTH":
				content = SignContent.BOTH;
			}

			Float dayPrice = Float.parseFloat(signElement
					.getAttribute("dayPrice"));
			Float nightPrice = Float.parseFloat(signElement
					.getAttribute("nightPrice"));

			Node cityNode = signNode.getChildNodes().item(1);
			String city = cityNode.getTextContent();

			Sign sign = new Sign(name, city, type, content, dayPrice,
					nightPrice);
			signs.add(sign);
			if (active)
				sign.enable(); // Start the sign thread and mark it as active
		}
	}

	@Override
	// Adds a given sign to the DB
	public synchronized void add(ISign sign) {

		// Updating XML

		Element signElement = doc.createElement("sign");
		signElement.setAttribute("name", sign.getName());
		signElement.setAttribute("active", "true");
		signElement.setAttribute("type", sign.getType().toString());
		signElement.setAttribute("content", sign.getContent().toString());
		signElement
				.setAttribute("dayPrice", Float.toString(sign.getDayPrice()));
		signElement.setAttribute("nightPrice",
				Float.toString(sign.getNightPrice()));
		Element cityElement = doc.createElement("city");
		cityElement.appendChild(doc.createTextNode(sign.getCity()));

		signElement.appendChild(cityElement);
		Element signsElement = (Element) doc.getElementsByTagName("signs")
				.item(0);
		signsElement.appendChild(signElement);

		updateXML();

		// Updating DB

		signs.add(sign);
		sign.enable(); // Start the sign thread and mark it as active
	}

	@Override
	// Returns a sign with the given unique name
	public synchronized ISign getSignByName(String name) {
		for (int i = 0; i < signs.size(); ++i)
			if (signs.get(i).getName().equals(name))
				return signs.get(i);
		return null;
	}

	@Override
	// Finds the sign in the DB and activates it
	public synchronized void enableSignByName(String name) {

		// Find the sign in DB

		ISign sign = getSignByName(name);
		if (sign == null) {
			// Not found
			return;
		} else {

			// Change XML attribute

			NodeList signNodeList = doc.getElementsByTagName("sign");
			for (int i = 0; i < signNodeList.getLength(); ++i) {
				Node signNode = signNodeList.item(i);
				Element signElement = (Element) signNode;
				if (signElement.getAttribute("name").equals(name)) {
					signElement.setAttribute("active", "true");
					updateXML();
					sign.enable(); // Enable
					return;
				}
			}
		}
	}

	@Override
	// Finds the sign in the DB and deactivates it
	public synchronized void disableSignByName(String name) {

		// TODO: cancel all future active ads that use this sign

		// Find the sign in DB

		ISign sign = getSignByName(name);
		if (sign == null) {
			// Not found
			return;
		} else {

			// Change XML attribute

			NodeList signNodeList = doc.getElementsByTagName("sign");
			for (int i = 0; i < signNodeList.getLength(); ++i) {
				Node signNode = signNodeList.item(i);
				Element signElement = (Element) signNode;
				if (signElement.getAttribute("name").equals(name)) {
					signElement.setAttribute("active", "false");
					updateXML();
					sign.disable(); // Disable
					return;
				}
			}
		}
	}

	@Override
	// Finds the sign in the DB and removes it
	public synchronized void removeSignByName(String name) {

		// TODO: cancel all future active ads that use this sign

		// Find the sign in DB

		ISign sign = getSignByName(name);
		if (sign == null) {
			// Not found
			return;
		} else {

			// Change XML attribute

			NodeList signNodeList = doc.getElementsByTagName("sign");
			for (int i = 0; i < signNodeList.getLength(); ++i) {
				Node signNode = signNodeList.item(i);
				Element signElement = (Element) signNode;
				if (signElement.getAttribute("name").equals(name)) {
					signElement.getParentNode().removeChild(signNode);
					updateXML();
					sign.die(); // Kill thread and GUI
					signs.remove(sign); // Remove from DB
					return;
				}
			}
		}
	}

	@Override
	// Returns all signs in the DB
	public synchronized ArrayList<ISign> getSigns() {
		return signs;
	}

	@Override
	// Returns only the active signs in the DB
	public synchronized ArrayList<ISign> getActiveSigns() {
		ArrayList<ISign> activeSigns = new ArrayList<>();
		for (ISign sign : signs)
			if (sign.isActive())
				activeSigns.add(sign);
		return activeSigns;
	}

	@Override
	public ArrayList<ISign> getNotActiveSigns() {
		ArrayList<ISign> notActiveSigns = new ArrayList<>();
		for (ISign sign : signs)
			if (!sign.isActive())
				notActiveSigns.add(sign);
		return notActiveSigns;
	}

	@Override
	// Returns all of the cities that have active signs in them
	public synchronized TreeSet<String> getCities() {
		TreeSet<String> cities = new TreeSet<String>();
		for (ISign sign : getActiveSigns()) {
			cities.add(sign.getCity());
		}
		return cities;
	}

	// Updates the XML in 'filepath' according to 'doc'
	private synchronized void updateXML() {

		TransformerFactory transformerFactory = TransformerFactory
				.newInstance();
		Transformer transformer = null;
		try {
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		}
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(filepath));
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "signs.dtd");
		try {
			transformer.transform(source, result);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setDayPriceByName(String name, Float price) {

		// Find the sign in DB

		ISign sign = getSignByName(name);
		if (sign == null) {
			// Not found
			return;
		} else {

			// Change XML attribute

			NodeList signNodeList = doc.getElementsByTagName("sign");
			for (int i = 0; i < signNodeList.getLength(); ++i) {
				Node signNode = signNodeList.item(i);
				Element signElement = (Element) signNode;
				if (signElement.getAttribute("name").equals(name)) {
					signElement.setAttribute("dayPrice", Float.toString(price));
					updateXML();
					sign.setDayPrice(price);
					return;
				}
			}
		}

	}

	@Override
	public void setNightPriceByName(String name, Float price) {

		// Find the sign in DB

		ISign sign = getSignByName(name);
		if (sign == null) {
			// Not found
			return;
		} else {

			// Change XML attribute

			NodeList signNodeList = doc.getElementsByTagName("sign");
			for (int i = 0; i < signNodeList.getLength(); ++i) {
				Node signNode = signNodeList.item(i);
				Element signElement = (Element) signNode;
				if (signElement.getAttribute("name").equals(name)) {
					signElement.setAttribute("nightPrice",
							Float.toString(price));
					updateXML();
					sign.setNightPrice(price);
					return;
				}
			}
		}

	}
}
