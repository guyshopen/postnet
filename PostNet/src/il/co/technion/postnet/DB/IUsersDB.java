package il.co.technion.postnet.DB;

import java.util.ArrayList;
import java.util.Calendar;

import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IOfficialAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Objects.Interfaces.IUser;

/**
 * 
 * User data base. It allows to add and remove users.
 * To view adds and advertisors.
 */
public interface IUsersDB {
/**
 *  @param user adds user
 */
	public abstract void addUser(IUser user);
	/**
	 *  @param user removes user
	 */
  public abstract void removeUser(String name);
  /**
   *  @param name gets user by name
   */
	public abstract IUser getUser(String name);
	/**
	 *  @return returns all users
	 */
	public abstract ArrayList<IUser> getUsers();
	/**
	 *  @return returns all active users
	 */
	public abstract ArrayList<IAd> getAllActiveAds();
	/**
	 * @return returns advirtasors
	 */
  public abstract ArrayList<IAdvertiser> getAdvertisers();
  /**
   * @return returns official advertisors
   */
  public abstract ArrayList<IOfficialAdvertiser> getOfficialAdvertisers();

}
