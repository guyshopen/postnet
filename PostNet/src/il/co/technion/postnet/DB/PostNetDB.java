package il.co.technion.postnet.DB;

import java.util.Calendar;
import java.util.Date;

import il.co.technion.postnet.Objects.Interfaces.IAdmin;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.ICommercialAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IOfficialAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.ISalesAdmin;
import il.co.technion.postnet.Objects.Interfaces.ISignsAdmin;
import il.co.technion.postnet.Objects.Interfaces.IUser;

/**
 * 
 * post net database
 * 
 */
public class PostNetDB implements IPostNetDB {

	private static ISignsDB signsDB;
	private static IUsersDB usersDB;
	//private static IUser currentUser;

	static Float defaultDayPrice = (float) 100;
	static Float defaultNightPrice = (float) 100;

	public PostNetDB(ISignsDB signsDB, IUsersDB usersDB) {
		PostNetDB.signsDB = signsDB;
		PostNetDB.usersDB = usersDB;
	}

	/**
	 * @return sign data base
	 */
	public static ISignsDB getSignsDB() {
		return signsDB;
	}

	/**
	 * @return user data base
	 */
	public static IUsersDB getUsersDB() {
		return usersDB;
	}

//	/**
//	 * @param user
//	 *            set current user
//	 */
//	public static void setCurrentUser(IUser user) {
//		currentUser = user;
//	}

//	/**
//	 * @return get current user
//	 */
//	public static IUser getCurrentUser() {
//		return currentUser;
//	}

	@Override
	public Date getCurrentTime() {
		return Calendar.getInstance().getTime();
	}

	@Override
	public Date getCurrentTimeRounded2MinutesUp() {
		Date time = getCurrentTime();
		time.setTime(((time.getTime() / 120000) * 120000) + 120000);
		return time;
	}
	/**
	 * @return default day price
	 */
	public static Float getDefaultDayPrice() {
		return defaultDayPrice;
	}
	/**
	 * @param price sets default day price
	 */
	public static Float getDefaultNightPrice() {
		return defaultNightPrice;
	}
	/**
	 * @return default night price
	 */
	public static void setDefaultDayPrice(Float price) {
		defaultDayPrice = price;
	}
	/**
	 * @param price sets default night price
	 */
	public static void setDefaultNightPrice(Float price) {
		defaultNightPrice = price;
	}

}
