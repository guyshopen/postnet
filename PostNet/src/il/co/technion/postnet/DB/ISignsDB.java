package il.co.technion.postnet.DB;

import il.co.technion.postnet.Objects.Sign;
import il.co.technion.postnet.Objects.Interfaces.ISign;

import java.util.ArrayList;
import java.util.TreeSet;

/**
 * 
 * Sign data base. Add or remove signs, change their status, set prices.
 *
 */
public interface ISignsDB {
/**
 * @return returns signs
 */
	ArrayList<ISign> getSigns();
/**
 *  @return returns active signs
 */
	ArrayList<ISign> getActiveSigns();
/**
 * returns sign by name
 *  @param name
 *  @return sign by name
 */
	ISign getSignByName(String name);
/**
 * enables sign by name
 *  @param name
 */
	void enableSignByName(String name);
	/**
	 * disables sign by name
	 *  @param name
	 */
	void disableSignByName(String name);
	/**
	 * removes sign by name
	 *  @param name
	 */
	void removeSignByName(String name);
	/**
	 * set sign day price by name
	 *  @param name
	 *  @param price
	 */
	void setDayPriceByName(String name, Float price);
	/**
	 * set sign night price by name
	 *  @param name
	 *  @param price
	 */
	void setNightPriceByName(String name, Float price);
	/**
	 * @return cities
	 */
	TreeSet<String> getCities();
/**
 * @param sign adds sign
 */
	void add(ISign sign);
/**
  * @return gets not active signs
 */
	ArrayList<ISign> getNotActiveSigns();

}
