// another test

package il.co.technion.postnet;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.ISignsDB;
import il.co.technion.postnet.DB.IUsersDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.DB.SignsDB;
import il.co.technion.postnet.DB.UsersDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.GUI.PostNetGUI;
import il.co.technion.postnet.Maintenance.IPostNetMaintenance;
import il.co.technion.postnet.Maintenance.PostNetMaintenance;
import il.co.technion.postnet.Objects.CommercialAdvertiser;
import il.co.technion.postnet.Objects.MainComputer;
import il.co.technion.postnet.Objects.OfficialAdvertiser;
import il.co.technion.postnet.Objects.PeriodicAd;
import il.co.technion.postnet.Objects.SalesAdmin;
import il.co.technion.postnet.Objects.SignsAdmin;
import il.co.technion.postnet.Objects.SingleAd;
import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IMainComputer;
import il.co.technion.postnet.Objects.Interfaces.ISalesAdmin;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Objects.Interfaces.ISignsAdmin;
import il.co.technion.postnet.Objects.Interfaces.IUser;

import javax.swing.JFrame;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * 
 * PostNnet class holds the postNet database, GUI and maintenance
 * 
 */
public class PostNet {

	/**
	 * postnet data base
	 */
	static IPostNetDB postNetDB;
	/**
	 * postnet maintanance class
	 */
	static IPostNetMaintenance postNetMaintenance;
	/**
	 * postnet GUI
	 */
	static IPostNetGUI postNetGUI;
	/**
	 * The signs administrator of the PostNet system
	 */
	static ISignsAdmin signsAdmin;
	/**
	 * The sales administrator of the PostNet system
	 */
	static ISalesAdmin salesAdmin;
	/**
	 * The main computer of the PostNet system
	 */
	static IMainComputer mainComputer;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		ISignsDB signsDB = new SignsDB();
		IUsersDB usersDB = new UsersDB();
		postNetDB = new PostNetDB(signsDB, usersDB);
		postNetMaintenance = new PostNetMaintenance(postNetDB);
		mainComputer = new MainComputer(usersDB, signsDB);
		CreateAdmins();
		postNetGUI = new PostNetGUI(postNetDB, postNetMaintenance,
				new JFrame(), signsAdmin, salesAdmin, mainComputer);

		CreateUsersForTesting(); // TODO: Delete before submit!

		postNetMaintenance.initialize();
		postNetGUI.initialize();

		boolean[] days = { false, false, true, false, false, true, false };
		Calendar startDate = new GregorianCalendar(2014, Calendar.JANUARY, 21,
				0, 0);
		Calendar startTime = new GregorianCalendar(0, 0, 0, 4, 10);
		Calendar endTime = new GregorianCalendar(0, 0, 0, 4, 20);
		PeriodicAd test = new PeriodicAd("test", new File("ad1.jpg"), PostNetDB
				.getSignsDB().getActiveSigns(), startDate, startTime, endTime,
				days, 3, false);
		for (Calendar c : test.getTimesToShow()) {
			// System.out.println(c);
		}

	}

	/***************************************************************************/

	/**
	 * The method which creates two admins for the system. Sales admin and Signs
	 * admin.
	 */
	static private void CreateAdmins() {
		IUsersDB usersDB = PostNetDB.getUsersDB();

		String salesAdminName = "salesAdmin";
		char[] salesAdminPassword = new char[0];
		salesAdmin = new SalesAdmin(salesAdminName, salesAdminPassword);
		usersDB.addUser((IUser) salesAdmin);

		String signsAdminName = "signsAdmin";
		char[] signsAdminPassword = new char[0];
		signsAdmin = new SignsAdmin(signsAdminName, signsAdminPassword);
		usersDB.addUser((IUser) signsAdmin);
	}

	/** Create a user with some ads just for easy access to the system **/
	static// TODO: DELETE BEFORE SUBMIT !!!
	void CreateUsersForTesting() {
		char[] tmpPassword = new char[0];
		File tmpFile = new File("ad1.jpg");
		ArrayList<ISign> tmpSigns = new ArrayList<>();
		tmpSigns.addAll(PostNetDB.getSignsDB().getActiveSigns());
		Calendar timeToShow = new GregorianCalendar(2014, Calendar.JANUARY, 16,
				14, 50);
		IAd tmpAd = new SingleAd("ad1", tmpFile, tmpSigns, timeToShow, false);

		IUser tmpCommercialAdvertiser = new CommercialAdvertiser("me",
				tmpPassword, "commercialCompany", "commercialAccount");
		((IAdvertiser) tmpCommercialAdvertiser).addAd(tmpAd);
		PostNetDB.getUsersDB().addUser(tmpCommercialAdvertiser);

		IUser tmpOfficialAdvertiser = new OfficialAdvertiser("o", tmpPassword,
				"officialCompany", "officialAccount");
		PostNetDB.getUsersDB().addUser(tmpOfficialAdvertiser);
	}

}
