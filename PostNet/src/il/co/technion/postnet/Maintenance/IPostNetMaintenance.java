package il.co.technion.postnet.Maintenance;

import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.ISign;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * 
 * class used for system maintananace
 *
 */
public interface IPostNetMaintenance {
	public void handleUnexpectedAds();
	void initialize();

	ArrayList<IAd> getAdList(ISign sign, Calendar time);

}
