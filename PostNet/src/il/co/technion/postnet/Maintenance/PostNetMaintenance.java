// TODO:
// handle unexpected ads separately (scan all the time and pushunexpected, don't send them with update)
// handle official constant ads
// display only active ads (but update stas for both active and inactive)
// only use active signs

package il.co.technion.postnet.Maintenance;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.IUsersDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IMainComputer;
import il.co.technion.postnet.Objects.Interfaces.IPeriodicAd;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Objects.Interfaces.ISingleAd;
import il.co.technion.postnet.Objects.Interfaces.IUnexpectedAd;
import il.co.technion.postnet.Objects.Interfaces.IUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

// This class will maintain the signs by updating their ad lists every 2 minutes
// and getting statistics every 4 minutes

public class PostNetMaintenance implements IPostNetMaintenance, Runnable

{

	private IPostNetDB db; // The main DB
	private IUsersDB usersDB;
	private final Thread thread; // The maintenance thread
	private boolean started; // Has the thread started

	private Calendar lastUpdate; // Time of last ad lists update
	private Calendar lastStatistics; // Time of last statistics requests

	public PostNetMaintenance(IPostNetDB db) {
		this.db = db;
		thread = new Thread(this);
		started = false;
		lastUpdate = null;
		lastStatistics = null;
		usersDB = PostNetDB.getUsersDB();
	}
	public PostNetMaintenance(IPostNetDB db, IUsersDB userDatabase) {
		this.db = db;
		thread = new Thread(this);
		started = false;
		lastUpdate = null;
		lastStatistics = null;
		usersDB = userDatabase;
	}
	@Override
	// Starts the maintenance thread
	public synchronized void initialize() {
		if (started)
			return;
		started = true;
		thread.start();
	}

	@Override
	// Maintenance thread method
	public void run() {
		while (true) {

			handleUnexpectedAds();

			if (isTimeToUpdateSigns())
				updateSigns();

			if (isTimeToGetStatistics())
				getStatistics();

		}
	}

	// Checks if it's time to update signs' ads (5 seconds before 2 minutes)
	private boolean isTimeToUpdateSigns() {
		Calendar c = Calendar.getInstance();
		if ((c.get(Calendar.MINUTE) % 2 == 1) && (c.get(Calendar.SECOND) == 55)
				&& (differentTime(c, lastUpdate))) {
			lastUpdate = c; // Timestamp
			return true;
		}
		return false;
	}

	// Checks if it's time to get signs' statistics (5 seconds past 4 minutes)
	private boolean isTimeToGetStatistics() {
		Calendar c = Calendar.getInstance();
		if ((c.get(Calendar.MINUTE) % 4 == 0) && (c.get(Calendar.SECOND) == 5)
				&& (differentTime(c, lastStatistics))) {
			lastStatistics = c; // Timestamp
			return true;
		}
		return false;
	}

	// Sends all active signs their current display list
	private synchronized void updateSigns() {

		// Scan all signs

		for (ISign sign : PostNetDB.getSignsDB().getActiveSigns()) {
			sign.updateAdList(getAdList(sign, nextCycle()));
		}

		return;
	}

	// Asks all signs for statistics and updates ads' information
	private synchronized void getStatistics() {

		// Get report from each active sign
		for (ISign sign : PostNetDB.getSignsDB().getActiveSigns()) {
			HashMap<String, Integer> report = sign.reportStatistics();

			// For each ad the sign has displayed
			for (String adName : report.keySet()) {
				int viewed = report.get(adName); // Times displayed
				// Find the user of the ad
				for (IUser user : PostNetDB.getUsersDB().getUsers()) {
					if ((user instanceof IAdvertiser)
							&& (((IAdvertiser) user).getAd(adName) != null)) {

						/*************************/

						// Ad found

						IAd ad = ((IAdvertiser) user).getAd(adName);

						if (isItDayTime()) {
							// Ad was displayed during the day
							try {
								ad.setDayTimeDispalyed(ad.getDayTimeDispalyed()
										+ viewed);
								ad.setCost(ad.getCost()
										+ (sign.getDayPrice() * viewed));
							} catch (Exception e) {
							}

						} else {
							// Ad was displayed during the night
							try {
								ad.setNightTimeDispalyed(ad
										.getNightTimeDispalyed() + viewed);
								ad.setCost(ad.getCost()
										+ (sign.getNightPrice() * viewed));
							} catch (Exception e) {
							}
						}

						break; // User found, ad updated

						/*************************/

					}
				}
			}
		}
	}

	// Scans and handles unexpected ads
	public synchronized void handleUnexpectedAds() {

		// Scan DB for unsent unexpected ads

		for (IAd ad : usersDB.getAllActiveAds()) {
			if (ad instanceof IUnexpectedAd) {

				// Unsent, active unexpected ad found. Sending

				for (ISign sign : ((IUnexpectedAd) ad).getSigns()) {
					// Send it to all related signs
					sign.pushUnexpactedAd(ad);
				}

				// Disable right away so that the ad won't show again
				((IUnexpectedAd) ad).setNotActive();
			}
		}
	}

	// Checks if two date objects indicate the same time (seconds resolution)
	public static boolean differentTime(Calendar a, Calendar b) {
		if ((a == null) || (b == null))
			return true;
		return (!((a.get(Calendar.YEAR) == b.get(Calendar.YEAR))
				&& (a.get(Calendar.MONTH) == b.get(Calendar.MONTH))
				&& (a.get(Calendar.DAY_OF_MONTH) == b
						.get(Calendar.DAY_OF_MONTH))
				&& (a.get(Calendar.HOUR_OF_DAY) == b.get(Calendar.HOUR_OF_DAY))
				&& (a.get(Calendar.MINUTE) == b.get(Calendar.MINUTE)) && (a
					.get(Calendar.SECOND) == b.get(Calendar.SECOND))));
	}

	@Override
	// Returns the list of ads that should be displayed on 'sign' in the cycle
	public synchronized ArrayList<IAd> getAdList(ISign sign, Calendar time) {

		ArrayList<IAd> ads = new ArrayList<IAd>(); // Ad list

		for (IAd ad : usersDB.getAllActiveAds()) {

			if ((ad.getSigns().contains(sign)) && (match(ad, time))) {

				// Check if the ad is constant or not (for officials)

				if ((ad instanceof ISingleAd) && ((ISingleAd) ad).isConstant()) {

					// Constant single ad

					for (int i = 0; i < 12; ++i) {
						ads.add(ad); // 12 cycles of 10 seconds = 2 minutes
					}

					return ads;

				}

				else if ((ad instanceof IPeriodicAd)
						&& ((IPeriodicAd) ad).isConstant()) {

					// Constant periodic ad

					for (int i = 0; i < 12; ++i) {
						ads.add(ad); // 12 cycles of 10 seconds = 2 minutes
					}

					return ads;

				}

				else if ((ad instanceof ISingleAd)
						|| (ad instanceof IPeriodicAd)) {

					// Other, non-immediate ad

					ads.add(ad); // Will appear once

				}
			}
		}
		return ads;
	}

	// Checks if it was 6AM - 6PM 2 minutes ago
	private boolean isItDayTime() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MINUTE, -2);
		return ((c.get(Calendar.HOUR_OF_DAY) < 18) && (c
				.get(Calendar.HOUR_OF_DAY) >= 6));
	}

	// Calculate the next 2 minute cycle from now
	private Calendar nextCycle() {

		Calendar now = Calendar.getInstance();
		Calendar nextCycle = new GregorianCalendar(now.get(Calendar.YEAR),
				now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH),
				now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE));

		// Round to the next 2 minutes

		int unroundedMinutes = nextCycle.get(Calendar.MINUTE);
		int mod = unroundedMinutes % 2;
		nextCycle.add(Calendar.MINUTE, (2 - mod));

		return nextCycle;
	}

	// Checks if an ad should be in the next cycle
	private boolean match(IAd ad, Calendar time) {

		if (ad instanceof IUnexpectedAd) {
			return false; // Unexpected ads are handled differently
		}

		else if (ad instanceof ISingleAd) {

			Calendar adTime = ((ISingleAd) ad).getTimeToShow();

			if (differentTime(time, adTime)) {
				return false; // Other block
			} else
				return true; // Same block

		}

		else if (ad instanceof IPeriodicAd) {

			ArrayList<Calendar> adTimes = ((IPeriodicAd) ad).getTimesToShow();

			for (Calendar adTime : adTimes) {
				if ((differentTime(time, adTime)) == false) {
					// / Same block
					return true;
				}
			}
		}

		return false;
	}
}
