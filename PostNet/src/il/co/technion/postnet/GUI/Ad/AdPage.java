package il.co.technion.postnet.GUI.Ad;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.TreeSet;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Maintenance.IPostNetMaintenance;
import il.co.technion.postnet.Objects.MainComputer;
import il.co.technion.postnet.Objects.Sign;
import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IMainComputer;
import il.co.technion.postnet.Objects.Interfaces.IPeriodicAd;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Objects.Interfaces.ISingleAd;
import il.co.technion.postnet.Types.AdType;
import il.co.technion.postnet.Types.SignType;
import il.co.technion.postnet.Types.UserType;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JCheckBox;

public class AdPage implements IAdPage {

  private IPostNetDB postNetDB;
  IPostNetGUI postNetGUI;
  IPostNetMaintenance postNetMaintenance;

  private JTextField txtfldAdName;
  private JRadioButton rdbtnPeriodicAd;
  private JRadioButton rdbtnSingleAd;
  private JRadioButton rdbtnUnexpectedAd;
  private JSpinner spnrSingleAdDateAndTime;
  private JSpinner spnrPeriodicAdStartDate;
  private JSpinner spnrPeriodicAdStartTime;
  private JSpinner spnrPeriodicAdEndTime; 
  private JSpinner spnrWeeks;
  private JTable tblSigns;
  private JTable tblDays;
  JCheckBox chckbxSingleAdConstant;
  JCheckBox chckbxPeriodicAdConstant;
  DefaultComboBoxModel<String> defaultComboBoxModel;
  final JFileChooser fc = new JFileChooser();
  JLabel lblFileSelected;
  File file = null;
  JLabel lblSimulate;
  IMainComputer mainComputer;
  
  public enum Mode {NEW, EDIT}
  Mode mode;
  IAd selectedAdToEdit;
  
  public AdPage(IPostNetDB postNetDB, IPostNetGUI postNetGUI, IPostNetMaintenance postNetMaintenance, IMainComputer mainComputer) {
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
    this.postNetMaintenance = postNetMaintenance;
    this.mainComputer = mainComputer;
    initPage(postNetGUI.getFrmPostnet());
  }

  private void initPage(JFrame frmPostnet) {

    JPanel pnlNewAd = new JPanel();
    frmPostnet.getContentPane().add(pnlNewAd, IPostNetGUI.AD);
    pnlNewAd.setLayout(null);

    JPanel pnlAdNameAndFile = new JPanel();
    pnlAdNameAndFile.setBounds(73, 296, 540, 23);
    pnlNewAd.add(pnlAdNameAndFile);
    pnlAdNameAndFile.setLayout(null);

    JLabel lblAdName = new JLabel("Ad name (unique):");
    lblAdName.setBounds(0, 4, 113, 14);
    pnlAdNameAndFile.add(lblAdName);

    // Ad name
    txtfldAdName = new JTextField();
    txtfldAdName.setBounds(123, 1, 161, 20);
    pnlAdNameAndFile.add(txtfldAdName);
    txtfldAdName.setColumns(10);

    JButton btnBrowse = new JButton("Browse");
    btnBrowse.setBounds(322, 0, 101, 23);
    pnlAdNameAndFile.add(btnBrowse);

    lblFileSelected = new JLabel("No file selected");
    lblFileSelected.setHorizontalAlignment(SwingConstants.RIGHT);
    lblFileSelected.setBounds(400, 4, 140, 14);
    pnlAdNameAndFile.add(lblFileSelected);
    
    lblSimulate = new JLabel(Sign.defaultAd);

    btnBrowse.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        int returnVal = fc.showOpenDialog(null);

       if (returnVal == JFileChooser.APPROVE_OPTION) {
          file = fc.getSelectedFile();
          // Checking extension

          int i = file.getName().lastIndexOf('.');
          if ((file.exists())
              && (i > 0)
              && ((file.getName().substring(i + 1).equals("txt")) || (file
                  .getName().substring(i + 1).equals("jpg")))) {
            lblFileSelected.setText(file.getName());
            
            if (file.getName().substring(i+1).equals("txt")) {
                lblSimulate.setText(Sign.readFile(file.getAbsolutePath()));
                lblSimulate.setIcon(null);
            }
            else {
                lblSimulate.setText("");
                lblSimulate.setIcon(new ImageIcon(file.getAbsolutePath()));
            }
            
          } else {
            file = null;
            lblFileSelected.setText("No file selected");
            postNetGUI.showMessage("Please select .txt or .jpg files only");
            
            lblSimulate.setText(Sign.defaultAd);
            lblSimulate.setIcon(null);
          }
        } else {
          file = null;
          lblFileSelected.setText("No file selected");
        }
      }
    });

    JButton btnCancel = new JButton("Cancel");
    btnCancel.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        clearForm();
        postNetGUI.switchPanel(IPostNetGUI.ADVERTISER_HOME);
        lblSimulate.setText(Sign.defaultAd);
        lblSimulate.setIcon(null);
      }
    });
    
    btnCancel.setBounds(598, 11, 76, 23);
    pnlNewAd.add(btnCancel);

    JPanel pnlSigns = new JPanel();
    pnlSigns.setBounds(73, 55, 542, 207);
    pnlNewAd.add(pnlSigns);
    pnlSigns.setLayout(null);

    JScrollPane scrlpnSignsTable = new JScrollPane();
    scrlpnSignsTable.setBounds(0, 40, 542, 167);
    pnlSigns.add(scrlpnSignsTable);

    tblSigns = new JTable();
    tblSigns.setFillsViewportHeight(true);
    tblSigns.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    tblSigns.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
        "Sign name", "City", "Type", "Text", "Images", "Day price",
    "Night price" }) {
      Class[] columnTypes = new Class[] { String.class, String.class,
          SignType.class, Boolean.class, Boolean.class, int.class,
          int.class };

      @Override
      public Class getColumnClass(int columnIndex) {
        return columnTypes[columnIndex];
      }

      boolean[] columnEditables = new boolean[] { false, false, false,
          false, false, false, false };

      @Override
      public boolean isCellEditable(int row, int column) {
        return columnEditables[column];
      }
    });

    tblSigns.getColumnModel().getColumn(0).setResizable(false);
    tblSigns.getColumnModel().getColumn(1).setResizable(false);
    tblSigns.getColumnModel().getColumn(2).setResizable(false);
    tblSigns.getColumnModel().getColumn(3).setResizable(false);
    tblSigns.getColumnModel().getColumn(4).setResizable(false);
    tblSigns.getColumnModel().getColumn(5).setResizable(false);
    tblSigns.getColumnModel().getColumn(6).setResizable(false);
    scrlpnSignsTable.setViewportView(tblSigns);

    JButton btnSelectAllSignsInArea = new JButton("Select all signs in area");
    btnSelectAllSignsInArea.setBounds(379, 0, 163, 23);
    pnlSigns.add(btnSelectAllSignsInArea);

    btnSelectAllSignsInArea.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        selectAllSignsInArea();
      }
    });

    JComboBox<String> cmbbxCities = new JComboBox<>();
    cmbbxCities.setBounds(0, 0, 369, 23);
    pnlSigns.add(cmbbxCities);

    defaultComboBoxModel = new DefaultComboBoxModel<>();
    // Set listener for area selection. (Guy)
    defaultComboBoxModel.addListDataListener(new ListDataListener() {
      @Override
      public void intervalRemoved(ListDataEvent e) {}
      @Override
      public void intervalAdded(ListDataEvent e) {}
      @Override
      public void contentsChanged(ListDataEvent e) {
        addSignsFromCityToTable(
            (String)defaultComboBoxModel.getSelectedItem(),tblSigns); // (Guy)
      }
    });

    cmbbxCities.setModel(defaultComboBoxModel);

    JPanel pnlSingleAd = new JPanel();
    pnlSingleAd.setBounds(73, 355, 540, 23);
    pnlNewAd.add(pnlSingleAd);
    pnlSingleAd.setLayout(null);

    rdbtnSingleAd = new JRadioButton("Single ad:");
    rdbtnSingleAd.setBounds(0, 0, 109, 23);
    rdbtnSingleAd.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        rdbtnPeriodicAd.setSelected(false);
        rdbtnUnexpectedAd.setSelected(false);
      }
    });
    pnlSingleAd.add(rdbtnSingleAd);
    
    spnrSingleAdDateAndTime = new JSpinner();
    spnrSingleAdDateAndTime.setBounds(160, 1, 155, 20);
    spnrSingleAdDateAndTime.setForeground(Color.WHITE);
    spnrSingleAdDateAndTime.setBackground(Color.WHITE);
    pnlSingleAd.add(spnrSingleAdDateAndTime);
    
    chckbxSingleAdConstant = new JCheckBox("Full 2 Minutes");
    chckbxSingleAdConstant.setBounds(341, -1, 199, 25);
    pnlSingleAd.add(chckbxSingleAdConstant);

    JSeparator sprtr1 = new JSeparator();
    sprtr1.setBounds(73, 399, 538, 2);
    pnlNewAd.add(sprtr1);

    JPanel pnlPeriodicAd = new JPanel();
    pnlPeriodicAd.setBounds(73, 424, 540, 135);
    pnlNewAd.add(pnlPeriodicAd);
    pnlPeriodicAd.setLayout(null);

    rdbtnPeriodicAd = new JRadioButton("Periodic ad:");
    rdbtnPeriodicAd.setBounds(0, 0, 109, 23);
    rdbtnPeriodicAd.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        rdbtnSingleAd.setSelected(false);
        rdbtnUnexpectedAd.setSelected(false);
      }
    });
    pnlPeriodicAd.add(rdbtnPeriodicAd);

    JScrollPane scrlpnDays = new JScrollPane();
    scrlpnDays.setBounds(0, 56, 540, 48);
    pnlPeriodicAd.add(scrlpnDays);
    scrlpnDays.setViewportBorder(null);
    scrlpnDays.setBorder(javax.swing.BorderFactory.createEmptyBorder());

    tblDays = new JTable();
    tblDays.setRowSelectionAllowed(false);
    tblDays.setShowVerticalLines(false);
    tblDays.setShowHorizontalLines(false);
    tblDays.setShowGrid(false);
    tblDays.setBorder(null);
    tblDays.setModel(new DefaultTableModel(new Object[][] { { null, null,
      null, null, null, null, null }, }, new String[] { "Sun", "Mon",
        "Tue", "Wed", "Thu", "Fri", "Sat" }) {
      Class[] columnTypes = new Class[] { Boolean.class, Boolean.class,
          Boolean.class, Boolean.class, Boolean.class, Boolean.class,
          Boolean.class };

      @Override
      public Class getColumnClass(int columnIndex) {
        return columnTypes[columnIndex];
      }
    });
    tblDays.getColumnModel().getColumn(0).setResizable(false);
    tblDays.getColumnModel().getColumn(1).setResizable(false);
    tblDays.getColumnModel().getColumn(2).setResizable(false);
    tblDays.getColumnModel().getColumn(3).setResizable(false);
    tblDays.getColumnModel().getColumn(4).setResizable(false);
    tblDays.getColumnModel().getColumn(5).setResizable(false);
    tblDays.getColumnModel().getColumn(6).setResizable(false);
    scrlpnDays.setViewportView(tblDays);

    spnrPeriodicAdStartDate = new JSpinner();
    spnrPeriodicAdStartDate.setBounds(173, 23, 76, 20);
    spnrPeriodicAdStartDate.setForeground(Color.WHITE);
    spnrPeriodicAdStartDate.setBackground(Color.WHITE);
    pnlPeriodicAd.add(spnrPeriodicAdStartDate);

    JLabel lblNumberOfWeeks = new JLabel(
        "Number of weeks to repeat the ad:");
    lblNumberOfWeeks.setBounds(10, 117, 210, 14);
    pnlPeriodicAd.add(lblNumberOfWeeks);

    spnrWeeks = new JSpinner();
    spnrWeeks.setModel(new SpinnerNumberModel(new Integer(1),
        new Integer(0), null, new Integer(1)));
    spnrWeeks.setBounds(464, 114, 76, 20);
    ((JSpinner.DefaultEditor) spnrWeeks.getEditor()).getTextField()
    .setEditable(false);
    pnlPeriodicAd.add(spnrWeeks);
    
    JLabel label = new JLabel("between");
    label.setBounds(274, 27, 49, 16);
    pnlPeriodicAd.add(label);
    
    spnrPeriodicAdStartTime = new JSpinner();
    spnrPeriodicAdStartTime.setForeground(Color.WHITE);
    spnrPeriodicAdStartTime.setBackground(Color.WHITE);
    spnrPeriodicAdStartTime.setBounds(335, 23, 76, 20);
    pnlPeriodicAd.add(spnrPeriodicAdStartTime);
    
    JLabel label_1 = new JLabel("to");
    label_1.setBounds(423, 27, 23, 16);
    pnlPeriodicAd.add(label_1);
    
    spnrPeriodicAdEndTime = new JSpinner();
    spnrPeriodicAdEndTime.setForeground(Color.WHITE);
    spnrPeriodicAdEndTime.setBackground(Color.WHITE);
    spnrPeriodicAdEndTime.setBounds(449, 23, 76, 20);
    pnlPeriodicAd.add(spnrPeriodicAdEndTime);
    
    JLabel lblStartOn = new JLabel("start on");
    lblStartOn.setBounds(112, 27, 49, 16);
    pnlPeriodicAd.add(lblStartOn);
    
    chckbxPeriodicAdConstant = new JCheckBox("Full time");
    chckbxPeriodicAdConstant.setBounds(449, -1, 83, 25);
    pnlPeriodicAd.add(chckbxPeriodicAdConstant);

    JSeparator sprtr2 = new JSeparator();
    sprtr2.setBounds(73, 580, 538, 2);
    pnlNewAd.add(sprtr2);

    rdbtnUnexpectedAd = new JRadioButton("Immediate ad");
    rdbtnUnexpectedAd.setBounds(73, 616, 203, 23);
    rdbtnUnexpectedAd.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        rdbtnSingleAd.setSelected(false);
        rdbtnPeriodicAd.setSelected(false);
      }
    });
    pnlNewAd.add(rdbtnUnexpectedAd);

    JButton btnSaveAd = new JButton("Save ad");
    btnSaveAd.setBounds(506, 673, 107, 23);
    pnlNewAd.add(btnSaveAd);

    btnSaveAd.addActionListener(new AdPageSaveAdActionListener(this, postNetDB, postNetGUI, postNetMaintenance, mainComputer));

    JButton btnSimulation = new JButton("Simulation");

		btnSimulation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				postNetGUI.switchPanel(IPostNetGUI.SIMULATE);

			}
		});
    
    
    btnSimulation.setBounds(73, 673, 107, 23);
    pnlNewAd.add(btnSimulation);
    ((JSpinner.DefaultEditor) spnrPeriodicAdStartDate.getEditor()).getTextField()
    .setEditable(false);

    JPanel simulate = new JPanel();
    simulate.setLayout(null);
    frmPostnet.getContentPane().add(simulate, "simulate");

    JButton button_12 = new JButton("OK");
    button_12.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(IPostNetGUI.NEW_AD);
      }
    });
    button_12.setBounds(598, 11, 76, 23);
    simulate.add(button_12);

    JPanel panel_15 = new JPanel();
    panel_15.setLayout(null);
    panel_15.setBorder(new LineBorder(new Color(0, 0, 0), 5));
    panel_15.setBackground(Color.BLACK);
    panel_15.setBounds(4, 40, 676, 667);
    simulate.add(panel_15);

    lblSimulate.setHorizontalAlignment(SwingConstants.CENTER);
    lblSimulate.setForeground(Color.WHITE);
    lblSimulate.setFont(new Font("Tahoma", Font.BOLD, 30));
    lblSimulate.setBackground(Color.BLACK);
    lblSimulate.setBounds(6, 16, 664, 655);
    panel_15.add(lblSimulate);

  }
  
  
  // On selecting a different area from the combo box (new_ad page):
  // Adding all signs from the selected area to a given table. (Guy)
  private void addSignsFromCityToTable(String city, JTable table) {
    postNetGUI.clearTable(table);
    ArrayList<ISign> signs = PostNetDB.getSignsDB().getActiveSigns();
    if (city.equals("Everywhere")) postNetGUI.addAllActiveSignsToTable(table);
    else for (int i = 0; i < signs.size(); ++i)
      if (signs.get(i).getCity().equals(city))
        postNetGUI.addSignRowToTable(signs.get(i), table); // (Guy)
  }

  // On pressing the select all signs in area button (new_ad page):
  // Select all the signs in the table for selected area. (Guy)
  private void selectAllSignsInArea() {
    tblSigns.selectAll();
  }

  //Adding all cities to combo box. (Guy)
  private void setUpCitiesComboBox() {
    defaultComboBoxModel.removeAllElements();
    defaultComboBoxModel.addElement("Everywhere");
    TreeSet<String> locations = PostNetDB.getSignsDB().getCities();
    Iterator<String> locationsIterator = locations.iterator();
    while( locationsIterator.hasNext())
      defaultComboBoxModel.addElement(locationsIterator.next());
  }
  
  private void setUpNewAdTimeSpinners() {
    Date currentTimeRounded2MinutesUp = postNetDB.getCurrentTimeRounded2MinutesUp();
    Date currentTime = postNetDB.getCurrentTime();
    
    spnrSingleAdDateAndTime.setModel(new SpinnerDateModel(currentTimeRounded2MinutesUp, currentTimeRounded2MinutesUp, null, Calendar.MINUTE));
    ((JSpinner.DefaultEditor) spnrSingleAdDateAndTime.getEditor()).getTextField().setEditable(false);

    spnrPeriodicAdStartDate.setModel(new SpinnerDateModel(currentTime, null, null, Calendar.DAY_OF_YEAR));
    spnrPeriodicAdStartDate.setEditor(new JSpinner.DateEditor(spnrPeriodicAdStartDate, "dd/MM/yyyy"));
    ((JSpinner.DefaultEditor) spnrPeriodicAdStartDate.getEditor()).getTextField().setEditable(false);

    spnrPeriodicAdStartTime.setModel(new SpinnerDateModel(currentTimeRounded2MinutesUp, null, null, Calendar.MINUTE));
    spnrPeriodicAdStartTime.setEditor(new JSpinner.DateEditor(spnrPeriodicAdStartTime, "HH:mm"));
    ((JSpinner.DefaultEditor) spnrPeriodicAdStartTime.getEditor()).getTextField().setEditable(false);
    
    spnrPeriodicAdEndTime.setModel(new SpinnerDateModel(currentTimeRounded2MinutesUp, null, null, Calendar.MINUTE));
    spnrPeriodicAdEndTime.setEditor(new JSpinner.DateEditor(spnrPeriodicAdEndTime, "HH:mm"));    
    ((JSpinner.DefaultEditor) spnrPeriodicAdEndTime.getEditor()).getTextField().setEditable(false);
  }
  
  private void setUpEditAdTimeSpinners() {
    setUpNewAdTimeSpinners();
    AdType adType = selectedAdToEdit.getType();
    IAdvertiser advertiser = (IAdvertiser) mainComputer.getCurrentUser();
    if (adType == AdType.SINGLE) {
      Calendar timeToShow = ((ISingleAd)selectedAdToEdit).getTimeToShow();
      spnrSingleAdDateAndTime.setModel(new SpinnerDateModel(timeToShow.getTime(), null, null, Calendar.MINUTE));
      ((JSpinner.DefaultEditor) spnrSingleAdDateAndTime.getEditor()).getTextField().setEditable(false); 
      rdbtnSingleAd.setSelected(true);
      if (advertiser.getType() == UserType.OFFICIAL_ADVERTISER)
        if (((ISingleAd) selectedAdToEdit).isConstant())
          chckbxSingleAdConstant.setSelected(true);
    }
    else if (adType == AdType.PERIODIC) {
      Calendar startDate = ((IPeriodicAd) selectedAdToEdit).getStartDate();      
      spnrPeriodicAdStartDate.setModel(new SpinnerDateModel(startDate.getTime(), null, null, Calendar.DAY_OF_YEAR));
      spnrPeriodicAdStartDate.setEditor(new JSpinner.DateEditor(spnrPeriodicAdStartDate, "DD/MM/yyyy"));
      ((JSpinner.DefaultEditor) spnrPeriodicAdStartDate.getEditor()).getTextField().setEditable(false);
      Calendar startTime = ((IPeriodicAd) selectedAdToEdit).getStartTime();
      spnrPeriodicAdStartTime.setModel(new SpinnerDateModel(startTime.getTime(), null, null, Calendar.DAY_OF_YEAR));
      spnrPeriodicAdStartTime.setEditor(new JSpinner.DateEditor(spnrPeriodicAdStartTime, "HH:mm"));
      ((JSpinner.DefaultEditor) spnrPeriodicAdStartTime.getEditor()).getTextField().setEditable(false);
      Calendar endTime = ((IPeriodicAd) selectedAdToEdit).getEndTime();
      spnrPeriodicAdEndTime.setModel(new SpinnerDateModel(endTime.getTime(), null, null, Calendar.MINUTE));
      spnrPeriodicAdEndTime.setEditor(new JSpinner.DateEditor(spnrPeriodicAdEndTime, "HH:mm"));    
      ((JSpinner.DefaultEditor) spnrPeriodicAdEndTime.getEditor()).getTextField().setEditable(false);
      rdbtnPeriodicAd.setSelected(true);
      if (advertiser.getType() == UserType.OFFICIAL_ADVERTISER)
        if (((IPeriodicAd) selectedAdToEdit).isConstant())
          chckbxPeriodicAdConstant.setSelected(true);
    }
    else if (adType == AdType.UNEXPECTED)
      rdbtnUnexpectedAd.setSelected(true);      
  }
  
  private void setUpEditAdAdName() {
    txtfldAdName.setText(selectedAdToEdit.getName());
  }
  
  private void setUpEditAdFileChooser() {
    file = postNetGUI.getSelectedAdToEdit().getFile();
    lblFileSelected.setText(file.getName());
  }

  private void setUpEditAdSelectedSigns() {
    IAd ad = postNetGUI.getSelectedAdToEdit();
    ArrayList<ISign> signs = ad.getSigns();
    ListSelectionModel model = tblSigns.getSelectionModel();
    for (int i = 0; i < tblSigns.getRowCount(); ++i) {
      int rowModel = tblSigns.convertRowIndexToModel(i);
      ISign sign = PostNetDB.getSignsDB().getSignByName(
          (String) tblSigns.getModel().getValueAt(rowModel,0));
      if (signs.contains(sign))
        model.addSelectionInterval(rowModel,rowModel);
    }
  }
  
  private void setUpOfficialOptions() {
    if (mainComputer.getCurrentUser().getType().equals(UserType.COMMERCIAL_ADVERTISER)) {
      chckbxSingleAdConstant.setVisible(false);
      chckbxPeriodicAdConstant.setVisible(false);
      rdbtnUnexpectedAd.setVisible(false);      
    }
    else {
      chckbxSingleAdConstant.setVisible(true);
      chckbxPeriodicAdConstant.setVisible(true);
      rdbtnUnexpectedAd.setVisible(true);
    }
  }
    
  /***************************************************************************/

  @Override
  public void setUpNew() {
    mode = Mode.NEW;
    setUpOfficialOptions();
    setUpCitiesComboBox();
    setUpNewAdTimeSpinners();
  }

  @Override
  public void setUpEdit() {
    clearForm();
    mode = Mode.EDIT;
    selectedAdToEdit = postNetGUI.getSelectedAdToEdit();
    setUpOfficialOptions();
    setUpCitiesComboBox();
    setUpEditAdTimeSpinners();
    setUpEditAdAdName();
    setUpEditAdFileChooser();
    setUpEditAdSelectedSigns();
  }
  
  @Override
  public Mode getMode() {
    return mode;
  }
  
  @Override
  public File getFile()  throws AdException {
    if (file == null) throw new AdException("Please select a file."); 
    return file;
  }

  @Override
  public String getName() throws AdException {
    String txt = txtfldAdName.getText();
    if (txt.length() == 0) throw new AdException("Please enter ad name.");
    return txt;
  }
  
  @Override
  public ArrayList<ISign> getSelectedSigns() throws AdException {    
    // Get selected signs rows.
    int[] selectedRowsIndex = tblSigns.getSelectedRows();
    if (selectedRowsIndex.length == 0) throw new AdException("Please select at least one sign.");
    // Get selected signs objects from rows.
    ISign[] signsArray = new ISign[selectedRowsIndex.length]; 
    for (int i = 0; i < selectedRowsIndex.length; ++i) {
      int rowModel = tblSigns.convertRowIndexToModel(selectedRowsIndex[i]);
      signsArray[i] = PostNetDB.getSignsDB().getSignByName(
          (String) tblSigns.getModel().getValueAt(rowModel,0));
    }    
    // Convert array to array list.
    ArrayList<ISign> signs = new ArrayList<>();
    for (int i = 0; i < signsArray.length; ++i) signs.add(signsArray[i]);
    return signs;
  }
  
  boolean isTimeEven(Date date) {
    if (date.getTime() != (date.getTime() / 120000) * 120000) return false;
    return true;
    
  }
  
  @Override
  public Calendar getSingleAdDateAndTime() throws AdException {
    Date date = (Date) spnrSingleAdDateAndTime.getValue();
    Date dateRoundedUp2Minutes = postNetDB.getCurrentTimeRounded2MinutesUp();
    if (date.before(dateRoundedUp2Minutes))
      throw new AdException("Your current minimal date and time are: "
          + dateRoundedUp2Minutes.toString());
    if (!isTimeEven(date))
      throw new AdException("Please select a time with even minutes value.");
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    return calendar;
  }

  @Override
  public Calendar getPeriodicAdStartDate() throws AdException {
    Date date = (Date) spnrPeriodicAdStartDate.getValue();
    Date currentDate = postNetDB.getCurrentTime();
    if ((date.getYear() < currentDate.getYear()) ||
        (date.getYear() == currentDate.getYear() &&
         date.getMonth() < currentDate.getMonth()) ||
        (date.getYear() == currentDate.getYear() &&
         date.getMonth() == currentDate.getMonth()) &&
         date.getDay() < currentDate.getDay())
      throw new AdException("Staring date can not be earlier then today.");
    Calendar calendar = Calendar.getInstance();
    calendar.setTime((Date) spnrPeriodicAdStartDate.getValue());
    return calendar;
  }

  private boolean isPeriodicAdStartDateToday() {
    Date date = (Date) spnrPeriodicAdStartDate.getValue();
    Date currentDate = postNetDB.getCurrentTime();
    if (date.getYear() == currentDate.getYear() &&
         date.getMonth() == currentDate.getMonth() &&
         date.getDay() == currentDate.getDay()) return true;
    return false;
  }
  
  @Override
  public Calendar getPeriodicAdStartTime() throws AdException {
    Date date = (Date) spnrPeriodicAdStartTime.getValue();
    Date currentDate = postNetDB.getCurrentTime();
    Date currentDateRoundedUp2Minutes = postNetDB.getCurrentTimeRounded2MinutesUp();
    if (isPeriodicAdStartDateToday())
      if ((date.getHours() < currentDate.getHours()) ||
          (date.getHours() == currentDate.getHours() &&
           date.getMinutes() < currentDateRoundedUp2Minutes.getMinutes()))
      throw new AdException("Your current minimal date and time are: "
          + currentDateRoundedUp2Minutes.toString());
    if (!isTimeEven(date))
      throw new AdException("Please select a time with even minutes value.");
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    return calendar;
  }

  @Override
  public Calendar getPeriodicAdEndTime() throws AdException {
    Date date = (Date) spnrPeriodicAdEndTime.getValue();
    Date startDate = (Date) spnrPeriodicAdStartTime.getValue();    
    if (date.getHours() == startDate.getHours() &&
        date.getMinutes() <= startDate.getMinutes())
      throw new AdException("End time must be later then start time.");
    if (!isTimeEven(date))
      throw new AdException("Please select a time with even minutes value.");
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    return calendar;
  }

  @Override
  public int getWeeks() throws AdException {
    int weeks = (int) spnrWeeks.getValue();
    if (weeks <= 0) throw new AdException("Please choose at least one week.");
    return weeks;
  }

  @Override
  public boolean[] getDays() throws AdException {
    boolean[] days = new boolean[7];
    int daysRowIndex = tblDays.convertRowIndexToModel(0);
    for (int i = 0; i < 7; ++i) {
      days[i] = false;
      if (tblDays.getModel().getValueAt(daysRowIndex,i) != null)
        days[i] = true;
    }
    boolean oneDaySelected = false;
    for (int i = 0; i < 7; ++i) if (days[i]) oneDaySelected = true;
    if (!oneDaySelected) throw new AdException("Please select at least one day.");
    return days;
  }

  @Override
  public boolean isSingleAdSelected() {
    return rdbtnSingleAd.isSelected();
  }

  @Override
  public boolean isPeriodicAdSelected() {
    return rdbtnPeriodicAd.isSelected();
  }

  @Override
  public boolean isUnexpectedAdSelected() {
    return rdbtnUnexpectedAd.isSelected();
  }

  @Override
  public boolean isFull2MinutesChecked() {
    return chckbxSingleAdConstant.isSelected();
  }
  
  @Override
  public boolean isFullTimeChecked() {
    return chckbxPeriodicAdConstant.isSelected();
  }
  
  @Override
  public void clearForm() {
    file = null;
    lblFileSelected.setText("No file selected");
    txtfldAdName.setText("");
    rdbtnPeriodicAd.setSelected(false);
    rdbtnSingleAd.setSelected(false);
    rdbtnUnexpectedAd.setSelected(false);
    setUpNewAdTimeSpinners();
    spnrWeeks.setValue(new Integer(1));
    defaultComboBoxModel.setSelectedItem("Everywhere");
    for (int i = 0 ; i < 7; ++i) tblDays.setValueAt(false, 0, i);
  }

}
