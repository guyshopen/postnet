package il.co.technion.postnet.GUI.Ad;

public class AdException extends Exception {

  public AdException(String message) {
    super(message);
  }
}
