package il.co.technion.postnet.GUI.Ad;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.GUI.Ad.AdPage.Mode;
import il.co.technion.postnet.Maintenance.IPostNetMaintenance;
import il.co.technion.postnet.Objects.MainComputer;
import il.co.technion.postnet.Objects.PeriodicAd;
import il.co.technion.postnet.Objects.SingleAd;
import il.co.technion.postnet.Objects.UnexpectedAd;
import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IMainComputer;
import il.co.technion.postnet.Objects.Interfaces.IPeriodicAd;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Objects.Interfaces.ISingleAd;
import il.co.technion.postnet.Objects.Interfaces.IUser;
import il.co.technion.postnet.Types.AdType;
import il.co.technion.postnet.Types.SignContent;
import il.co.technion.postnet.Types.UserType;

public class AdPageSaveAdActionListener implements ActionListener {

	IAdPage adPage;
	IPostNetDB postNetDB;
	IPostNetGUI postNetGUI;
	IPostNetMaintenance postNetMaintenance;
	IMainComputer mainComputer;

	AdPageSaveAdActionListener(IAdPage adPage, IPostNetDB postNetDB,
			IPostNetGUI postNetGUI, IPostNetMaintenance postNetMaintenance,IMainComputer mainComputer) {
		this.adPage = adPage;
		this.postNetDB = postNetDB;
		this.postNetGUI = postNetGUI;
		this.postNetMaintenance = postNetMaintenance;
		this.mainComputer = mainComputer;
	}

	@Override
	// Cross input with data base and publish new ad. (Guy)
	public void actionPerformed(ActionEvent e) {
		try {
			// Get selected file.
			File file = adPage.getFile();
			
      // Save selected file extension and content type.
			SignContent desiredContent;
			String fileName = file.getName();
			String fileExtension =
			    fileName.substring(fileName.length() - 3, fileName.length());			
			if (fileExtension.equals("txt"))
			  desiredContent = SignContent.TEXT;
			else desiredContent = SignContent.IMAGE;
						
			// Get selected signs.
			ArrayList<ISign> signs = adPage.getSelectedSigns();
			
			// Check content compatibility with selected signs.
			for (ISign sign : signs)
			  if (sign.getContent() != SignContent.BOTH &&
			      sign.getContent() != desiredContent)
			    throw new AdException(
			        "Selected signs are incompatible with selected content");

			// Get ad name.
			String name = adPage.getName();
			legalAdName(name);

			// Get selected type and date.
			IAd ad;
			boolean isFull2Minutes = false;
			if (adPage.isSingleAdSelected()) {
				Calendar dateAndTime = adPage.getSingleAdDateAndTime();
				if (mainComputer.getCurrentUser().getType()
						.equals(UserType.OFFICIAL_ADVERTISER))
					if (adPage.isFull2MinutesChecked())
						isFull2Minutes = true;
				// Create the new single ad.
				ad = new SingleAd(name, file, signs, dateAndTime,
						isFull2Minutes);
			} else if (adPage.isPeriodicAdSelected()) {

				Calendar startDate = adPage.getPeriodicAdStartDate();
				Calendar startTime = adPage.getPeriodicAdStartTime();
				Calendar endTime = adPage.getPeriodicAdEndTime();
				
				if (mainComputer.getCurrentUser().getType()
						.equals(UserType.OFFICIAL_ADVERTISER))
					if (adPage.isFullTimeChecked())
						isFull2Minutes = true;
				// Get selected days
				boolean[] days = adPage.getDays();
				// Get number of Periodic weeks.
				int weeks = adPage.getWeeks();

				// Create the new Periodic ad.

				ad = new PeriodicAd(name, file, signs, startDate, startTime,
						endTime, days, weeks, isFull2Minutes);

			} else if (adPage.isUnexpectedAdSelected()) {
				// Create the new unexpected ad.
				ad = new UnexpectedAd(name, file, signs);
			}
			// If no type selected show message and return to page.
			else
				throw new AdException("Please select ad type.");

			/************************************************************************/

			// Check if there's time to display the ad in the requested signs
			if (ad.getType().equals(AdType.SINGLE)) {
				for (ISign sign : ((ISingleAd) ad).getSigns()) {

					// For every sign in the ad, check availability
					ArrayList<IAd> adList = postNetMaintenance.getAdList(sign,
							((ISingleAd) ad).getTimeToShow());
					if ((adPage.getMode().equals(Mode.EDIT)))
						adList.removeAll(Collections.singleton(postNetGUI
								.getSelectedAdToEdit()));

					if ((((ISingleAd) ad).isConstant()) && (adList.size() > 0)) {

						// Not enough time to display this constant ad

						throw new AdException(
								"Signs are not available at the requested time.");

					} else if (adList.size() >= 12) {

						// Not enough time to display this ad

						throw new AdException(
								"Signs are not available at the requested time.");

					}
				}

			} else if (ad.getType().equals(AdType.PERIODIC)) {

				// For every sign in the ad, check availability

				for (ISign sign : ((IPeriodicAd) ad).getSigns()) {
					for (Calendar timeToShow : ((IPeriodicAd) ad)
							.getTimesToShow()) {

						// Check availability in each 2 minute time frame

						ArrayList<IAd> adList = postNetMaintenance.getAdList(
								sign, timeToShow);
						if ((adPage.getMode().equals(Mode.EDIT)))
							adList.removeAll(Collections.singleton(postNetGUI
									.getSelectedAdToEdit()));

						if ((((IPeriodicAd) ad).isConstant())
								&& (adList.size() > 0)) {

							// Not enough time to display this constant ad

							throw new AdException(
									"Signs are not available at the requested time.");

						} else if (adList.size() >= 12) {

							// Not enough time to display this ad

							throw new AdException(
									"Signs are not available at the requested time.");

						}
					}
				}
			}

			/************************************************************************/

			// Check that a constant ad is text only.
			int i = file.getName().lastIndexOf('.');
			if ((isFull2Minutes) && (i > 0)
					&& ((file.getName().substring(i + 1).equals("jpg")))) {
				throw new AdException("Constant ads are for text only.");
			}

			// If editing then cancel old ad.
			if (adPage.getMode().equals(Mode.EDIT))
				((IAdvertiser) mainComputer.getCurrentUser()).cancelAd(postNetGUI
						.getSelectedAdToEdit().getName());

			// Add ad to the user database
			((IAdvertiser) mainComputer.getCurrentUser()).addAd(ad);
			adPage.clearForm();
			postNetGUI.switchPanel(IPostNetGUI.ADVERTISER_HOME);

		} catch (AdException exception) {
			postNetGUI.showMessage(exception.getMessage());
		}
	}

	public void legalAdName(String name) throws AdException {
		// Check if ad name exist
		if ((adPage.getMode().equals(Mode.EDIT))
				&& (name.equals(postNetGUI.getSelectedAdToEdit().getName()))) {
			throw new AdException("The edited ad must have a new name.");
		}
		if (((IAdvertiser) mainComputer.getCurrentUser()).getAd(name) != null) {
			throw new AdException("Ad name already exist");
		}
	}
}
