package il.co.technion.postnet.GUI.Ad;

import il.co.technion.postnet.GUI.Ad.AdPage.Mode;
import il.co.technion.postnet.Objects.Interfaces.ISign;

import java.io.File;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * 
 * allows various options in ad definition GUI.
 *
 */
public interface IAdPage {

	/**
	 * set up new ad
	 */
  void setUpNew();
  /**
   *  set up edit ad 
   */
  void setUpEdit();
  /**
   * @return mode
   */
  Mode getMode();
/**
 *  @return ad file
 */
  File getFile() throws AdException;
  /**
   *  @return ad name
   */
  String getName() throws AdException;
  /**
   *  @return the signs the user selected
   */
  ArrayList<ISign> getSelectedSigns() throws AdException;
  /**
   *  @return the single date and time 
   */
  Calendar getSingleAdDateAndTime() throws AdException;
  /**
   *  @return the periodic start date 
   */
  Calendar getPeriodicAdStartDate() throws AdException;
  /**
   *  @return the periodic start time 
   */
  Calendar getPeriodicAdStartTime() throws AdException;
  /**
   *  @return the periodic end time 
   */
  Calendar getPeriodicAdEndTime() throws AdException;
  /**
   *  @return number of weeks 
   */
  int getWeeks() throws AdException;
  /**
   *  @return selected days
   */
  boolean[] getDays() throws AdException;
  /**
   *  @return true if single advertising was selected, else false
   */
  boolean isSingleAdSelected();
  /**
   *  @return true if periodic advertising was selected, else false
   */
  boolean isPeriodicAdSelected();
  /**
   *  @return true if unexcpected advertising was selected, else false
   */
  boolean isUnexpectedAdSelected();
  /**
   *  @return true if full two minutes checked, else false
   */
  boolean isFull2MinutesChecked();
  /**
   *  @return true if full time was selected, else false
   */
  boolean isFullTimeChecked();
  /**
   *  clears the form from all user entered data
   */
  void clearForm();

}
