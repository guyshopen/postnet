package il.co.technion.postnet.GUI;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.Account.AccountPage;
import il.co.technion.postnet.GUI.Account.IAccountPage;
import il.co.technion.postnet.GUI.Ad.IAdPage;
import il.co.technion.postnet.GUI.Ad.AdPage;
import il.co.technion.postnet.GUI.AddOfficialAdvertiser.AddOfficialAdvertiserPage;
import il.co.technion.postnet.GUI.AddOfficialAdvertiser.IAddOfficialAdvertiserPage;
import il.co.technion.postnet.GUI.AddSign.AddSignPage;
import il.co.technion.postnet.GUI.AddSign.IAddSignPage;
import il.co.technion.postnet.GUI.AdvertiserHome.AdvertiserHomePage;
import il.co.technion.postnet.GUI.AdvertiserHome.IAdvertiserHomePage;
import il.co.technion.postnet.GUI.ChangeSignStatus.ChangeSignStatusPage;
import il.co.technion.postnet.GUI.ChangeSignStatus.IChangeSignStatusPage;
import il.co.technion.postnet.GUI.ChooseAdToCancel.ChooseAdToCancelPage;
import il.co.technion.postnet.GUI.ChooseAdToCancel.IChooseAdToCancelPage;
import il.co.technion.postnet.GUI.ChooseAdToEdit.ChooseAdToEditPage;
import il.co.technion.postnet.GUI.ChooseAdToEdit.IChooseAdToEditPage;
import il.co.technion.postnet.GUI.Register.IRegisterPage;
import il.co.technion.postnet.GUI.Register.RegisterPage;
import il.co.technion.postnet.GUI.RemoveOfficialAdvertiser.IRemoveOfficialAdvertiserPage;
import il.co.technion.postnet.GUI.RemoveOfficialAdvertiser.RemoveOfficialAdvertiserPage;
import il.co.technion.postnet.GUI.RemoveSigns.IRemoveSignsPage;
import il.co.technion.postnet.GUI.RemoveSigns.RemoveSignsPage;
import il.co.technion.postnet.GUI.SalesAdminHome.SalesAdminHomePage;
import il.co.technion.postnet.GUI.SalesAdminHome.ISalesAdminHomePage;
import il.co.technion.postnet.GUI.SignsAdminHome.ISignsAdminHomePage;
import il.co.technion.postnet.GUI.SignsAdminHome.SignsAdminHomePage;
import il.co.technion.postnet.GUI.Statistics.IStatisticsPage;
import il.co.technion.postnet.GUI.Statistics.StatisticsPage;
import il.co.technion.postnet.GUI.Welcome.IWelcomePage;
import il.co.technion.postnet.GUI.Welcome.WelcomePage;
import il.co.technion.postnet.Maintenance.IPostNetMaintenance;
import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IMainComputer;
import il.co.technion.postnet.Objects.Interfaces.ISalesAdmin;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Objects.Interfaces.ISignsAdmin;
import il.co.technion.postnet.Types.SignContent;

import java.awt.CardLayout;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class PostNetGUI implements IPostNetGUI {
private IMainComputer mainComputer;
  private IPostNetDB postNetDB;
  private IPostNetMaintenance postNetMaintenance;
  private JFrame frmPostnet;

  IAccountPage accountPage;
  IAddOfficialAdvertiserPage addOfficialAdvertiserPage;
  IAddSignPage addSignPage;
  IAdvertiserHomePage advertiserHomePage;
  IChangeSignStatusPage changeSignStatusPage;
  IChooseAdToCancelPage chooseAdToCancelPage;
  IChooseAdToEditPage chooseAdToEditPage;
  IAdPage newAdPage;
  IRegisterPage registerPage;
  IRemoveOfficialAdvertiserPage removeOfficialAdvertiserPage;
  IRemoveSignsPage removeSignsPage;
  ISalesAdminHomePage salesAdminHomePage;
  ISignsAdminHomePage signsAdminHomePage;
  IStatisticsPage statisticsPage;
  IWelcomePage welcomePage;
  ISignsAdmin signsAdmin;
  ISalesAdmin salesAdmin;



  public PostNetGUI(IPostNetDB postNetDB, IPostNetMaintenance postNetMaintenance, JFrame frmPostnet, ISignsAdmin signsAdmin, ISalesAdmin salesAdmin, IMainComputer mainComputer) {
    this.postNetDB = postNetDB;
    this.frmPostnet = frmPostnet;
    this.postNetMaintenance = postNetMaintenance;
    this.salesAdmin = salesAdmin;
    this.signsAdmin = signsAdmin;
    this.mainComputer = mainComputer;
  }

  @Override
  public void initialize() {
    frmPostnet.setTitle("PostNet");
    frmPostnet.setBounds(100, 100, 700, 750);
    frmPostnet.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frmPostnet.getContentPane().setLayout(new CardLayout(0, 0));
    createPages();
    frmPostnet.setVisible(true);
  }

  void createPages() {

    welcomePage = new WelcomePage(postNetDB, this,mainComputer);
    addOfficialAdvertiserPage = new AddOfficialAdvertiserPage(postNetDB, this,signsAdmin);
    accountPage = new AccountPage(postNetDB, this,mainComputer);
    addSignPage = new AddSignPage(postNetDB, this, signsAdmin);
    advertiserHomePage = new AdvertiserHomePage(postNetDB, this,mainComputer);
    changeSignStatusPage = new ChangeSignStatusPage(postNetDB, this,signsAdmin);
    chooseAdToCancelPage = new ChooseAdToCancelPage(postNetDB, this,mainComputer);
    chooseAdToEditPage = new ChooseAdToEditPage(postNetDB, this,mainComputer);
    newAdPage = new AdPage(postNetDB, this, postNetMaintenance,mainComputer);
    registerPage = new RegisterPage(postNetDB, this,mainComputer);
    removeOfficialAdvertiserPage = new RemoveOfficialAdvertiserPage(postNetDB, this,signsAdmin);
    removeSignsPage = new RemoveSignsPage(postNetDB, this,signsAdmin);
    salesAdminHomePage = new SalesAdminHomePage(postNetDB, this,salesAdmin);
    signsAdminHomePage = new SignsAdminHomePage(signsAdmin, this);
    statisticsPage = new StatisticsPage(postNetDB, this,mainComputer);

  }

  @Override
  public JFrame getFrmPostnet() {
    return frmPostnet;
  }

  @Override
  public void showMessage(String s) {
    JOptionPane.showMessageDialog(null, s);
  }

  @Override
  public void switchPanel(String dest) {
    switch (dest) {
    case NEW_AD:
      newAdPage.setUpNew();
      ((CardLayout) frmPostnet.getContentPane().getLayout()).show(frmPostnet.getContentPane(),AD);
      return;
    case EDIT_AD:
      newAdPage.setUpEdit();
      ((CardLayout) frmPostnet.getContentPane().getLayout()).show(frmPostnet.getContentPane(),AD);
      return;
    case CHOOSE_AD_TO_CANCEL:
      chooseAdToCancelPage.setUp();
      break;
    case CHOOSE_AD_TO_EDIT:
      chooseAdToEditPage.setUp();
      break;
    case ACCOUNT:
      accountPage.setUp();
      break;
    case STATISTICS:
        statisticsPage.setUp();
        break;
    case SALES_ADMIN_HOME:
      salesAdminHomePage.setUp();
      break;
    case ADVERTISER_HOME:
      advertiserHomePage.setUp();
      break;
    case REMOVE_SIGNS:
      removeSignsPage.setUp();
      break;
    case CHANGE_SIGN_STATUS:
      changeSignStatusPage.setUp();
      break;
    case REMOVE_OFFICIAL_ADVERTISER:
      removeOfficialAdvertiserPage.setUp();
      break;
    default:
      break;
    }

    ((CardLayout) frmPostnet.getContentPane().getLayout()).show(frmPostnet.getContentPane(),
        dest);
  }

  /***************************************************************************/
  /*
   * Global GUI methods. 
   * 
   */

  @Override
  public void clearTable(JTable table) {
    DefaultTableModel model = (DefaultTableModel)table.getModel();
    while(model.getRowCount() > 0) model.removeRow(0);
  }

  // Adding all user ads to the cancel ads table.
  @Override
  public void addAllAdvertiserAdsToTable(IAdvertiser user,JTable table) {
    clearTable(table);
    DefaultTableModel model = (DefaultTableModel) table.getModel();
    ArrayList<IAd> ads = user.getAds();
    String[][] adNamesArray = new String[ads.size()][1];
    for (int i = 0; i < ads.size(); ++i) {
      adNamesArray[i][0] = ads.get(i).getName();
      model.addRow(adNamesArray[i]);
    }
  }

  // Adding all user active ads to the cancel ads table.
  @Override
  public void addAllAdvertiserActiveAdsToTable(IAdvertiser user,JTable table) {
    clearTable(table);
    DefaultTableModel model = (DefaultTableModel) table.getModel();
    ArrayList<IAd> ads = user.getActiveAds();
    String[][] adNamesArray = new String[ads.size()][1];
    for (int i = 0; i < ads.size(); ++i) {
      adNamesArray[i][0] = ads.get(i).getName();
      model.addRow(adNamesArray[i]);
    }
  }
  
  @Override
  public IAd getSelectedAdToEdit() {
    return chooseAdToEditPage.getSelectedAd();
  }

  @Override
  // Adding all signs from the signs database to a given table. (Guy)
  public void addAllSignsToTable(JTable table) {
    clearTable(table);
    ArrayList<ISign> signs = PostNetDB.getSignsDB().getSigns();
    for (int i = 0; i < signs.size(); ++i)
      addSignRowToTable(signs.get(i), table);          
  }
  
  @Override
  // Adding all active signs from the signs database to a given table. (Guy)
  public void addAllActiveSignsToTable(JTable table) {
    clearTable(table);
    ArrayList<ISign> signs = PostNetDB.getSignsDB().getActiveSigns();
    for (int i = 0; i < signs.size(); ++i)
      addSignRowToTable(signs.get(i), table);      
  }

  @Override
  public void addAllNotActiveSignsToTable(JTable table) {
    clearTable(table);
    ArrayList<ISign> signs = PostNetDB.getSignsDB().getNotActiveSigns();
    for (int i = 0; i < signs.size(); ++i)
      addSignRowToTable(signs.get(i), table);      
  }
  
  // Adding a sign to a given table. (Guy)
  @Override
  public void addSignRowToTable(ISign sign, JTable table) {
    DefaultTableModel model = (DefaultTableModel) table.getModel();
    Object[] signRow = new Object[]{sign.getName(),sign.getCity(),sign.getType(),
        sign.getContent() == SignContent.BOTH || sign.getContent() == SignContent.TEXT,
        sign.getContent() == SignContent.BOTH || sign.getContent() == SignContent.IMAGE,
        sign.getDayPrice(),sign.getNightPrice()};
    model.addRow(signRow);
  }

}
