package il.co.technion.postnet.GUI.SalesAdminHome;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Objects.Interfaces.IUser;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class SalesAdminHomePageSaveActionListener implements ActionListener {

  ISalesAdminHomePage salesAdminHomePage;
  IPostNetDB postNetDB;
  IPostNetGUI postNetGUI;

  SalesAdminHomePageSaveActionListener(ISalesAdminHomePage salesAdminHomePage, IPostNetDB postNetDB, IPostNetGUI postNetGUI) {
    this.salesAdminHomePage = salesAdminHomePage;
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
  }

  @Override
  // Cross input with the users database and register new user. (Guy)
  public void actionPerformed(ActionEvent e) {
    try {
      ArrayList<ISign> signs = new ArrayList<>();
      ArrayList<Float> dayPrices  = new ArrayList<>();
      ArrayList<Float> nightPrices  = new ArrayList<>();
      salesAdminHomePage.getSelectedSignsAndPrices(signs, dayPrices, nightPrices);
      for (int i = 0; i < signs.size(); ++i) {
    	  PostNetDB.getSignsDB().setDayPriceByName(signs.get(i).getName(), dayPrices.get(i));
    	  PostNetDB.getSignsDB().setNightPriceByName(signs.get(i).getName(), nightPrices.get(i));
      }
      
      // Notifying users
      
			for (IUser user : PostNetDB.getUsersDB().getUsers()) {
				if (user instanceof IAdvertiser) {
					((IAdvertiser) user).sendNotification("Our pricing policy has been changed. Please view our new prices.");
				}
			}
      
      PostNetDB.setDefaultDayPrice(salesAdminHomePage.getDefaultDayPrices());
      PostNetDB.setDefaultNightPrice(salesAdminHomePage.getDefaultNightPrices());
      postNetGUI.showMessage("Your changes have been saved.");
    } catch (SalesAdminHomeException exception) {
      postNetGUI.showMessage(exception.getMessage());
    }
  }    

}
