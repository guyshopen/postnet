package il.co.technion.postnet.GUI.SalesAdminHome;

import il.co.technion.postnet.Objects.Interfaces.ISign;

import java.util.ArrayList;
/**
 * 
 * sales admin home page
 *
 */
public interface ISalesAdminHomePage {
	/**
	 * setting up page
	 */
  void setUp();
/**
 * @param signs are selected signs
 * @param dayPrices are day prices
 * @param nightPrices are night prices
 */
  void getSelectedSignsAndPrices(ArrayList<ISign> signs,
      ArrayList<Float> dayPrices, ArrayList<Float> nightPrices)
      throws SalesAdminHomeException;
	/**
	 * @return default day price
	 */
  Float getDefaultDayPrices() throws SalesAdminHomeException;
	/**
	 * @return default night price
	 */
  Float getDefaultNightPrices() throws SalesAdminHomeException;
	/**
	 * @return notification
	 */
  String getNotification() throws SalesAdminHomeException;
  /**
	 * clear nitifications
	 */
  void clearNotification();

}
