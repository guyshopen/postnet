package il.co.technion.postnet.GUI.SalesAdminHome;

import javax.swing.JFrame;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.JSpinner;

import il.co.technion.postnet.Objects.Interfaces.ISalesAdmin;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Types.SignType;

public class SalesAdminHomePage implements ISalesAdminHomePage{

  private IPostNetDB postNetDB;
  private IPostNetGUI postNetGUI;
  private ISalesAdmin salesAdmin;
  private JTable tblSigns;
  private JTextField txtfldNotifyCostumers;
  private JSpinner spnrDefaultDayPrice;
  private JSpinner spnrDefaultNightPrice;

  public SalesAdminHomePage(IPostNetDB postNetDB, IPostNetGUI postNetGUI,ISalesAdmin salesAdmin) {
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
    this.salesAdmin = salesAdmin;
    initPage(postNetGUI.getFrmPostnet());
  }

  void initPage(JFrame frmPostnet) {

    JPanel pnlAdminHome = new JPanel();
    frmPostnet.getContentPane().add(pnlAdminHome, IPostNetGUI.SALES_ADMIN_HOME);
    pnlAdminHome.setLayout(null);

    JLabel lblPostNet = new JLabel("PostNet");
    lblPostNet.setHorizontalAlignment(SwingConstants.CENTER);
    lblPostNet.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 80));
    lblPostNet.setBounds(146, 24, 392, 139);
    pnlAdminHome.add(lblPostNet);

    JPanel pnlSigns = new JPanel();
    pnlSigns.setBounds(95, 176, 511, 353);
    pnlAdminHome.add(pnlSigns);
    pnlSigns.setLayout(null);

    JScrollPane scrlpnSigns = new JScrollPane();
    scrlpnSigns.setBounds(0, 0, 511, 316);
    pnlSigns.add(scrlpnSigns);

    tblSigns = new JTable();
    tblSigns.setFillsViewportHeight(true);
    tblSigns.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    tblSigns.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
        "Sign name", "City", "Type", "Text", "Images", "Day price",
    "Night price" }) {
      Class[] columnTypes = new Class[] { String.class, String.class,
          SignType.class, Boolean.class, Boolean.class, Float.class,
          Float.class };

      @Override
      public Class getColumnClass(int columnIndex) {
        return columnTypes[columnIndex];
      }

      boolean[] columnEditables = new boolean[] { false, false, false,
          false, false, true, true };

      @Override
      public boolean isCellEditable(int row, int column) {
        return columnEditables[column];
      }
    });

    tblSigns.getColumnModel().getColumn(0).setResizable(false);
    tblSigns.getColumnModel().getColumn(1).setResizable(false);
    tblSigns.getColumnModel().getColumn(2).setResizable(false);
    tblSigns.getColumnModel().getColumn(3).setResizable(false);
    tblSigns.getColumnModel().getColumn(4).setResizable(false);
    tblSigns.getColumnModel().getColumn(5).setResizable(false);
    tblSigns.getColumnModel().getColumn(6).setResizable(false);
    scrlpnSigns.setViewportView(tblSigns);

    JLabel lblDefaultPrice = new JLabel("Default price for new signs (in USD):");
    lblDefaultPrice.setBounds(0, 334, 224, 14);
    pnlSigns.add(lblDefaultPrice);

    JLabel lblDay = new JLabel("day:");
    lblDay.setBounds(290, 333, 25, 16);
    pnlSigns.add(lblDay);

    JLabel lblNight = new JLabel("night:");
    lblNight.setBounds(412, 333, 39, 16);
    pnlSigns.add(lblNight);

    spnrDefaultDayPrice = new JSpinner();
    spnrDefaultDayPrice.setBounds(327, 329, 58, 22);
    pnlSigns.add(spnrDefaultDayPrice);

    spnrDefaultNightPrice = new JSpinner();
    spnrDefaultNightPrice.setBounds(453, 329, 58, 22);
    pnlSigns.add(spnrDefaultNightPrice);

    txtfldNotifyCostumers = new JTextField();
    txtfldNotifyCostumers.setBounds(125, 631, 253, 20);
    pnlAdminHome.add(txtfldNotifyCostumers);
    txtfldNotifyCostumers.setColumns(10);

    JButton btnNotifyCustomers = new JButton("Notify customers");
    btnNotifyCustomers.setBounds(388, 630, 172, 23);
    btnNotifyCustomers.addActionListener(new SalesAdminHomePageNotifyActionListener(this, postNetDB, postNetGUI,salesAdmin));
    pnlAdminHome.add(btnNotifyCustomers);

    JButton btnLogOut = new JButton("Log out");
    btnLogOut.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(IPostNetGUI.WELCOME);
      }
    });
    btnLogOut.setBounds(598, 11, 76, 23);
    pnlAdminHome.add(btnLogOut);

    JButton btnSave = new JButton("Save");
    btnSave.addActionListener(new SalesAdminHomePageSaveActionListener(this, postNetDB, postNetGUI));
    btnSave.setBounds(267, 566, 172, 23);
    pnlAdminHome.add(btnSave);
  }

  @Override
  public void setUp() {
    postNetGUI.addAllSignsToTable(tblSigns);
    spnrDefaultDayPrice.setValue(PostNetDB.getDefaultDayPrice());
    ((JSpinner.DefaultEditor) spnrDefaultDayPrice.getEditor()).getTextField().setEditable(false);
    spnrDefaultNightPrice.setValue(PostNetDB.getDefaultNightPrice());
    ((JSpinner.DefaultEditor) spnrDefaultNightPrice.getEditor()).getTextField().setEditable(false);
  }
  
  @Override
  public void getSelectedSignsAndPrices(ArrayList<ISign> signs, ArrayList<Float> dayPrices, ArrayList<Float> nightPrices) throws SalesAdminHomeException {    
	  
	    // Get signs objects from rows.
	    ISign[] signsArray = new ISign[tblSigns.getRowCount()];
	    Float[] dayPricesArray = new Float[tblSigns.getRowCount()];
	    Float[] nightPricesArray = new Float[tblSigns.getRowCount()];
	    for (int i = 0; i < tblSigns.getRowCount(); ++i) {
	      int rowModel = tblSigns.convertRowIndexToModel(i);
	      signsArray[i] = PostNetDB.getSignsDB().getSignByName(
	          (String) tblSigns.getModel().getValueAt(rowModel,0));
	      dayPricesArray[i] = (float) tblSigns.getModel().getValueAt(rowModel,5);
	      nightPricesArray[i] = (float) tblSigns.getModel().getValueAt(rowModel,6);
	    }
	    
	    
    // Convert array to array list.
    for (int i = 0; i < signsArray.length; ++i) {
      signs.add(signsArray[i]);
      if (dayPricesArray[i] < 0 || nightPricesArray[i] < 0) throw new SalesAdminHomeException("Price must be positive.");
      dayPrices.add(dayPricesArray[i]);
      nightPrices.add(nightPricesArray[i]);
    }
  }

  @Override
  public Float getDefaultDayPrices() throws SalesAdminHomeException {
	  Float defaultDayPrice = (float) spnrDefaultDayPrice.getValue();
    if (defaultDayPrice < 0) throw new SalesAdminHomeException("Price must be positive.");    
    return defaultDayPrice;
  }

  @Override
  public Float getDefaultNightPrices() throws SalesAdminHomeException {
	  Float defaultNightPrice = (float) spnrDefaultNightPrice.getValue();
    if (defaultNightPrice < 0) throw new SalesAdminHomeException("Price must be positive.");    
    return defaultNightPrice;
  }

  @Override
  public String getNotification() throws SalesAdminHomeException {
    String notification = txtfldNotifyCostumers.getText();
    if (notification.length() == 0) throw new SalesAdminHomeException("Notification can not be blank.");
    return notification;
  }

  @Override
  public void clearNotification() {
    txtfldNotifyCostumers.setText("");    
  }
}

