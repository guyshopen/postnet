package il.co.technion.postnet.GUI.SalesAdminHome;

public class SalesAdminHomeException extends Exception {
  
  public SalesAdminHomeException(String message) {
    super(message);
  }

}
