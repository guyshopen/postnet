package il.co.technion.postnet.GUI.SalesAdminHome;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.ISalesAdmin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class SalesAdminHomePageNotifyActionListener implements ActionListener {

  ISalesAdminHomePage salesAdminHomePage;
  IPostNetDB postNetDB;
  IPostNetGUI postNetGUI;
  ISalesAdmin salesAdmin;

  SalesAdminHomePageNotifyActionListener(ISalesAdminHomePage salesAdminHomePage, IPostNetDB postNetDB, IPostNetGUI postNetGUI,ISalesAdmin salesAdmin) {
    this.salesAdminHomePage = salesAdminHomePage;
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
    this.salesAdmin = salesAdmin;
  }

  @Override
  // Cross input with the users database and register new user. (Guy)
  public void actionPerformed(ActionEvent e) {
    try {
      String notification = salesAdminHomePage.getNotification();
      salesAdmin.sendAdvertisersNotification(notification);
//      ArrayList<IAdvertiser> advertisers = PostNetDB.getUsersDB().getAdvertisers();
//      for (IAdvertiser advertiser : advertisers)
//        advertiser.sendNotification(notification);
      salesAdminHomePage.clearNotification();
    } catch (SalesAdminHomeException exception) {
      postNetGUI.showMessage(exception.getMessage());
    }
  }    


}
