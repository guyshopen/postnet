package il.co.technion.postnet.GUI.RemoveSigns;

public class RemoveSignsException extends Exception {

  public RemoveSignsException(String message) {
    super(message);
  }

}
