package il.co.technion.postnet.GUI.RemoveSigns;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Objects.Interfaces.ISignsAdmin;
import il.co.technion.postnet.Types.SignType;

import javax.swing.JFrame;
import javax.swing.JTable;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

public class RemoveSignsPage implements IRemoveSignsPage {

  private IPostNetDB postNetDB;
  private IPostNetGUI postNetGUI;
  private ISignsAdmin signsAdmin;

  private JTable tblSigns;

  public RemoveSignsPage(IPostNetDB postNetDB, IPostNetGUI postNetGUI,ISignsAdmin signsAdmin) {
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
    this.signsAdmin =signsAdmin;
    initPage(postNetGUI.getFrmPostnet());
  }

  public void initPage(JFrame frmPostnet) {

    JPanel pnlRemoveSign = new JPanel();
    frmPostnet.getContentPane().add(pnlRemoveSign, IPostNetGUI.REMOVE_SIGNS);
    pnlRemoveSign.setLayout(null);

    JButton btnOk = new JButton("OK");
    btnOk.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(IPostNetGUI.SIGNS_ADMIN_HOME);
      }
    });
    btnOk.setBounds(598, 11, 76, 23);
    pnlRemoveSign.add(btnOk);

    JScrollPane scrlpnSigns = new JScrollPane();
    scrlpnSigns.setBounds(42, 73, 607, 565);
    pnlRemoveSign.add(scrlpnSigns);

    tblSigns = new JTable();
    tblSigns.setFillsViewportHeight(true);
    tblSigns.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    tblSigns.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
        "Sign name", "City", "Type", "Text", "Images", "Day price",
    "Night price" }) {
      Class[] columnTypes = new Class[] { String.class, String.class,
          SignType.class, Boolean.class, Boolean.class, int.class,
          int.class };

      @Override
      public Class getColumnClass(int columnIndex) {
        return columnTypes[columnIndex];
      }

      boolean[] columnEditables = new boolean[] { false, false, false,
          false, false, false, false };

      @Override
      public boolean isCellEditable(int row, int column) {
        return columnEditables[column];
      }
    });

    tblSigns.getColumnModel().getColumn(0).setResizable(false);
    tblSigns.getColumnModel().getColumn(1).setResizable(false);
    tblSigns.getColumnModel().getColumn(2).setResizable(false);
    tblSigns.getColumnModel().getColumn(3).setResizable(false);
    tblSigns.getColumnModel().getColumn(4).setResizable(false);
    tblSigns.getColumnModel().getColumn(5).setResizable(false);
    tblSigns.getColumnModel().getColumn(6).setResizable(false);
    scrlpnSigns.setViewportView(tblSigns);

    JButton btnRemove = new JButton("Remove");
    btnRemove.addActionListener(new RemoveSignsPageRemoveActionListener(this, postNetDB, postNetGUI,signsAdmin));
    btnRemove.setBounds(560, 655, 89, 23);
    pnlRemoveSign.add(btnRemove);
  }
  
  @Override
  public void setUp() {
    postNetGUI.addAllSignsToTable(tblSigns);
  }

  @Override
  public String getSelectedSignName() throws RemoveSignsException {
    int selectedSignModel = tblSigns.convertRowIndexToModel(tblSigns.getSelectedRow());
    if (selectedSignModel == -1) throw new RemoveSignsException("Please select a sign to remove.");
    return (String) tblSigns.getValueAt(selectedSignModel, 0);
  }

}

