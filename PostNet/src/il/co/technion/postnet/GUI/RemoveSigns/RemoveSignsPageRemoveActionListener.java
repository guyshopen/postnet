package il.co.technion.postnet.GUI.RemoveSigns;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IPeriodicAd;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Objects.Interfaces.ISignsAdmin;
import il.co.technion.postnet.Objects.Interfaces.ISingleAd;
import il.co.technion.postnet.Objects.Interfaces.IUser;
import il.co.technion.postnet.Types.AdType;
import il.co.technion.postnet.Types.UserType;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

public class RemoveSignsPageRemoveActionListener implements ActionListener {

	IRemoveSignsPage removeSignPage;
	IPostNetDB postNetDB;
	IPostNetGUI postNetGUI;
	ISignsAdmin signsAdmin;

	RemoveSignsPageRemoveActionListener(IRemoveSignsPage removeSignPage,
			IPostNetDB postNetDB, IPostNetGUI postNetGUI,ISignsAdmin signsAdmin) {
		this.removeSignPage = removeSignPage;
		this.postNetDB = postNetDB;
		this.postNetGUI = postNetGUI;
		this.signsAdmin = signsAdmin;
	}

	@Override
	// Cross input with the users database and register new user. (Guy)
	public void actionPerformed(ActionEvent e) {
		try {

			String signName = removeSignPage.getSelectedSignName();
			signsAdmin.removeSign(signName);
//			ISign sign = PostNetDB.getSignsDB().getSignByName(signName);
//			PostNetDB.getSignsDB().removeSignByName(signName);
			postNetGUI.showMessage("The sign " + signName
					+ " has been removed.");
			removeSignPage.setUp();
		} catch (RemoveSignsException exception) {
			postNetGUI.showMessage(exception.getMessage());
		}
	}
}
