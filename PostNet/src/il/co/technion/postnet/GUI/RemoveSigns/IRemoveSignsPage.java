package il.co.technion.postnet.GUI.RemoveSigns;

/**
 * 
 * remove signs page
 *
 */
public interface IRemoveSignsPage {
/**
 * @return get selected sign name
 */
  String getSelectedSignName() throws RemoveSignsException;
/**
 * setting up page
 */
  void setUp();

}
