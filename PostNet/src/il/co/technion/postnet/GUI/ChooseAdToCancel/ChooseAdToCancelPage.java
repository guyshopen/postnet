package il.co.technion.postnet.GUI.ChooseAdToCancel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.MainComputer;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IMainComputer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

public class ChooseAdToCancelPage implements IChooseAdToCancelPage {

  private IPostNetDB postNetDB;
  private IPostNetGUI postNetGUI;
  IMainComputer mainComputer;

  private JTable tblAds;

  public ChooseAdToCancelPage(IPostNetDB postNetDB, IPostNetGUI postNetGUI,IMainComputer mainComputer) {
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
    this.mainComputer = mainComputer;
    initPage(postNetGUI.getFrmPostnet());
  }

  void initPage(JFrame frmPostnet) {

    JPanel pnlCancelAd = new JPanel();
    frmPostnet.getContentPane().add(pnlCancelAd, IPostNetGUI.CHOOSE_AD_TO_CANCEL);
    pnlCancelAd.setLayout(null);

    JButton btnOk = new JButton("Ok");
    btnOk.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(IPostNetGUI.ADVERTISER_HOME);
      }
      });
    btnOk.setBounds(598, 11, 76, 23);
    pnlCancelAd.add(btnOk);

    JScrollPane scrlpnAds = new JScrollPane();
    scrlpnAds.setBounds(123, 73, 437, 565);
    pnlCancelAd.add(scrlpnAds);

    tblAds = new JTable();
    tblAds.setFillsViewportHeight(true);
    tblAds.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    tblAds.setModel(new DefaultTableModel(new Object[][] {},
        new String[] { "Ad name" }) {
      boolean[] columnEditables = new boolean[] { false };
      @Override
      public boolean isCellEditable(int row, int column) {
        return columnEditables[column];
      }
    });
    
    tblAds.getColumnModel().getColumn(0).setResizable(false);
    scrlpnAds.setViewportView(tblAds);

    JButton btnCancelAd = new JButton("Cancel ad");
    btnCancelAd.setBounds(456, 649, 104, 23);
    pnlCancelAd.add(btnCancelAd);

    btnCancelAd.addActionListener(new ChooseAdToCancelPageCancelAdActionListener(this, postNetDB, postNetGUI,mainComputer));

  }

  @Override
  public void setUp() {
    postNetGUI.addAllAdvertiserActiveAdsToTable((IAdvertiser) mainComputer.getCurrentUser(),tblAds);
  }
  
  @Override
  public String[] getSelectedAds() throws ChooseAdToCancelException {
    int[] selectedRows = tblAds.getSelectedRows();
    if (selectedRows.length == 0) throw new ChooseAdToCancelException("Please select at least one ad.");
    String[] selectedAds = new String[selectedRows.length];
    for (int i = 0; i < selectedRows.length; ++i) {
      int rowIndex = tblAds.convertRowIndexToModel(i);
      selectedAds[i] = (String)tblAds.getModel().getValueAt(rowIndex,0);
    }
    return selectedAds;
  }

  @Override
  public void updateAdsTable() {
    postNetGUI.addAllAdvertiserActiveAdsToTable((IAdvertiser) mainComputer.getCurrentUser(), tblAds);    
  }
  

}

