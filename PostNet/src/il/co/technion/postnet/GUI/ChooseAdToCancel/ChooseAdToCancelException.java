package il.co.technion.postnet.GUI.ChooseAdToCancel;

public class ChooseAdToCancelException extends Exception {

	public ChooseAdToCancelException(String message) {
	  super(message);
	}

}
