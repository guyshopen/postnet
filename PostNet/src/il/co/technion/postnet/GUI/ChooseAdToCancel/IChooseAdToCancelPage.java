package il.co.technion.postnet.GUI.ChooseAdToCancel;

import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;

/**
 * 
 * The page that lets user deleting ads
 *
 */
public interface IChooseAdToCancelPage {
/**
 * setting up the page
 */
  void setUp();
/**
 * @return selected user ads
 */
  String[] getSelectedAds() throws ChooseAdToCancelException;
  /**
   * setting up ads table
   */
  void updateAdsTable();

}
