package il.co.technion.postnet.GUI.ChooseAdToCancel;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IMainComputer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChooseAdToCancelPageCancelAdActionListener implements ActionListener {
  IChooseAdToCancelPage chooseAdToCancelPage;
  IPostNetDB postNetDB;
  IPostNetGUI postNetGUI;
  IMainComputer mainComputer;

  ChooseAdToCancelPageCancelAdActionListener(IChooseAdToCancelPage chooseAdToCancelPage, IPostNetDB postNetDB, IPostNetGUI postNetGUI,  IMainComputer mainComputer) {
    this.chooseAdToCancelPage = chooseAdToCancelPage;
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
    this.mainComputer = mainComputer;
  }

  // Cross input with data base and publish new ad. (Guy)
  @Override
  public void actionPerformed(ActionEvent e) {
    try {
      // Remove all selected adds from user and refresh.
      String[] selectedAds = chooseAdToCancelPage.getSelectedAds();
      mainComputer.cancelAd(selectedAds);
//      for (int i = 0; i < selectedAds.length; ++i)
//        ((IAdvertiser) PostNetDB.getCurrentUser()).cancelAd(selectedAds[i]);
      chooseAdToCancelPage.updateAdsTable();      
    } catch (ChooseAdToCancelException exception) {
      postNetGUI.showMessage(exception.getMessage());
    }
  }    
}
