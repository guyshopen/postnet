package il.co.technion.postnet.GUI.AdvertiserHome;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.GUI.PostNetGUI;
import il.co.technion.postnet.Objects.MainComputer;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IMainComputer;

public class AdvertiserHomePage implements IAdvertiserHomePage {

  private IPostNetDB postNetDB;
  private IPostNetGUI postNetGUI;
  IMainComputer mainComputer;
  
  private JList<String> lstNotifications;
  private DefaultListModel<String> lstmdlNotifications;
  
  public AdvertiserHomePage(IPostNetDB postNetDB, IPostNetGUI postNetGUI, IMainComputer mainComputer) {
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;  
    this.mainComputer = mainComputer;
    initPage(postNetGUI.getFrmPostnet());
  }
  
  public void initPage(JFrame frmPostnet) {
    JPanel pnlAadvertiserHome = new JPanel();
    frmPostnet.getContentPane().add(pnlAadvertiserHome, IPostNetGUI.ADVERTISER_HOME);
    pnlAadvertiserHome.setLayout(null);

    JLabel lblPostNet = new JLabel("PostNet");
    lblPostNet.setHorizontalAlignment(SwingConstants.CENTER);
    lblPostNet.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 80));
    lblPostNet.setBounds(146, 70, 392, 181);
    pnlAadvertiserHome.add(lblPostNet);

    JPanel pnlButtons = new JPanel();
    pnlButtons.setBounds(195, 274, 294, 189);
    pnlAadvertiserHome.add(pnlButtons);
    pnlButtons.setLayout(null);

    JButton btnNewAd = new JButton("New ad");
    btnNewAd.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        postNetGUI.switchPanel(PostNetGUI.NEW_AD);
      }
    });
    btnNewAd.setBounds(10, 30, 100, 23);
    pnlButtons.add(btnNewAd);

    JButton btnAccount = new JButton("Account");
    btnAccount.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(PostNetGUI.ACCOUNT);
      }
    });
    btnAccount.setBounds(184, 30, 100, 23);
    pnlButtons.add(btnAccount);

    JButton btnEditAd = new JButton("Edit ad");
    btnEditAd.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(PostNetGUI.CHOOSE_AD_TO_EDIT);
      }
    });
    btnEditAd.setBounds(10, 83, 100, 23);
    pnlButtons.add(btnEditAd);

    JButton btnCancelAd = new JButton("Cancel ad");
    btnCancelAd.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(PostNetGUI.CHOOSE_AD_TO_CANCEL);
      }
    });
    btnCancelAd.setBounds(10, 136, 100, 23);
    pnlButtons.add(btnCancelAd);

    JButton btnStatistics = new JButton("Statistics");
    btnStatistics.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(PostNetGUI.STATISTICS);
      }
    });
    btnStatistics.setBounds(184, 83, 100, 23);
    pnlButtons.add(btnStatistics);

    lstNotifications = new JList<String>();
    lstNotifications.setFont(new Font("Tahoma", Font.PLAIN, 11));
    
    lstmdlNotifications = new DefaultListModel<String>();
    lstNotifications.setModel(lstmdlNotifications);

    lstNotifications.setBounds(129, 492, 425, 171);
    pnlAadvertiserHome.add(lstNotifications);
    lstNotifications.setBorder(new TitledBorder(new CompoundBorder(null, UIManager
        .getBorder("CheckBoxMenuItem.border")), "Notifications",
        TitledBorder.CENTER, TitledBorder.TOP, null, null));

    JButton btnLogOut = new JButton("Log out");
    btnLogOut.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(PostNetGUI.WELCOME);
      }
    });
    btnLogOut.setBounds(598, 11, 76, 23);
    pnlAadvertiserHome.add(btnLogOut);

  }
  
  @Override
  public void setUp() {
    lstmdlNotifications.clear();
    IAdvertiser advertiser = (IAdvertiser) mainComputer.getCurrentUser();
    ArrayList<String> notifications = advertiser.getNotifications();
    for (String notification : notifications)
      lstmdlNotifications.addElement(notification);
  }
  
}
