package il.co.technion.postnet.GUI.Statistics;

import javax.swing.JFrame;
import javax.swing.JTable;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.MainComputer;
import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IMainComputer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

public class StatisticsPage implements IStatisticsPage {

	private IPostNetDB postNetDB;
	 IMainComputer mainComputer;
	private IPostNetGUI postNetGUI;

	private JTable tblAds;

	public StatisticsPage(IPostNetDB postNetDB, IPostNetGUI postNetGUI, IMainComputer mainComputer) {
		this.postNetDB = postNetDB;
		this.postNetGUI = postNetGUI;
		this.mainComputer = mainComputer;
		initPage(postNetGUI.getFrmPostnet());
	}

	void initPage(JFrame frmPostnet) {

		JPanel pnlStatistics = new JPanel();
		frmPostnet.getContentPane().add(pnlStatistics, IPostNetGUI.STATISTICS);
		pnlStatistics.setLayout(null);

		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				postNetGUI.switchPanel(IPostNetGUI.ADVERTISER_HOME);
			}
		});
		btnOk.setBounds(598, 11, 76, 23);
		pnlStatistics.add(btnOk);

		JScrollPane scrlpnAds = new JScrollPane();
		scrlpnAds.setBounds(123, 81, 437, 550);
		pnlStatistics.add(scrlpnAds);

		tblAds = new JTable();
		tblAds.setFillsViewportHeight(true);
		tblAds.setEnabled(false);
		tblAds.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
				"Ad name", "Day time", "Night time" }) {
			Class[] columnTypes = new Class[] { Object.class, String.class,
					String.class };

			@Override
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		tblAds.getColumnModel().getColumn(0).setResizable(false);
		tblAds.getColumnModel().getColumn(1).setResizable(false);
		tblAds.getColumnModel().getColumn(2).setResizable(false);
		scrlpnAds.setViewportView(tblAds);

	}

	@Override
	public void setUp() {
		IAdvertiser advertiser = (IAdvertiser) mainComputer.getCurrentUser();
		postNetGUI.clearTable(tblAds);
		DefaultTableModel model = (DefaultTableModel) tblAds.getModel();
		ArrayList<IAd> ads = advertiser.getAds();
		Object[][] adsArray = new Object[ads.size()][3];
		for (int i = 0; i < ads.size(); ++i) {
			adsArray[i][0] = ads.get(i).getName();
			adsArray[i][1] = "" + ads.get(i).getDayTimeDispalyed() * 10
					+ " seconds";
			adsArray[i][2] = "" + ads.get(i).getNightTimeDispalyed() * 10
					+ " seconds";
			model.addRow(adsArray[i]);
		}
	}
}
