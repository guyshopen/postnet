package il.co.technion.postnet.GUI.RemoveOfficialAdvertiser;

public class RemoveOfficialAdvertiserPageException extends Exception {
  
  public RemoveOfficialAdvertiserPageException(String message) {
    super(message);
  }

}
