package il.co.technion.postnet.GUI.RemoveOfficialAdvertiser;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.Interfaces.IOfficialAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.ISignsAdmin;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

public class RemoveOfficialAdvertiserPage implements IRemoveOfficialAdvertiserPage {

  private IPostNetDB postNetDB;
  private IPostNetGUI postNetGUI;
  private ISignsAdmin signAdmin;

  private JTable tblUsers;

  public RemoveOfficialAdvertiserPage(IPostNetDB postNetDB, IPostNetGUI postNetGUI, ISignsAdmin signsAdmin) {
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
    this.signAdmin = signsAdmin;
    initPage(postNetGUI.getFrmPostnet());
  }

  public void initPage(JFrame frmPostnet) {

    JPanel pnlRemoveOfficial = new JPanel();
    frmPostnet.getContentPane().add(pnlRemoveOfficial, IPostNetGUI.REMOVE_OFFICIAL_ADVERTISER);
    pnlRemoveOfficial.setLayout(null);

    JButton btnOk = new JButton("OK");
    btnOk.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(IPostNetGUI.SIGNS_ADMIN_HOME);
      }
    });
    btnOk.setBounds(598, 11, 76, 23);
    pnlRemoveOfficial.add(btnOk);

    JScrollPane scrlpnUsers = new JScrollPane();
    scrlpnUsers.setBounds(123, 73, 437, 565);
    pnlRemoveOfficial.add(scrlpnUsers);

    tblUsers = new JTable();
    tblUsers.setFillsViewportHeight(true);
    tblUsers.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    tblUsers.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
        "Username", "Organization" }) {
      boolean[] columnEditables = new boolean[] { false, false };

      @Override
      public boolean isCellEditable(int row, int column) {
        return columnEditables[column];
      }
    });
    tblUsers.getColumnModel().getColumn(0).setResizable(false);
    tblUsers.getColumnModel().getColumn(1).setResizable(false);
    scrlpnUsers.setViewportView(tblUsers);

    JButton btnRemove = new JButton("Remove");
    btnRemove.setBounds(471, 649, 89, 23);
    btnRemove.addActionListener(new RemoveOfficialAdvertiserPageRemoveActionListener(this, postNetDB, postNetGUI, signAdmin));
    pnlRemoveOfficial.add(btnRemove);

  }

  @Override
  public void setUp() {
    postNetGUI.clearTable(tblUsers);
    ArrayList<IOfficialAdvertiser> officialAdvertisers = PostNetDB.getUsersDB().getOfficialAdvertisers();
    DefaultTableModel tblUsersModel = (DefaultTableModel) tblUsers.getModel();
    for (IOfficialAdvertiser officialAdvertiser : officialAdvertisers) {
      Object[] officialAdvertiserRow = new Object[] {officialAdvertiser.getName(), officialAdvertiser.getCompany()};
      tblUsersModel.addRow(officialAdvertiserRow);
    }
  }

  @Override
  public String getSelectedOfficialAdvertiserName() throws RemoveOfficialAdvertiserPageException {
    int selectedOfficialAdvertiserRow = tblUsers.getSelectedRow();
    if (selectedOfficialAdvertiserRow == -1) throw new RemoveOfficialAdvertiserPageException("Please select an official advertiser to remove.");
    int selectedOfficialAdvertiserModel = tblUsers.convertRowIndexToModel(selectedOfficialAdvertiserRow);
    return (String) tblUsers.getModel().getValueAt(selectedOfficialAdvertiserModel, 0);
  }

}

