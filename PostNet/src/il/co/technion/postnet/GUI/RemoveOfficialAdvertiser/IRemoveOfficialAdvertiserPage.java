package il.co.technion.postnet.GUI.RemoveOfficialAdvertiser;

/**
 * 
 * remove official advertisor page
 *
 */
public interface IRemoveOfficialAdvertiserPage {
/**
 * setting up the page
 */
  void setUp();
/**
 * @return returning selected official advisor name
 */
  String getSelectedOfficialAdvertiserName() throws RemoveOfficialAdvertiserPageException;

}
