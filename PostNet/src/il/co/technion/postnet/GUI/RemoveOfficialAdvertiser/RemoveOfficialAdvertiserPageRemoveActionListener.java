package il.co.technion.postnet.GUI.RemoveOfficialAdvertiser;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.Interfaces.ISignsAdmin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RemoveOfficialAdvertiserPageRemoveActionListener implements ActionListener {

  private IRemoveOfficialAdvertiserPage removeOfficialAdvertiserPage;
  private IPostNetDB postNetDB;
  private IPostNetGUI postNetGUI;
  ISignsAdmin signsAdmin;

  RemoveOfficialAdvertiserPageRemoveActionListener(IRemoveOfficialAdvertiserPage removeOfficialAdvertiserPage, IPostNetDB postNetDB, IPostNetGUI postNetGUI, ISignsAdmin signsAdmin) {
    this.removeOfficialAdvertiserPage = removeOfficialAdvertiserPage;
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
    this.signsAdmin = signsAdmin;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    try {
      String name = removeOfficialAdvertiserPage.getSelectedOfficialAdvertiserName();
      signsAdmin.removeOfficialAdvertiser(name);
      postNetGUI.showMessage("The official advertiser " + name + " has been removed from the system.");
      removeOfficialAdvertiserPage.setUp();
    } catch (RemoveOfficialAdvertiserPageException exception) {
      postNetGUI.showMessage(exception.getMessage());      
    }
  }

}
