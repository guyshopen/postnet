package il.co.technion.postnet.GUI;

import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.ISign;

import javax.swing.JFrame;
import javax.swing.JTable;
/**
 * 
 * postNet GUI
 *
 */
public interface IPostNetGUI {

  static public final String ACCOUNT = "account";
  static public final String AD = "ad";
  static public final String ADD_OFFICIAL_ADVERTISER = "add_official_advertiser";
  static public final String ADD_SIGN = "ad_sign";
  static public final String ADVERTISER_HOME = "advertiser_home";
  static public final String CHANGE_SIGN_STATUS = "change_sign_status";
  static public final String CHOOSE_AD_TO_CANCEL = "choose_ad_to_cancel";
  static public final String CHOOSE_AD_TO_EDIT = "choose_ad_to_edit";
  static public final String EDIT_AD = "edit_ad";
  static public final String NEW_AD = "new_ad";
  static public final String REGISTER = "register";
  static public final String REMOVE_OFFICIAL_ADVERTISER = "remove_official_advertiser";
  static public final String REMOVE_SIGNS = "remove_signs";
  static public final String SALES_ADMIN_HOME = "sales_admin_home";
  static public final String SIGNS_ADMIN_HOME = "signs_admin_home";
  static public final String SIMULATE = "simulate";
  static public final String STATISTICS = "statistics";
  static public final String WELCOME = "welcome";
  /**
   * initialize postnet gui
   */
  void initialize();
/**
 * @return jframe
 */
  JFrame getFrmPostnet();
/**
 * @param s show massage s
 */
  void showMessage(String s);
  /**
   * @param string switch panel 
   */
  void switchPanel(String string);
  /**
   * @param table clearing the table
   */
  void clearTable(JTable table);
  /**
   * @param currentUser the advertiser who's ads we'll add to the table
   * @param table the table to add the advertisors ads
   */
  void addAllAdvertiserAdsToTable(IAdvertiser currentUser, JTable table);
  /**
   * @param currentUser the advertiser who's active ads we'll add to the table
   * @param table the table to add the advertisors ads
   */
  void addAllAdvertiserActiveAdsToTable(IAdvertiser currentUser, JTable table);
  /**
   * @return selected ad to edit
   */
  IAd getSelectedAdToEdit();
  /**
   * @param table adding all active signs to table
   */
  void addAllActiveSignsToTable(JTable table);
  /**
   * @param iSign the sign we add
   * @param table the table we add to 
   */
  void addSignRowToTable(ISign iSign, JTable table);
  /**
   * @param table adding all signs to table
   */
  void addAllSignsToTable(JTable table);
  /**
   * @param table adding all not active signs to table
   */
  void addAllNotActiveSignsToTable(JTable table);

}
