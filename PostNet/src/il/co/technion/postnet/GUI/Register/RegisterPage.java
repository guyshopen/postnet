package il.co.technion.postnet.GUI.Register;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.Interfaces.IMainComputer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class RegisterPage implements IRegisterPage {

  private IPostNetDB postNetDB;
  private IPostNetGUI postNetGUI;
  private IMainComputer mainComputer;

  
  JTextField txtfldUsername, txtfldCompany, txtfldAccount;
  JPasswordField pswrdfldPassword;
  
  public RegisterPage(IPostNetDB postNetDB, IPostNetGUI postNetGUI,IMainComputer mainComputer) {
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
    this.mainComputer = mainComputer;

    initPage(postNetGUI.getFrmPostnet());
  }
  
  public void initPage(JFrame frmPostnet) {
    JPanel pnlRegister = new JPanel();
    frmPostnet.getContentPane().add(pnlRegister, IPostNetGUI.REGISTER);
    pnlRegister.setLayout(null);

    JPanel pnlForm = new JPanel();
    pnlForm.setBounds(124, 104, 435, 455);
    pnlRegister.add(pnlForm);
    pnlForm.setLayout(null);

    JLabel lblUsername = new JLabel("Username:");
    lblUsername.setBounds(0, 3, 131, 14);
    pnlForm.add(lblUsername);

    JLabel lblPassword = new JLabel("Password:");
    lblPassword.setBounds(0, 148, 131, 14);
    pnlForm.add(lblPassword);

    JLabel lblCompany = new JLabel("Company:");
    lblCompany.setBounds(0, 293, 131, 14);
    pnlForm.add(lblCompany);

    JLabel lblAccount = new JLabel("Account #:");
    lblAccount.setBounds(0, 438, 131, 14);
    pnlForm.add(lblAccount);

    txtfldUsername = new JTextField();
    txtfldUsername.setBounds(255, 0, 180, 20);
    pnlForm.add(txtfldUsername);
    txtfldUsername.setColumns(10);

    pswrdfldPassword = new JPasswordField();
    pswrdfldPassword.setBounds(255, 145, 180, 20);
    pnlForm.add(pswrdfldPassword);

    txtfldCompany = new JTextField();
    txtfldCompany.setBounds(255, 290, 180, 20);
    pnlForm.add(txtfldCompany);
    txtfldCompany.setColumns(10);

    txtfldAccount = new JTextField();
    txtfldAccount.setBounds(255, 435, 180, 20);
    pnlForm.add(txtfldAccount);
    txtfldAccount.setColumns(10);

    JButton btnRegister = new JButton("Register");
    btnRegister.setBounds(258, 649, 167, 23);
    btnRegister.addActionListener(new RegisterPageRegisterActionListener(this,postNetDB,postNetGUI,mainComputer));
    pnlRegister.add(btnRegister);

    JButton btnCancel = new JButton("Cancel");
    btnCancel.setBounds(598, 11, 76, 23);
    btnCancel.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        postNetGUI.switchPanel(IPostNetGUI.WELCOME);
      }
    });
    pnlRegister.add(btnCancel);
  }
  
  @Override
  public String getUsername() throws RegisterException {
    String txt = txtfldUsername.getText();
    if (txt.length() == 0) throw new RegisterException("Please enter a username");
    return txt;
  }

  @Override
  public char[] getPassword() throws RegisterException {
    char[] pswrd = pswrdfldPassword.getPassword();
    if (pswrd.length == 0) throw new RegisterException("Please enter a password.");
    if (pswrd.length < 6) throw new RegisterException("Password length must be at least 6.");
    return pswrd;
  }

  @Override
  public String getCompany() throws RegisterException {
    String txt = txtfldCompany.getText();
    if (txt.length() == 0) throw new RegisterException("Please enter company details");
    return txt;
  }

  @Override
  public String getAccount() throws RegisterException {
    String txt = txtfldAccount.getText();
    if (txt.length() == 0) throw new RegisterException("Please enter account information");
    return txt;
  }

}
