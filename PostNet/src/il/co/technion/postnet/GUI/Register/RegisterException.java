package il.co.technion.postnet.GUI.Register;

public class RegisterException extends Exception {

  public RegisterException(String message) {
    super(message);
  }

}
