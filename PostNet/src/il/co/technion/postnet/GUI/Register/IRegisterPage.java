package il.co.technion.postnet.GUI.Register;

/**
 * 
 * registration page
 *
 */
public interface IRegisterPage {
/**
 * @return returning user name
 */
  String getUsername() throws RegisterException;
  /**
   * @return returning user password
   */
  char[] getPassword() throws RegisterException;
  /**
   * @return returning user company
   */
  String getCompany() throws RegisterException;
  /**
   * @return returning user account
   */
  String getAccount() throws RegisterException;

}
