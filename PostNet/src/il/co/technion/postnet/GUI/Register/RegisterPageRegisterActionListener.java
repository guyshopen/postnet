package il.co.technion.postnet.GUI.Register;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.CommercialAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IMainComputer;
import il.co.technion.postnet.Objects.Interfaces.IUser;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RegisterPageRegisterActionListener implements ActionListener {

  IRegisterPage registerPage;
  IPostNetDB postNetDB;
  IPostNetGUI postNetGUI;
  IMainComputer mainComputer;

  public RegisterPageRegisterActionListener(IRegisterPage registerPage, IPostNetDB postNetDB, IPostNetGUI postNetGUI,  IMainComputer mainComputer) {
    this.registerPage = registerPage;
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
    this.mainComputer = mainComputer;
  }

  @Override
  // Cross input with the users database and register new user. (Guy)
  public void actionPerformed(ActionEvent e) {
    try {
      String username = registerPage.getUsername();
    char[] password = registerPage.getPassword();
    String company = registerPage.getCompany();
    String account = registerPage.getAccount();
    mainComputer.register(username, password, company, account);
//      if (PostNetDB.getUsersDB().getUser(username) != null)
//        throw new RegisterException("Username already exist");
//      char[] password = registerPage.getPassword();
//      String company = registerPage.getCompany();
//      String account = registerPage.getAccount();
//      IUser user = new CommercialAdvertiser(username,password,company,account);
//      PostNetDB.setCurrentUser(user);
//      PostNetDB.getUsersDB().addUser(user);
      postNetGUI.switchPanel(IPostNetGUI.ADVERTISER_HOME);           
    } catch (RegisterException exception) {
      postNetGUI.showMessage(exception.getMessage());
    }
  }    

}
