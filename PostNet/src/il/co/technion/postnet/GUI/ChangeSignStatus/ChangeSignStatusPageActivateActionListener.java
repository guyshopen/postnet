package il.co.technion.postnet.GUI.ChangeSignStatus;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Objects.Interfaces.ISignsAdmin;
import il.co.technion.postnet.Objects.Interfaces.IUser;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChangeSignStatusPageActivateActionListener implements ActionListener {

  IChangeSignStatusPage changeSignStatusPage;
  IPostNetDB postNetDB;
  IPostNetGUI postNetGUI;
  ISignsAdmin signAdmin;

  ChangeSignStatusPageActivateActionListener(IChangeSignStatusPage changeSignStatusPage, IPostNetDB postNetDB, IPostNetGUI postNetGUI,ISignsAdmin signAdmin) {
    this.changeSignStatusPage = changeSignStatusPage;
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
    this.signAdmin = signAdmin;
  }

  @Override
  // Cross input with the users database and register new user. (Guy)
  public void actionPerformed(ActionEvent e) {
    try {
      String signName = changeSignStatusPage.getSelectedSignToActivate();
      signAdmin.enableSign(signName);
//      ISign sign = PostNetDB.getSignsDB().getSignByName(signName);
//      PostNetDB.getSignsDB().enableSignByName(signName);
      changeSignStatusPage.setUp();
      
    } catch (ChangeSignStatusException exception) {
      postNetGUI.showMessage(exception.getMessage());
    }
  }

}
