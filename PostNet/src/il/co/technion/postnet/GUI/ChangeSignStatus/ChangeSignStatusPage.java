package il.co.technion.postnet.GUI.ChangeSignStatus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Objects.Interfaces.ISignsAdmin;
import il.co.technion.postnet.Types.SignType;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;

public class ChangeSignStatusPage implements IChangeSignStatusPage {

  private IPostNetDB postNetDB;
  private IPostNetGUI postNetGUI;
private ISignsAdmin signAdmin;
  private JTable tblActiveSigns;
  private JTable tblNotActiveSigns;

  public ChangeSignStatusPage(IPostNetDB postNetDB, IPostNetGUI postNetGUI, ISignsAdmin signAdmin) {
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
    this.signAdmin = signAdmin;
    initPage(postNetGUI.getFrmPostnet());
  }

  public void initPage(JFrame frmPostnet) {
    
    JPanel pnlChangeSignStatus = new JPanel();
    frmPostnet.getContentPane().add(pnlChangeSignStatus,
        "change_sign_status");
    pnlChangeSignStatus.setLayout(null);

    JButton btnOk = new JButton("OK");
    btnOk.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(IPostNetGUI.SIGNS_ADMIN_HOME);
      }
    });
    btnOk.setBounds(598, 11, 76, 23);
    pnlChangeSignStatus.add(btnOk);

    JScrollPane scrlpnActiveSigns = new JScrollPane();
    scrlpnActiveSigns.setBounds(42, 112, 607, 221);
    pnlChangeSignStatus.add(scrlpnActiveSigns);

    tblActiveSigns = new JTable();
    tblActiveSigns.setFillsViewportHeight(true);
    tblActiveSigns.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    tblActiveSigns.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
        "Sign name", "City", "Type", "Text", "Images", "Day price",
    "Night price" }) {
      Class[] columnTypes = new Class[] { String.class, String.class,
          SignType.class, Boolean.class, Boolean.class, int.class,
          int.class };

      @Override
      public Class getColumnClass(int columnIndex) {
        return columnTypes[columnIndex];
      }

      boolean[] columnEditables = new boolean[] { false, false, false,
          false, false, false, false };

      @Override
      public boolean isCellEditable(int row, int column) {
        return columnEditables[column];
      }
    });

    tblActiveSigns.getColumnModel().getColumn(0).setResizable(false);
    tblActiveSigns.getColumnModel().getColumn(1).setResizable(false);
    tblActiveSigns.getColumnModel().getColumn(2).setResizable(false);
    tblActiveSigns.getColumnModel().getColumn(3).setResizable(false);
    tblActiveSigns.getColumnModel().getColumn(4).setResizable(false);
    tblActiveSigns.getColumnModel().getColumn(5).setResizable(false);
    tblActiveSigns.getColumnModel().getColumn(6).setResizable(false);
    scrlpnActiveSigns.setViewportView(tblActiveSigns);

    JScrollPane scrlpnNotActiveSigns = new JScrollPane();
    scrlpnNotActiveSigns.setBounds(42, 410, 607, 221);
    pnlChangeSignStatus.add(scrlpnNotActiveSigns);

    tblNotActiveSigns = new JTable();
    tblNotActiveSigns.setFillsViewportHeight(true);
    tblNotActiveSigns.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    tblNotActiveSigns.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
        "Sign name", "City", "Type", "Text", "Images", "Day price",
    "Night price" }) {
      Class[] columnTypes = new Class[] { String.class, String.class,
          SignType.class, Boolean.class, Boolean.class, int.class,
          int.class };

      @Override
      public Class getColumnClass(int columnIndex) {
        return columnTypes[columnIndex];
      }

      boolean[] columnEditables = new boolean[] { false, false, false,
          false, false, false, false };

      @Override
      public boolean isCellEditable(int row, int column) {
        return columnEditables[column];
      }
    });

    tblNotActiveSigns.getColumnModel().getColumn(0).setResizable(false);
    tblNotActiveSigns.getColumnModel().getColumn(1).setResizable(false);
    tblNotActiveSigns.getColumnModel().getColumn(2).setResizable(false);
    tblNotActiveSigns.getColumnModel().getColumn(3).setResizable(false);
    tblNotActiveSigns.getColumnModel().getColumn(4).setResizable(false);
    tblNotActiveSigns.getColumnModel().getColumn(5).setResizable(false);
    tblNotActiveSigns.getColumnModel().getColumn(6).setResizable(false);
    scrlpnNotActiveSigns.setViewportView(tblNotActiveSigns);
    
    JButton btnActivate = new JButton("Activate");
    btnActivate.setBounds(531, 644, 118, 23);
    btnActivate.addActionListener(new ChangeSignStatusPageActivateActionListener(this, postNetDB, postNetGUI,signAdmin));
    pnlChangeSignStatus.add(btnActivate);
    
    JLabel lblActive = new JLabel("Active:");
    lblActive.setBounds(42, 83, 56, 16);
    pnlChangeSignStatus.add(lblActive);
    
    JLabel lblNotActive = new JLabel("Not active:");
    lblNotActive.setBounds(42, 381, 76, 16);
    pnlChangeSignStatus.add(lblNotActive);
    
    JButton btnDeActivate = new JButton("DeActivate");
    btnDeActivate.setBounds(531, 346, 118, 23);
    btnDeActivate.addActionListener(new ChangeSignStatusPageDeActivateActionListener(this, postNetDB, postNetGUI,signAdmin));
    pnlChangeSignStatus.add(btnDeActivate);
    
  }

  @Override
  public void setUp() {
    postNetGUI.addAllActiveSignsToTable(tblActiveSigns);
    postNetGUI.addAllNotActiveSignsToTable(tblNotActiveSigns);
  }

  @Override
  public String getSelectedSignToDeActivate() throws ChangeSignStatusException {
    int selectedRowIndex = tblActiveSigns.getSelectedRow();
    if (selectedRowIndex == -1) throw new ChangeSignStatusException("Please select a sign to deactivate.");
    int selectedRowModel = tblActiveSigns.convertRowIndexToModel(selectedRowIndex);
    return (String) tblActiveSigns.getModel().getValueAt(selectedRowModel, 0);
  }
  
  @Override
  public String getSelectedSignToActivate() throws ChangeSignStatusException {
    int selectedRowIndex = tblNotActiveSigns.getSelectedRow();
    if (selectedRowIndex == -1) throw new ChangeSignStatusException("Please select a sign to activate.");
    int selectedRowModel = tblNotActiveSigns.convertRowIndexToModel(selectedRowIndex);
    return (String) tblNotActiveSigns.getModel().getValueAt(selectedRowModel, 0);
  }

}

