package il.co.technion.postnet.GUI.ChangeSignStatus;

import il.co.technion.postnet.Objects.Interfaces.ISign;

/**
 * 
 * Changing sign status page. Changing the sign status.
 *
 */
public interface IChangeSignStatusPage {
/**
 * setting up the page
 */
  void setUp();
/**
 * @return selected sign to deactivate name
 */
  String getSelectedSignToDeActivate() throws ChangeSignStatusException;
  /**
   * @return selected sign to activate name
   */
  String getSelectedSignToActivate() throws ChangeSignStatusException;

}
