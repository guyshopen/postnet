package il.co.technion.postnet.GUI.ChangeSignStatus;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IPeriodicAd;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Objects.Interfaces.ISignsAdmin;
import il.co.technion.postnet.Objects.Interfaces.ISingleAd;
import il.co.technion.postnet.Objects.Interfaces.IUser;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

public class ChangeSignStatusPageDeActivateActionListener implements
		ActionListener {

	IChangeSignStatusPage changeSignStatusPage;
	IPostNetDB postNetDB;
	IPostNetGUI postNetGUI;
	ISignsAdmin signsAdmin;

	ChangeSignStatusPageDeActivateActionListener(
			IChangeSignStatusPage changeSignStatusPage, IPostNetDB postNetDB,
			IPostNetGUI postNetGUI, ISignsAdmin signsAdmin) {
		this.changeSignStatusPage = changeSignStatusPage;
		this.postNetDB = postNetDB;
		this.postNetGUI = postNetGUI;
		this.signsAdmin = signsAdmin;
	}

	@Override
	// Cross input with the users database and register new user. (Guy)
	public void actionPerformed(ActionEvent e) {
		try {
			String signName = changeSignStatusPage
					.getSelectedSignToDeActivate();
			signsAdmin.disableSign(signName);
			
//			ISign sign = PostNetDB.getSignsDB().getSignByName(signName);
//			PostNetDB.getSignsDB().disableSignByName(signName);
			changeSignStatusPage.setUp();

//			// Cancelling ads and notifying customers
//
//			// Go over all advertisers
//			for (IUser user : PostNetDB.getUsersDB().getUsers()) {
//				if (user instanceof IAdvertiser) {
//					// Go over all active (ongoing) ads of advertiser
//					for (IAd ad : ((IAdvertiser) user).getActiveAds()) {
//
//						// Future ads that contain this sign
//
//						if (ad.getSigns().contains(sign)) {
//
//							ad.setNotActive(); // Cancel;
//							((IAdvertiser) user).sendNotification("Sign \""
//									+ signName + "\" has been disabled. Ad \""
//									+ ad.getName() + "\" has been canceled.");
//						}
//					}
//				}
//			}

		} catch (ChangeSignStatusException exception) {
			postNetGUI.showMessage(exception.getMessage());
		}
	}

}
