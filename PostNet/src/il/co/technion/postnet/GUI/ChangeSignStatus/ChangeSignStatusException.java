package il.co.technion.postnet.GUI.ChangeSignStatus;

public class ChangeSignStatusException extends Exception {

  public ChangeSignStatusException(String message) {
    super(message);
  }

}
