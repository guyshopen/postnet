package il.co.technion.postnet.GUI.AddOfficialAdvertiser;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.GUI.Register.RegisterException;
import il.co.technion.postnet.Objects.Interfaces.ISignsAdmin;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class AddOfficialAdvertiserPage implements IAddOfficialAdvertiserPage {

  private IPostNetDB postNetDB;
  private IPostNetGUI postNetGUI;
private ISignsAdmin signsAdmin;
  private JTextField txtfldUsername;
  private JTextField txtfldOrganization;
  private JTextField txtfldAccount;
  private JPasswordField pswrdfldPassword;

  public AddOfficialAdvertiserPage(IPostNetDB postNetDB, IPostNetGUI postNetGUI,ISignsAdmin signsAdmin) {
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
    this.signsAdmin = signsAdmin;
    initPage(postNetGUI.getFrmPostnet());
  }

  public void initPage(JFrame frmPostnet) {


    JPanel pnlAddOfficialUser = new JPanel();
    frmPostnet.getContentPane().add(pnlAddOfficialUser, IPostNetGUI.ADD_OFFICIAL_ADVERTISER);
    pnlAddOfficialUser.setLayout(null);

    JPanel pnlForm = new JPanel();
    pnlForm.setLayout(null);
    pnlForm.setBounds(124, 104, 435, 455);
    pnlAddOfficialUser.add(pnlForm);

    JLabel lblUsername = new JLabel("Username:");
    lblUsername.setBounds(0, 3, 131, 14);
    pnlForm.add(lblUsername);

    JLabel lblPassword = new JLabel("Password:");
    lblPassword.setBounds(0, 148, 131, 14);
    pnlForm.add(lblPassword);

    JLabel lblOrganization = new JLabel("Organization:");
    lblOrganization.setBounds(0, 293, 131, 14);
    pnlForm.add(lblOrganization);

    JLabel lblAccount = new JLabel("Account #:");
    lblAccount.setBounds(0, 438, 131, 14);
    pnlForm.add(lblAccount);

    txtfldUsername = new JTextField();
    txtfldUsername.setColumns(10);
    txtfldUsername.setBounds(255, 0, 180, 20);
    pnlForm.add(txtfldUsername);

    txtfldOrganization = new JTextField();
    txtfldOrganization.setColumns(10);
    txtfldOrganization.setBounds(255, 290, 180, 20);
    pnlForm.add(txtfldOrganization);

    txtfldAccount = new JTextField();
    txtfldAccount.setColumns(10);
    txtfldAccount.setBounds(255, 435, 180, 20);
    pnlForm.add(txtfldAccount);

    pswrdfldPassword = new JPasswordField();
    pswrdfldPassword.setBounds(255, 145, 180, 20);
    pnlForm.add(pswrdfldPassword);

    JButton btnAddOfficial = new JButton("Add official");
    btnAddOfficial.setBounds(258, 649, 167, 23);
    btnAddOfficial.addActionListener(new AddOfficialAdvertiserPageAddActionListener(this, signsAdmin, postNetGUI));
    pnlAddOfficialUser.add(btnAddOfficial);

    JButton btnCancel = new JButton("Cancel");
    btnCancel.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(IPostNetGUI.SIGNS_ADMIN_HOME);
      }
    });
    btnCancel.setBounds(598, 11, 76, 23);
    pnlAddOfficialUser.add(btnCancel);

  }

  @Override
  public String getUsername() throws AddOfficialAdvertiserException {
    String txt = txtfldUsername.getText();
    if (txt.length() == 0) throw new AddOfficialAdvertiserException("Please enter a username");
    return txt;
  }

  @Override
  public char[] getPassword() throws AddOfficialAdvertiserException {
    char[] pswrd = pswrdfldPassword.getPassword();
    if (pswrd.length == 0) throw new AddOfficialAdvertiserException("Please enter a password.");
    if (pswrd.length < 6) throw new AddOfficialAdvertiserException("Password length must be at least 6.");
    return pswrd;
  }

  @Override
  public String getOrginization() throws AddOfficialAdvertiserException {
    String txt = txtfldOrganization.getText();
    if (txt.length() == 0) throw new AddOfficialAdvertiserException("Please enter orginization details");
    return txt;
  }

  @Override
  public String getAccount() throws AddOfficialAdvertiserException {
    String txt = txtfldAccount.getText();
    if (txt.length() == 0) throw new AddOfficialAdvertiserException("Please enter account information");
    return txt;
  }

}
