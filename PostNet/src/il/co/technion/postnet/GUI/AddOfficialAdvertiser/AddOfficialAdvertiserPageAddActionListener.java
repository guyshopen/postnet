package il.co.technion.postnet.GUI.AddOfficialAdvertiser;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.Interfaces.ISignsAdmin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddOfficialAdvertiserPageAddActionListener implements ActionListener {

  IAddOfficialAdvertiserPage addOfficialAdvertiserPage;

  IPostNetGUI postNetGUI;
  ISignsAdmin signsAdmin;

  AddOfficialAdvertiserPageAddActionListener(IAddOfficialAdvertiserPage addOfficialAdvertiserPage, ISignsAdmin signsAdmin, IPostNetGUI postNetGUI) {
    this.addOfficialAdvertiserPage = addOfficialAdvertiserPage;

    this.postNetGUI = postNetGUI;
    this.signsAdmin = signsAdmin;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    try {
      String username = addOfficialAdvertiserPage.getUsername();
      char[] password = addOfficialAdvertiserPage.getPassword();
      String company = addOfficialAdvertiserPage.getOrginization();
      String account = addOfficialAdvertiserPage.getAccount();
     signsAdmin.addOfficialAdvertiser(username,password,company,account);
//      IUser user = new OfficialAdvertiser(username,password,company,account);
//      PostNetDB.getUsersDB().addUser(user);
      postNetGUI.showMessage("The official user " + username + " has been added to the system.");
      postNetGUI.switchPanel(IPostNetGUI.SIGNS_ADMIN_HOME);           
    } catch (AddOfficialAdvertiserException exception) {
      postNetGUI.showMessage(exception.getMessage());
    }
  }    

}
