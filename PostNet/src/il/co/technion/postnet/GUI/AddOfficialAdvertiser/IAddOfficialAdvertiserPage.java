package il.co.technion.postnet.GUI.AddOfficialAdvertiser;

/**
 * 
 * official advertiser page
 *
 */
public interface IAddOfficialAdvertiserPage {
/**
 * @return user name
 */
  String getUsername() throws AddOfficialAdvertiserException;
  /**
   * @return password
   */
  char[] getPassword() throws AddOfficialAdvertiserException;
  /**
   * @return organization name
   */
  String getOrginization() throws AddOfficialAdvertiserException;
  /**
   * @return account name
   */
  String getAccount() throws AddOfficialAdvertiserException;

}
