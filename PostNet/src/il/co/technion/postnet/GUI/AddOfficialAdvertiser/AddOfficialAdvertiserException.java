package il.co.technion.postnet.GUI.AddOfficialAdvertiser;

public class AddOfficialAdvertiserException extends Exception {

  public AddOfficialAdvertiserException(String message) {
    super(message);
  }
}
