package il.co.technion.postnet.GUI.Welcome;

public class WelcomeException extends Exception {

  public WelcomeException(String message) {
    super(message);
  }
}
