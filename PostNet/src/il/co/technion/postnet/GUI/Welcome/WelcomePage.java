package il.co.technion.postnet.GUI.Welcome;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.Interfaces.IMainComputer;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class WelcomePage implements IWelcomePage {
   private IMainComputer mainComputer;
  private IPostNetDB postNetDB;
  private IPostNetGUI postNetGUI;
  WelcomePageLogInActionListener logInActionListener;
  
  JTextField txtfldUsername;
  JPasswordField pswrdfldPassword;
    
  public WelcomePage(IPostNetDB postNetDB, IPostNetGUI postNetGUI,IMainComputer mainComputer) {
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
    this.mainComputer = mainComputer;
    initPage(postNetGUI.getFrmPostnet());
  }
  
  void initPage(JFrame frmPostnet) {
    
    JPanel pnlWelcome = new JPanel();
    frmPostnet.getContentPane().add(pnlWelcome, IPostNetGUI.WELCOME);
    pnlWelcome.setLayout(null);

    JLabel lblPostnet = new JLabel("PostNet");
    lblPostnet.setHorizontalAlignment(SwingConstants.CENTER);
    lblPostnet.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 80));
    lblPostnet.setBounds(146, 70, 392, 181);
    pnlWelcome.add(lblPostnet);

    JPanel pnlForm = new JPanel();
    pnlForm.setBounds(174, 357, 336, 175);
    pnlWelcome.add(pnlForm);
    pnlForm.setLayout(null);

    JLabel lblUsername = new JLabel("Username:");
    lblUsername.setBounds(0, 3, 91, 14);
    pnlForm.add(lblUsername);

    JLabel lblPassword = new JLabel("Password:");
    lblPassword.setBounds(0, 31, 71, 14);
    pnlForm.add(lblPassword);

    txtfldUsername = new JTextField();
    txtfldUsername.setBounds(74, 0, 148, 20);
    pnlForm.add(txtfldUsername);
    txtfldUsername.setColumns(10);

    pswrdfldPassword = new JPasswordField();
    pswrdfldPassword.setBounds(74, 28, 148, 20);
    pnlForm.add(pswrdfldPassword);

    JButton btnLogin = new JButton("Login");
    btnLogin.addActionListener(new WelcomePageLogInActionListener(this,postNetDB,postNetGUI,mainComputer));

    btnLogin.setBounds(232, 27, 94, 23);
    pnlForm.add(btnLogin);

    JButton btnRegister = new JButton("Register");
    btnRegister.setBounds(232, 141, 96, 23);
    pnlForm.add(btnRegister);

    JLabel lblNewToPostnet = new JLabel("New to PostNet?");
    lblNewToPostnet.setBounds(0, 145, 151, 14);
    pnlForm.add(lblNewToPostnet);

    btnRegister.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        postNetGUI.switchPanel(IPostNetGUI.REGISTER);
      }
    });
  }

  @Override
  public String getUsername() throws WelcomeException {
    String txt = txtfldUsername.getText();
    if (txt.length() == 0) throw new WelcomeException("Please enter a username.");
    return txt;
  }

  @Override
  public char[] getPassword() throws WelcomeException {
    char[] pswrd = pswrdfldPassword.getPassword();
//    if (pswrd.length == 0) throw new WelcomeException("Please enter a password."); TODO: UNCOMMENT BEFORE SUBMIT!!!
    return pswrd;
  }
    
}
