package il.co.technion.postnet.GUI.Welcome;

public interface IWelcomePage {

  String getUsername() throws WelcomeException;

  char[] getPassword() throws WelcomeException;

}
