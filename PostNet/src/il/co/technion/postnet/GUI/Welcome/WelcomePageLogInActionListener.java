package il.co.technion.postnet.GUI.Welcome;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.GUI.PostNetGUI;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IMainComputer;
import il.co.technion.postnet.Objects.Interfaces.ISalesAdmin;
import il.co.technion.postnet.Objects.Interfaces.ISignsAdmin;
import il.co.technion.postnet.Objects.Interfaces.IUser;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

public class WelcomePageLogInActionListener implements ActionListener {

  private IWelcomePage welcomePage;
  private IPostNetDB postNetDB;
  private IPostNetGUI postNetGUI;
  IMainComputer mainComputer;

  public WelcomePageLogInActionListener(IWelcomePage welcomePage, IPostNetDB postNetDB, IPostNetGUI postNetGUI,IMainComputer mainComputer) {
    this.welcomePage = welcomePage;
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
    this.mainComputer = mainComputer;
  }

  @Override
  // Cross input with the users database and attempt login. (Guy)
  public void actionPerformed(ActionEvent e) {
    try {
      String username = welcomePage.getUsername();
      char[] password = welcomePage.getPassword();
      IUser user = mainComputer.login(username,password);
      if (IAdvertiser.class.isAssignableFrom(user.getClass()))
        postNetGUI.switchPanel(IPostNetGUI.ADVERTISER_HOME);
      else if (ISignsAdmin.class.isAssignableFrom(user.getClass()))
        postNetGUI.switchPanel(IPostNetGUI.SIGNS_ADMIN_HOME);
      else if (ISalesAdmin.class.isAssignableFrom(user.getClass()))
        postNetGUI.switchPanel(IPostNetGUI.SALES_ADMIN_HOME);
    } catch (WelcomeException exception) {
      postNetGUI.showMessage(exception.getMessage());      
    }
  }
}
