package il.co.technion.postnet.GUI.SignsAdminHome;

import javax.swing.JFrame;

import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.Interfaces.ISignsAdmin;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class SignsAdminHomePage implements ISignsAdminHomePage {

  private IPostNetGUI postNetGUI;
  ISignsAdmin signsAdmin;


  public SignsAdminHomePage(ISignsAdmin signsAdmin, IPostNetGUI postNetGUI) {
    this.postNetGUI = postNetGUI;
    this.signsAdmin = signsAdmin;
    initPage(postNetGUI.getFrmPostnet());
  }

  void initPage(JFrame frmPostnet) {

    JPanel pnlSignAdminHome = new JPanel();
    frmPostnet.getContentPane().add(pnlSignAdminHome, IPostNetGUI.SIGNS_ADMIN_HOME);
    pnlSignAdminHome.setLayout(null);

    JLabel lblPostNet = new JLabel("PostNet");
    lblPostNet.setHorizontalAlignment(SwingConstants.CENTER);
    lblPostNet.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 80));
    lblPostNet.setBounds(146, 70, 392, 181);
    pnlSignAdminHome.add(lblPostNet);

    JPanel pnlButtons = new JPanel();
    pnlButtons.setLayout(null);
    pnlButtons.setBounds(163, 322, 375, 189);
    pnlSignAdminHome.add(pnlButtons);

    JButton btnAddSign = new JButton("Add sign");
    btnAddSign.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(IPostNetGUI.ADD_SIGN);
      }
    });
    btnAddSign.setBounds(10, 30, 155, 23);
    pnlButtons.add(btnAddSign);

    JButton btnAddOfficials = new JButton("Add official");
    btnAddOfficials.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(IPostNetGUI.ADD_OFFICIAL_ADVERTISER);
      }
    });
    btnAddOfficials.setBounds(208, 30, 155, 23);
    pnlButtons.add(btnAddOfficials);

    JButton btnRemoveSign = new JButton("Remove sign");
    btnRemoveSign.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(IPostNetGUI.REMOVE_SIGNS);
      }
    });
    btnRemoveSign.setBounds(10, 83, 155, 23);
    pnlButtons.add(btnRemoveSign);

    JButton btnDisableSign = new JButton("Change sign status");
    btnDisableSign.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(IPostNetGUI.CHANGE_SIGN_STATUS);
      }
    });
    btnDisableSign.setBounds(10, 136, 155, 23);
    pnlButtons.add(btnDisableSign);

    JButton btnRemoveOfficials = new JButton("Remove official");
    btnRemoveOfficials.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(IPostNetGUI.REMOVE_OFFICIAL_ADVERTISER);
      }
    });
    btnRemoveOfficials.setBounds(208, 83, 155, 23);
    pnlButtons.add(btnRemoveOfficials);

    JButton btnLogOut = new JButton("Log out");
    btnLogOut.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(IPostNetGUI.WELCOME);
      }
    });
    btnLogOut.setBounds(598, 11, 76, 23);
    pnlSignAdminHome.add(btnLogOut);

  }

}
