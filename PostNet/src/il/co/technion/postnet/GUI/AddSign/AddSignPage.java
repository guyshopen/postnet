package il.co.technion.postnet.GUI.AddSign;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.Interfaces.ISignsAdmin;
import il.co.technion.postnet.Types.SignContent;
import il.co.technion.postnet.Types.SignType;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class AddSignPage implements IAddSignPage {

  private IPostNetDB postNetDB;
  private IPostNetGUI postNetGUI;
  private ISignsAdmin signsAdmin;

  private JTextField txtfldSignName;
  private JTextField txtfldCity;
  private JRadioButton rdbtnCity;
  private JRadioButton rdbtnInterCity;
  private JCheckBox chckbxText;
  private JCheckBox chckbxImages;

  public AddSignPage(IPostNetDB postNetDB, IPostNetGUI postNetGUI, ISignsAdmin signsAdmin) {
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
    this.signsAdmin = signsAdmin;
    initPage(postNetGUI.getFrmPostnet());
  }

  public void initPage(JFrame frmPostnet) {

    JPanel pnlAddSign = new JPanel();
    frmPostnet.getContentPane().add(pnlAddSign, IPostNetGUI.ADD_SIGN);
    pnlAddSign.setLayout(null);

    JPanel pnlForm = new JPanel();
    pnlForm.setLayout(null);
    pnlForm.setBounds(124, 104, 435, 455);
    pnlAddSign.add(pnlForm);

    JLabel lblSignName = new JLabel("Sign name (unique):");
    lblSignName.setBounds(0, 3, 131, 14);
    pnlForm.add(lblSignName);

    JLabel lblCity = new JLabel("City:");
    lblCity.setBounds(0, 148, 131, 14);
    pnlForm.add(lblCity);

    JLabel lblType = new JLabel("Type:");
    lblType.setBounds(0, 293, 131, 14);
    pnlForm.add(lblType);

    JLabel lblContent = new JLabel("Content:");
    lblContent.setBounds(0, 438, 131, 14);
    pnlForm.add(lblContent);

    txtfldSignName = new JTextField();
    txtfldSignName.setColumns(10);
    txtfldSignName.setBounds(255, 0, 180, 20);
    pnlForm.add(txtfldSignName);

    txtfldCity = new JTextField();
    txtfldCity.setColumns(10);
    txtfldCity.setBounds(255, 145, 180, 20);
    pnlForm.add(txtfldCity);

    JPanel pnlType = new JPanel();
    pnlType.setBounds(255, 284, 180, 23);
    pnlForm.add(pnlType);
    pnlType.setLayout(null);

    rdbtnCity = new JRadioButton("City");
    rdbtnCity.setBounds(0, 0, 59, 23);
    rdbtnCity.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        rdbtnInterCity.setSelected(false);
      }
    });
    pnlType.add(rdbtnCity);
    rdbtnCity.setSelected(true);

    rdbtnInterCity = new JRadioButton("Inter-city");
    rdbtnInterCity.setHorizontalAlignment(SwingConstants.RIGHT);
    rdbtnInterCity.setBounds(81, 0, 93, 23);
    rdbtnInterCity.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        rdbtnCity.setSelected(false);
      }
    });
    pnlType.add(rdbtnInterCity);

    JPanel pnlContent = new JPanel();
    pnlContent.setLayout(null);
    pnlContent.setBounds(255, 429, 180, 23);
    pnlForm.add(pnlContent);

    chckbxText = new JCheckBox("Text");
    chckbxText.setSelected(true);
    chckbxText.setBounds(0, 0, 97, 23);
    pnlContent.add(chckbxText);

    chckbxImages = new JCheckBox("Images");
    chckbxImages.setHorizontalAlignment(SwingConstants.RIGHT);
    chckbxImages.setBounds(70, 0, 97, 23);
    pnlContent.add(chckbxImages);

    JButton btnAddSign = new JButton("Add sign");
    btnAddSign.setBounds(258, 649, 167, 23);
    btnAddSign.addActionListener(new AddSignPageAddSignActionListener(this, postNetDB, postNetGUI,signsAdmin));
    pnlAddSign.add(btnAddSign);

    JButton btnCancel = new JButton("Cancel");
    btnCancel.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(IPostNetGUI.SIGNS_ADMIN_HOME);
      }
    });
    btnCancel.setBounds(598, 11, 76, 23);
    pnlAddSign.add(btnCancel);

  }

  @Override
  public String getSignName() throws AddSignException {
    String name = txtfldSignName.getText();
    if (name.length() == 0) throw new AddSignException("Please enter sign name.");
    return name;
  }

  @Override
  public String getSignCity() throws AddSignException {
    String city = txtfldCity.getText();
    if (city.length() == 0) throw new AddSignException("Please enter city.");
    return city;
  }

  @Override
  public SignType getSignType() throws AddSignException {
    if (rdbtnCity.isSelected()) return SignType.CITY;
    if (rdbtnInterCity.isSelected()) return SignType.INTERCITY;
    throw new AddSignException("Please select sign type.");  
  }

  @Override
  public SignContent getSignContent() throws AddSignException {
    if (chckbxText.isSelected() && chckbxImages.isSelected()) return SignContent.BOTH;
    if (chckbxText.isSelected()) return SignContent.TEXT;
    if (chckbxImages.isSelected()) return SignContent.IMAGE;
    throw new AddSignException("Please select sign content.");
  }

}


