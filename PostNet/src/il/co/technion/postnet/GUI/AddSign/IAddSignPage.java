package il.co.technion.postnet.GUI.AddSign;

import il.co.technion.postnet.Types.SignContent;
import il.co.technion.postnet.Types.SignType;

/**
 * adding signs page 
 *
 */
public interface IAddSignPage {
/**
 * @return sign name
 */
  String getSignName() throws AddSignException;
  /**
   * @return city name
   */
  String getSignCity() throws AddSignException;
  /**
   * @return sign type
   */
  SignType getSignType() throws AddSignException;
  /**
   * @return sign content
   */
  SignContent getSignContent() throws AddSignException;

}
