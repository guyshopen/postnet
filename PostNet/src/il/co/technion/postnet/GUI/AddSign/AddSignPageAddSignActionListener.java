package il.co.technion.postnet.GUI.AddSign;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.Sign;
import il.co.technion.postnet.Objects.Interfaces.IAdmin;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Objects.Interfaces.ISignsAdmin;
import il.co.technion.postnet.Objects.Interfaces.IUser;
import il.co.technion.postnet.Types.SignContent;
import il.co.technion.postnet.Types.SignType;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddSignPageAddSignActionListener implements ActionListener {

	IAddSignPage addSignPage;
	IPostNetDB postNetDB;
	IPostNetGUI postNetGUI;
	ISignsAdmin signsAdmin;

	AddSignPageAddSignActionListener(IAddSignPage addSignPage,
			IPostNetDB postNetDB, IPostNetGUI postNetGUI, ISignsAdmin signsAdmin) {
		this.addSignPage = addSignPage;
		this.postNetDB = postNetDB;
		this.postNetGUI = postNetGUI;
		this.signsAdmin = signsAdmin;
	}

	@Override
	// Cross input with the users database and register new user. (Guy)
	public void actionPerformed(ActionEvent e) {
		try {
			String name = addSignPage.getSignName();
			if (PostNetDB.getSignsDB().getSignByName(name) != null)
				throw new AddSignException("Sign name already exist.");
			String city = addSignPage.getSignCity();
			SignType type = addSignPage.getSignType();
			SignContent content = addSignPage.getSignContent();
			signsAdmin. addSign( name,  city,  type,  content);
//			ISign sign = new Sign(name, city, type, content,
//					postNetDB.getDefaultDayPrice(),
//					postNetDB.getDefaultNightPrice());
//			PostNetDB.getSignsDB().add(sign);

			// Notifying users
			postNetGUI.switchPanel(IPostNetGUI.SIGNS_ADMIN_HOME);
		} catch (AddSignException exception) {
			postNetGUI.showMessage(exception.getMessage());
		}
	}

}
