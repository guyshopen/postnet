package il.co.technion.postnet.GUI.AddSign;

public class AddSignException extends Exception {

  public AddSignException(String message) {
    super(message);
  }
}
