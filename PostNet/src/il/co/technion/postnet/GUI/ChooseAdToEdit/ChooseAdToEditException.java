package il.co.technion.postnet.GUI.ChooseAdToEdit;

public class ChooseAdToEditException extends Exception {
  
  public ChooseAdToEditException(String message) {
    super(message);
  }
}
