package il.co.technion.postnet.GUI.ChooseAdToEdit;

import il.co.technion.postnet.Objects.Interfaces.IAd;

/**
 * 
 * the page that lets choosing ads to edit
 *
 */
public interface IChooseAdToEditPage {
/**
 * setting up the page
 */
  void setUp();
/**
 * @return ads the the user selected
 */
  IAd getSelectedAd();
  /**
   * @param ad is setting the ad selected by user as selected
   */
  void setSelectedAd(IAd ad);
  /**
   * @return getting the selected ad name
   */
  String getSelectedAdName() throws ChooseAdToEditException;


}
