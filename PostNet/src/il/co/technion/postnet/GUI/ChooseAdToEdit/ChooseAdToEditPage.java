package il.co.technion.postnet.GUI.ChooseAdToEdit;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.MainComputer;
import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IMainComputer;

public class ChooseAdToEditPage implements IChooseAdToEditPage {

  private IPostNetDB postNetDB;
  private IPostNetGUI postNetGUI;
  IMainComputer mainComputer;
  JTable tblAds;

  IAd selectedAd;

  public ChooseAdToEditPage(IPostNetDB postNetDB, IPostNetGUI postNetGUI, IMainComputer mainComputer) {
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;  
    this.mainComputer = mainComputer;
    initPage(postNetGUI.getFrmPostnet());
  }

  public void initPage(JFrame frmPostnet) {
    JPanel pnlEditAd = new JPanel();
    pnlEditAd.setLayout(null);
    frmPostnet.getContentPane().add(pnlEditAd, IPostNetGUI.CHOOSE_AD_TO_EDIT);

    JButton btnCancel = new JButton("Cancel");
    btnCancel.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(IPostNetGUI.ADVERTISER_HOME);
      }
    });
    btnCancel.setBounds(598, 11, 76, 23);
    pnlEditAd.add(btnCancel);

    JScrollPane scrlpnAds = new JScrollPane();
    scrlpnAds.setBounds(123, 73, 437, 565);
    pnlEditAd.add(scrlpnAds);

    tblAds = new JTable();
    tblAds.setFillsViewportHeight(true);
    tblAds.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    tblAds.setModel(new DefaultTableModel(new Object[][] {},
        new String[] { "Ad name" }) {
      boolean[] columnEditables = new boolean[] { false };
      @Override
      public boolean isCellEditable(int row, int column) {
        return columnEditables[column];
      }
    });

    tblAds.getColumnModel().getColumn(0).setResizable(false);
    scrlpnAds.setViewportView(tblAds);

    JButton btnSelectAd = new JButton("Select ad");
    btnSelectAd.addActionListener(new ChooseAdToEditSelectAdActionListener(this, postNetDB, postNetGUI,mainComputer));
      
    btnSelectAd.setBounds(471, 649, 89, 23);
    pnlEditAd.add(btnSelectAd);

  }

  @Override
  public void setUp() {
    postNetGUI.addAllAdvertiserActiveAdsToTable((IAdvertiser) mainComputer.getCurrentUser(),tblAds);
  }

  @Override
  public IAd getSelectedAd() {
    return selectedAd;
  }

  @Override
  public void setSelectedAd(IAd ad) {
    this.selectedAd = ad;
  }
  
  @Override
  public String getSelectedAdName() throws ChooseAdToEditException {
    tblAds.getSelectedRow();
    int selectedRowIndex = tblAds.getSelectedRow();
    if (selectedRowIndex == -1) throw new ChooseAdToEditException("Please select an ad.");
    int rowModel = tblAds.convertRowIndexToModel(selectedRowIndex);
    return (String) tblAds.getModel().getValueAt(rowModel,0);

  }

}
