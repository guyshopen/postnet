package il.co.technion.postnet.GUI.ChooseAdToEdit;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.GUI.PostNetGUI;
import il.co.technion.postnet.Objects.MainComputer;
import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IMainComputer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChooseAdToEditSelectAdActionListener  implements ActionListener {
  IChooseAdToEditPage chooseAdToEditPage;
  IPostNetDB postNetDB;
  IPostNetGUI postNetGUI;
  IMainComputer mainComputer;

  ChooseAdToEditSelectAdActionListener(IChooseAdToEditPage chooseAdToEditPage, IPostNetDB postNetDB, IPostNetGUI postNetGUI, IMainComputer mainComputer) {
    this.chooseAdToEditPage = chooseAdToEditPage;
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
    this.mainComputer = mainComputer;;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    try {
      String selectedAdName = chooseAdToEditPage.getSelectedAdName();
      IAd ad = ((IAdvertiser) mainComputer.getCurrentUser()).getAd(selectedAdName);
      chooseAdToEditPage.setSelectedAd(ad);
      postNetGUI.switchPanel(PostNetGUI.EDIT_AD);
    } catch (ChooseAdToEditException exception) {
      postNetGUI.showMessage(exception.getMessage());
    }
  }    

}
