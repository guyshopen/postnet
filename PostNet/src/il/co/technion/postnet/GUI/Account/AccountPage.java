package il.co.technion.postnet.GUI.Account;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.Objects.MainComputer;
import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IMainComputer;

import javax.swing.JFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

public class AccountPage implements IAccountPage {

  private IPostNetDB postNetDB;
  private IPostNetGUI postNetGUI;

  private JTable tblAds;
  JLabel lblTotalCost;
  JLabel lblAccount;
  IMainComputer mainComputer;

  public AccountPage(IPostNetDB postNetDB, IPostNetGUI postNetGUI, IMainComputer mainComputer) {
    this.postNetDB = postNetDB;
    this.postNetGUI = postNetGUI;
    this.mainComputer = mainComputer;
    initPage(postNetGUI.getFrmPostnet());
  }

  void initPage(JFrame frmPostnet) {

    JPanel pnlAccount = new JPanel();
    frmPostnet.getContentPane().add(pnlAccount, IPostNetGUI.ACCOUNT);
    pnlAccount.setLayout(null);

    JButton btnOk = new JButton("OK");
    btnOk.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        postNetGUI.switchPanel(IPostNetGUI.ADVERTISER_HOME);
      }
    });
    btnOk.setBounds(598, 11, 76, 23);
    pnlAccount.add(btnOk);

    JScrollPane scrlpnAds = new JScrollPane();
    scrlpnAds.setBounds(123, 44, 437, 504);
    pnlAccount.add(scrlpnAds);

    tblAds = new JTable();
    tblAds.setFillsViewportHeight(true);
    tblAds.setEnabled(false);
    tblAds.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
        "Ad name", "Active", "Cost (so far)" }) {
      Class[] columnTypes = new Class[] { String.class, Boolean.class,
          Float.class };

      @Override
      public Class getColumnClass(int columnIndex) {
        return columnTypes[columnIndex];
      }

      boolean[] columnEditables = new boolean[] { false, false, false };

      @Override
      public boolean isCellEditable(int row, int column) {
        return columnEditables[column];
      }
    });
    tblAds.getColumnModel().getColumn(0).setResizable(false);
    tblAds.getColumnModel().getColumn(1).setResizable(false);
    tblAds.getColumnModel().getColumn(2).setResizable(false);
    scrlpnAds.setViewportView(tblAds);

    JLabel lblTotalCostLabel = new JLabel("Total cost (so far):");
    lblTotalCostLabel.setBounds(123, 580, 152, 14);
    pnlAccount.add(lblTotalCostLabel);

    lblTotalCost = new JLabel("?");
    lblTotalCost.setHorizontalAlignment(SwingConstants.RIGHT);
    lblTotalCost.setBounds(434, 580, 126, 14);
    pnlAccount.add(lblTotalCost);

    JLabel lblAccountLabel = new JLabel("Account #:");
    lblAccountLabel.setBounds(123, 635, 132, 14);
    pnlAccount.add(lblAccountLabel);

    lblAccount = new JLabel("?");
    lblAccount.setHorizontalAlignment(SwingConstants.RIGHT);
    lblAccount.setBounds(434, 635, 126, 14);
    pnlAccount.add(lblAccount);

  }
  
  @Override
  public void setUp() {
    IAdvertiser advertiser = (IAdvertiser) mainComputer.getCurrentUser();
    Float totalCost = (float) 0;
    postNetGUI.clearTable(tblAds);
    DefaultTableModel model = (DefaultTableModel) tblAds.getModel();
    ArrayList<IAd> ads = advertiser.getAds();
    Object[][] adsArray = new Object[ads.size()][3];
    for (int i = 0; i < ads.size(); ++i) {
      adsArray[i][0] = ads.get(i).getName();
      adsArray[i][1] = ads.get(i).isActive() ? new Boolean(true) : new Boolean(false);
      Float cost = ads.get(i).getCost();
      adsArray[i][2] = new Float(cost);
      model.addRow(adsArray[i]);
      totalCost += cost;
    }
    lblTotalCost.setText(String.valueOf(totalCost));
    lblAccount.setText(advertiser.getAccount());
  }
}

