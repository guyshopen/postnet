package il.co.technion.postnet.Types;

/**
 * 
 * Possible adType: NONE, SINGLE, PERIODIC, UNEXPECTED
 *
 */
public enum AdType {
  NONE, SINGLE, PERIODIC, UNEXPECTED;
}
