package il.co.technion.postnet.Types;

/**
 * 
 * Two sign types: CITY, INERCITY
 *
 */
public enum SignType {
	CITY, INTERCITY
}
