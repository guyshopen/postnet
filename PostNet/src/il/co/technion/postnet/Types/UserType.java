package il.co.technion.postnet.Types;

/**
 * 
 * All possible user types: USER, EMPLOYEE, ADMIN, SALES_ADMIN, SIGNS_ADMIN, ADVERTISER, COMMERCIAL_ADVERTISER, OFFICIAL_ADVERTISER
 *
 */
public enum UserType {
  USER, EMPLOYEE, ADMIN, SALES_ADMIN, SIGNS_ADMIN, ADVERTISER, COMMERCIAL_ADVERTISER, OFFICIAL_ADVERTISER;
  
  /**
   * @return returns true if user type is commercial or official advertiser, else false.
   */
  public boolean isAdvertiser() {
    return (this.equals(COMMERCIAL_ADVERTISER) || this.equals(OFFICIAL_ADVERTISER));
  }

  /**
   * @return returns true if user type is official advertiser, else false.
   */
  public boolean isOfficialAdvertiser() {
    return (this.equals(OFFICIAL_ADVERTISER));
  }

  /**
   * @return Returns true if user type is an admin, else false.
   */
  public boolean isAdmin() {
    return (this.equals(SALES_ADMIN) || this.equals(SIGNS_ADMIN));
  }
  
}
