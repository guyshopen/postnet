package il.co.technion.postnet.Types;


/**
 * Possible sign content: TEXT,IMAGE,BOTH
 */
public enum SignContent {
  TEXT,IMAGE,BOTH;
}
