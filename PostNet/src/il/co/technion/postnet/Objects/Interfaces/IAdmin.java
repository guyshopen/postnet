package il.co.technion.postnet.Objects.Interfaces;

import il.co.technion.postnet.Types.UserType;

/**
 * 
 * admin class
 *
 */
public interface IAdmin {
  /**
   * @return user type
   */
  public UserType getType();

}
