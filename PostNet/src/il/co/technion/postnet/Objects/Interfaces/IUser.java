package il.co.technion.postnet.Objects.Interfaces;

import il.co.technion.postnet.Types.UserType;


public interface IUser {

	/**
	 * @return username
	 */
	public String getName();
	/**
	 * @param name set user name
	 */
	public void setName(String password);
	/**
	 * @return password
	 */
	public char[] getPassword();
	/**
	 *  @param password set user password
	 */
	public void setPassword(char[] password);
	/**
	 * @return UserType USER
	 */
    public UserType getType();


}
