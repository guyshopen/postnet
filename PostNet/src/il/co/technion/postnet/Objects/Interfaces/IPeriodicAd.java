package il.co.technion.postnet.Objects.Interfaces;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * 
 * interface of the periodic ad
 *
 */
public interface IPeriodicAd extends IAd {

	/**
	 * @return is constant
	 */
	boolean isConstant();
	/**
	 * @return the days the ad shows
	 */
	boolean[] getDays();
	/**
	 * @return number of weeks
	 */
	int getWeeks();
	/**
	 * @return start time
	 */
	Calendar getStartTime();
	/**
	 * @return number of weeks
	 */
	Calendar getEndTime();
	/**
	 * @return start date
	 */
	Calendar getStartDate();
	/**
	 * @return times to show
	 */
	ArrayList<Calendar> getTimesToShow();

}
