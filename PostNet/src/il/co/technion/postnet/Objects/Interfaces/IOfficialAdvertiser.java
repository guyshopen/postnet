package il.co.technion.postnet.Objects.Interfaces;

/**
 * 
 * official advertiser interface
 *
 */
public interface IOfficialAdvertiser extends IAdvertiser {

	/**
	 * get organization name
	 */
  String getOrginization();
	/**
	 * @return organization name
	 */
  void setOrginization(String orginization);

}
