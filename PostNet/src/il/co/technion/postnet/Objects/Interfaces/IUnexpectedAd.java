package il.co.technion.postnet.Objects.Interfaces;

/**
 * 
 * Unexpected ad interface. Represents immediate ad.
 *
 */
public interface IUnexpectedAd extends IAd {

}
