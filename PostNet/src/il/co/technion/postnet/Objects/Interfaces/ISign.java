package il.co.technion.postnet.Objects.Interfaces;

import il.co.technion.postnet.Types.SignContent;
import il.co.technion.postnet.Types.SignType;

import java.util.ArrayList;
import java.util.HashMap;
/**
 *  This class represents a sign, and is an independent thread
 */
public interface ISign {
	/**
	 * @return name
	 */
	String getName();
	/**
	 * @return city
	 */
	String getCity();
	/**
	 * @return sign type
	 */
	SignType getType();
	/**
	 * @return sign content
	 */
	SignContent getContent();
	/**
	 * @param price sets night price
	 */
	void setNightPrice(Float price);
	/**
	 * @return night price
	 */
	Float getNightPrice();
	/**
	 * @param price sets day price
	 */
	void setDayPrice(Float price);
	/**
	 * @return day price
	 */
	Float getDayPrice();
	/**
	 * @return if ad is active
	 */
	boolean isActive();
	/** 
	 * Enables the sign
	 */
	void enable();
	/**
	 * disables the sign
	 */
	void disable();
	/**
	 *  Gives the sign a new list of non-immediate ads to display
	 *  @param adList list of ads
	 */
	void updateAdList(ArrayList<IAd> adList);
	/**
	 * Gives the sign a new immediate unexpected ad
	 * @param ad
	 */
	void pushUnexpactedAd(IAd ad);
	/**
	 *  @return Returns a report of ads displayed since last report
	 */
	HashMap<String, Integer> reportStatistics();
	/**
	 *  Disables the sign and kills the thread
	 */
	void die();
	
	public ArrayList<IAd> getUnexpactedAds();

}
