package il.co.technion.postnet.Objects.Interfaces;

import il.co.technion.postnet.Types.UserType;
/**
 * 
 *a class that represents the sales admin account
 *
 */
public interface ISalesAdmin {
	/**
	 * @return user type
	 */
  public UserType getType();
  public void sendAdvertisersNotification(String notification);
}
