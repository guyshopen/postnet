package il.co.technion.postnet.Objects.Interfaces;

import il.co.technion.postnet.GUI.Register.RegisterException;
import il.co.technion.postnet.GUI.Welcome.WelcomeException;

public interface IMainComputer {
	/**
	 * Login to the PostNet system
	 * 
	 * @param username
	 *            - The username of the user that was entered
	 * @param password
	 *            - The password that the user entered
	 * @return the user that exist in the database
	 * @throws WelcomeException
	 *             if the username doesn't exist in the database
	 */
	public IUser login(String username, char[] password)
			throws WelcomeException;

	public void logout();

	public void register(String username, char[] password, String company,
			String account) throws RegisterException;

	public void newAd();

	public void editAd();

	/**
	 * 
	 * @return the current user that entered the system
	 */
	public IUser getCurrentUser();

	/**
	 * This function cancels all the requested ads of the user
	 * 
	 * @param selectedAds
	 *            - all the ads that the user chose to cancel
	 */
	public void cancelAd(String[] selectedAds);

	public void simulation();
}
