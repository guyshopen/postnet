package il.co.technion.postnet.Objects.Interfaces;


import il.co.technion.postnet.Types.UserType;

import java.util.ArrayList;

/**
 * 
 * advertiser interface
 *
 */
public interface IAdvertiser extends IUser {

	/**
	 * @return ads
	 */
	public ArrayList<IAd> getAds();
	/**
	 * @param ads sets ads
	 */
	public void setAds(ArrayList<IAd> ads);
	/**
	 * @param ad adds ad
	 */
	void addAd(IAd ad);
	/**
	 * @param name removes ad
	 */
	void removeAd(String name);
	/**
	 * @return ad by name
	 */
	public IAd getAd(String name);
	/**
	 * @return notifications
	 */
  ArrayList<String> getNotifications();
	/**
	 * @param message sends notification
	 */
  void sendNotification(String message);
	/**
	 * @return active ads
	 */
  ArrayList<IAd> getActiveAds();
	/**
	 * @param cancels ad by name
	 */
  void cancelAd(String name);
	/**
	 * @return account
	 */
  String getAccount();
	/**
	 * @param account sets account
	 */
  void setAccount(String account);
	/**
	 * @return returns company name
	 */
  String getCompany();
	/**
	 * @param company sets company name
	 */
  void setCompany(String company);
	/**
	 * @return user type
	 */
  @Override
  public UserType getType();

}
