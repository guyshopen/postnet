package il.co.technion.postnet.Objects.Interfaces;

import java.util.Calendar;

/**
 * 
 * Ad that will be show one time for a specipic duration.
 *
 */
public interface ISingleAd extends IAd {
	/**
	 * Is this a constant official ad?
	 */
	public boolean isConstant();
	/**
	 * time to show the ad
	 */
	public Calendar getTimeToShow();

}
