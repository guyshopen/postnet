package il.co.technion.postnet.Objects.Interfaces;

import il.co.technion.postnet.Types.AdType;

import java.io.File;
import java.util.ArrayList;
/**
 * 
 * ad
 *
 */
public interface IAd {

	/**
	 * @return cost
	 */
	public Float getCost();
	/**
	 * @param cost 
	 */
	public void setCost(Float cost);
	/**
	 * @return signs
	 */
	public ArrayList<ISign> getSigns();
	/**
	 * @param signs sets the ad signs 
	 */
	public void setSigns(ArrayList<ISign> signs);
	/**
	 * @return night time display
	 */
	public int getNightTimeDispalyed();
	/**
	 * @param time sets night display time
	 */
	public void setNightTimeDispalyed(int time);
	/**
	 * @return day time display
	 */
	public int getDayTimeDispalyed();
	/**
	 * @param time sets day display time
	 */
	public void setDayTimeDispalyed(int time);
	/**
	 * set ad not active
	 */
	public void setNotActive();
	/**
	 * set ad as active
	 */
	public void setActive();
	/**
	 * @return is ad active
	 */
	public boolean isActive();
	/**
	 * @return ad name
	 */
	public String getName();
	/**
	 * @param name sets the ad name
	 */
	public void setName(String name);
	/**
	 * @return the file of the advertisment material
	 */
  public File getFile();
  /**
	 * @return the name of the file
	 */
	public String getFileName();
	/**
	 * @return get ad type
	 */
	public AdType getType();
}
