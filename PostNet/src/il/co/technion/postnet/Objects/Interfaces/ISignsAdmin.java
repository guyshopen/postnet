package il.co.technion.postnet.Objects.Interfaces;

import il.co.technion.postnet.GUI.AddOfficialAdvertiser.AddOfficialAdvertiserException;
import il.co.technion.postnet.Types.SignContent;
import il.co.technion.postnet.Types.SignType;
import il.co.technion.postnet.Types.UserType;

/**
 * 
 * the class which represents the sign admin account
 * 
 */
public interface ISignsAdmin {
	/**
	 * @return UserType.SIGNS_ADMIN
	 */
	public UserType getType();

	/**
	 * 
	 * @param username
	 *            - the username of the official advertiser
	 * @param password
	 *            - the password of the official advertiser
	 * @param company
	 *            - the company of the official advertiser
	 * @param account
	 *            - the account of the official advertiser
	 * @throws AddOfficialAdvertiserException
	 */
	public void addOfficialAdvertiser(String username, char[] password,
			String company, String account)
			throws AddOfficialAdvertiserException;

	/**
	 * 
	 * @param name
	 *            - the name of the official advertiser to remove from the
	 *            system
	 */
	public void removeOfficialAdvertiser(String name);

	/**
	 * This method adds a new sign to the PostNetSystem
	 * @param name - The name of the new sign to insert
	 * @param city- The city of the new sign to insert
	 * @param type- The type of the new sign to insert
	 * @param content- The content of the new sign to insert
	 */
	public void addSign(String name, String city, SignType type,
			SignContent content);

	/**
	 * 
	 * @param name - The name of the sign to remove from the PostNetSystem
	 */
	public void removeSign(String name);

	/**
	 * 
	 * @param signName - The name of the sign to disable
	 */
	public void disableSign(String signName);

	/**
	 * 
	 * @param signName - The name of the sign to enable
	 */
	public void enableSign(String signName);
}
