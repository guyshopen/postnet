package il.co.technion.postnet.Objects;

import il.co.technion.postnet.Objects.Interfaces.IOfficialAdvertiser;
import il.co.technion.postnet.Types.UserType;

public class OfficialAdvertiser extends Advertiser implements
		IOfficialAdvertiser {

	public OfficialAdvertiser(String name, char[] password, String orginization, String account) {
		super(name, password, orginization, account);
	}
	
  @Override
  public UserType getType() {
    return UserType.OFFICIAL_ADVERTISER;
  }
  
  @Override
  public String getOrginization() {
    return super.getCompany();
  }

  @Override
  public void setOrginization(String orginization) {
    super.setCompany(orginization);
  }
  
}
