package il.co.technion.postnet.Objects;

import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Objects.Interfaces.IUnexpectedAd;
import il.co.technion.postnet.Types.AdType;

import java.io.File;
import java.util.ArrayList;

/**
 * 
 * Unexpected ad implementation. Represents immediate ad.
 *
 */
public class UnexpectedAd extends Ad implements IUnexpectedAd {

	/**
	 * initialization of unexpected ad
	 * @param name ad name
	 * @param file is the sign file
	 * @param signs are all the sign the ad applys to
	 */
	public UnexpectedAd(String name, File file, ArrayList<ISign> signs) {
		super(name, file, signs);
	}

	/**
	 * @return AdType UNEXPECTED
	 */
  @Override
  public AdType getType() {
    return AdType.UNEXPECTED;
  }
	

}
