package il.co.technion.postnet.Objects;

import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Types.AdType;

import java.io.File;
import java.util.ArrayList;

public class Ad implements IAd {

	String name; // Unique name
	File file; // File of the ad
	ArrayList<ISign> signs; // Signs to display in
	boolean active = true; // Is the ad active (not cancelled and ongoing)

	int DayTimeDisplayed; // Total times displayed during day
	int NightTimeDisplayed; // Total times displayed during night
	Float cost = (float) 0; // Total cost so far

	public Ad(String name, File file, ArrayList<ISign> signs) {
		this.name = name;
		this.file = file;
		this.signs = (signs != null) ? signs : new ArrayList<ISign>();
	}

	@Override
	public synchronized Float getCost() {
		return cost;
	}

	@Override
	public synchronized void setCost(Float cost) {
		this.cost = cost;
	}

	@Override
	public synchronized ArrayList<ISign> getSigns() {
		return signs;
	}

	@Override
	public synchronized void setSigns(ArrayList<ISign> signs) {
		this.signs = signs;
	}

	@Override
	public synchronized int getNightTimeDispalyed() {
		return NightTimeDisplayed;
	}

	@Override
	public synchronized void setNightTimeDispalyed(int time) {
		this.NightTimeDisplayed = time;
	}

	@Override
	public synchronized int getDayTimeDispalyed() {
		return DayTimeDisplayed;
	}

	@Override
	public synchronized void setDayTimeDispalyed(int time) {
		this.DayTimeDisplayed = time;
	}

	@Override
	public synchronized void setNotActive() {
		active = false;
	}

	@Override
	public synchronized void setActive() {
		active = true;
	}

	@Override
	public synchronized boolean isActive() {
		return active;
	}

	@Override
	public synchronized String getName() {
		return name;
	}

	@Override
	public synchronized void setName(String name) {
		this.name = name;
	}

  @Override
  public synchronized File getFile() {
    return file;
  }
  
	@Override
	public synchronized String getFileName() {
		return file.getAbsolutePath();
	}

	@Override
	public AdType getType() {
		return AdType.NONE;
	}
}