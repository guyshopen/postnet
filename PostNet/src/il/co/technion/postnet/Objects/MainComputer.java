package il.co.technion.postnet.Objects;

import il.co.technion.postnet.DB.ISignsDB;
import il.co.technion.postnet.DB.IUsersDB;
import il.co.technion.postnet.GUI.Register.RegisterException;
import il.co.technion.postnet.GUI.Welcome.WelcomeException;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IMainComputer;
import il.co.technion.postnet.Objects.Interfaces.IUser;

import java.util.Arrays;

public class MainComputer implements IMainComputer {
	IUsersDB userDB;
	ISignsDB signsDB;
	IUser currentUser;

	public MainComputer(IUsersDB userDB, ISignsDB signsDB) {
		this.userDB = userDB;
		this.signsDB = signsDB;
	}
	/**
	 * @return get current user
	 */
	public IUser getCurrentUser() {
		return currentUser;
	}
	
	@Override
	public IUser login(String username, char[] password)
			throws WelcomeException {
		IUser user = userDB.getUser(username);
		if (user == null)
			throw new WelcomeException("Bad username!");
		if (!Arrays.equals(password, user.getPassword()))
			throw new WelcomeException("Bad password!");
		//postNetDB.setCurrentUser(user);
		currentUser = user;
		return user;
	}

	@Override
	public void logout() {
	
	}

	@Override
	public void register(String username, char[] password, String company, String account) throws RegisterException {
		   if (!islegalNewUsername(username))
		        throw new RegisterException("Username already exist");
		      IUser user = new CommercialAdvertiser(username,password,company,account);
		      //PostNetDB.setCurrentUser(user);
		      currentUser = user;
		      userDB.addUser(user);
	}
private boolean islegalNewUsername(String username){
	if (userDB.getUser(username) != null)
		return false;
	return true;
}
	@Override
	public void newAd() {
		// TODO Auto-generated method stub

	}

	@Override
	public void editAd() {
		// TODO Auto-generated method stub

	}

	@Override
	public void cancelAd(String[] selectedAds) {
	      for (int i = 0; i < selectedAds.length; ++i)
	          ((IAdvertiser) currentUser).cancelAd(selectedAds[i]);


	}

	@Override
	public void simulation() {
		// TODO Auto-generated method stub

	}

}
