package il.co.technion.postnet.Objects;

import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Types.UserType;

import java.util.ArrayList;

public class Advertiser extends User implements IAdvertiser {

  String company;
  String account;
  ArrayList<IAd> ads;
  ArrayList<String> notifications;

  public Advertiser(String name, char[] password, String company, String account) {
    super(name, password);
    this.company = company;
    this.account = account;
    this.ads = new ArrayList<>();
    this.notifications = new ArrayList<>();
    this.notifications.add("Welcome to PostNet!");
  }

  @Override
  public synchronized ArrayList<String> getNotifications() {
    return notifications;
  }

  @Override
  public synchronized void sendNotification(String message) {
    notifications.add(message);
  }

  @Override
  public synchronized ArrayList<IAd> getAds() {
    return ads;
  }

  @Override
  public synchronized void setAds(ArrayList<IAd> ads) {
    this.ads = ads;
  }

  @Override
  public synchronized void addAd(IAd ad) {
    ads.add(ad);
  }

  @Override
  public synchronized void removeAd(String name) {
    for (int i = 0; i < ads.size(); ++i)
      if (ads.get(i).getName().equals(name))
        ads.remove(i);
  }

  @Override
  public synchronized IAd getAd(String name) {
    return findAd(name);
  }

  @Override
  public synchronized ArrayList<IAd> getActiveAds() {
    ArrayList<IAd> activeAds = new ArrayList<>();
    for (IAd ad : ads) {
      if (ad.isActive()) {
        activeAds.add(ad);
      }
    }
    return activeAds;
  }

  @Override
  public synchronized void cancelAd(String name) {
    IAd ad = findAd(name);
    if (ad == null) return;
    ad.setNotActive();
  }

  private synchronized IAd findAd(String name) {
    for (IAd ad : ads)
      if (ad.getName().equals(name))
        return ad;
    return null;
  }

  @Override
  public synchronized String getAccount() {
    return account;
  }

  @Override
  public synchronized void setAccount(String account) {
    this.account = account;
  }

  @Override
  public synchronized String getCompany() {
    return company;
  }

  @Override
  public synchronized void setCompany(String company) {
    this.company = company;
  }

  @Override
  public UserType getType() {
    return UserType.ADVERTISER;
  }

}

