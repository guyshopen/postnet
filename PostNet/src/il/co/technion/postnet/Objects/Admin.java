package il.co.technion.postnet.Objects;

import il.co.technion.postnet.Objects.Interfaces.IAdmin;
import il.co.technion.postnet.Types.UserType;

public class Admin extends User implements IAdmin {

	public Admin(String name, char[] password) {
		super(name, password);
	}

  @Override
  public UserType getType() {
    return UserType.ADMIN;
  }

}
