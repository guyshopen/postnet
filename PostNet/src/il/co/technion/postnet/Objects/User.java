package il.co.technion.postnet.Objects;

import il.co.technion.postnet.Objects.Interfaces.IUser;
import il.co.technion.postnet.Types.UserType;


public class User implements IUser {


	String username;
	char[] password;


	public User(String name, char[] password) {
		this.username = name;
		this.password = password;
	}


	@Override
	public synchronized String getName() {
		return username;
	}

	@Override
	public synchronized void setName(String name) {
		this.username = name;
	}

	@Override
	public synchronized char[] getPassword() {
		return password;
	}

	@Override
	public synchronized void setPassword(char[] password) {
		this.password = password;
	}

	  @Override
	  public UserType getType() {
	    return UserType.USER;
	  }

}
