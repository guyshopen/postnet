package il.co.technion.postnet.Objects;

import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.ISignsDB;
import il.co.technion.postnet.DB.IUsersDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.GUI.IPostNetGUI;
import il.co.technion.postnet.GUI.AddOfficialAdvertiser.AddOfficialAdvertiserException;
import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Objects.Interfaces.ISignsAdmin;
import il.co.technion.postnet.Objects.Interfaces.IUser;
import il.co.technion.postnet.Types.SignContent;
import il.co.technion.postnet.Types.SignType;
import il.co.technion.postnet.Types.UserType;

/**
 * 
 * the class which represents the sign admin account
 *
 */
public class SignsAdmin extends Admin implements ISignsAdmin {
	IUsersDB UserDB;
	ISignsDB SignsDB;
	/**
	 * initialize sign admin
	 * @param name the sign admin name
	 * @param password the sign admin password
	 */
	public SignsAdmin(String name, char[] password) {
		super(name, password);
		UserDB = PostNetDB.getUsersDB();
		SignsDB = PostNetDB.getSignsDB();
	}

	public SignsAdmin(String name, char[] password, IUsersDB userDB,
			ISignsDB signsDB) {
		super(name, password);
		UserDB = userDB;
		this.SignsDB = signsDB;
	}
	 /**
	   * @return UserType.SIGNS_ADMIN
	   */
	@Override
	public UserType getType() {
		return UserType.SIGNS_ADMIN;
	}

	public void addOfficialAdvertiser(String username, char[] password,
			String organization, String account)
			throws AddOfficialAdvertiserException {
		if (UserDB.getUser(username) != null)
			throw new AddOfficialAdvertiserException("Username already exist");
		IUser user = new OfficialAdvertiser(username, password, organization,
				account);
		UserDB.addUser(user);
	}

	public void removeOfficialAdvertiser(String name) {
		UserDB.removeUser(name);
	}

	public void addSign(String name, String city, SignType type,
			SignContent content) {
		ISign sign = new Sign(name, city, type, content,
				PostNetDB.getDefaultDayPrice(),
				PostNetDB.getDefaultNightPrice());
		SignsDB.add(sign);

		for (IUser user : UserDB.getUsers()) {
			if (user instanceof IAdvertiser) {
				((IAdvertiser) user).sendNotification("Sign \""
						+ sign.getName() + "\" has been added in "
						+ sign.getCity() + ".");
			}
		}
	}

	@Override
	public void removeSign(String signName) {
		ISign sign = SignsDB.getSignByName(signName);
		SignsDB.removeSignByName(signName);
		// Cancelling ads and notifying customers
		// Go over all advertisers
		for (IUser user : UserDB.getUsers()) {
			if (user instanceof IAdvertiser) {
				// Go over all active (ongoing) ads of advertiser
				for (IAd ad : ((IAdvertiser) user).getActiveAds()) {

					// Future ads that contain this sign

					if (ad.getSigns().contains(sign)) {

						ad.setNotActive(); // Cancel;
						((IAdvertiser) user).sendNotification("Sign \""
								+ signName + "\" has been removed. Ad \""
								+ ad.getName() + "\" has been removed.");
					}
				}
			}
		}

	}

	public void disableSign(String signName) {
		ISign sign = SignsDB.getSignByName(signName);
		SignsDB.disableSignByName(signName);

		// Cancelling ads and notifying customers

		// Go over all advertisers
		for (IUser user : UserDB.getUsers()) {
			if (user instanceof IAdvertiser) {
				// Go over all active (ongoing) ads of advertiser
				for (IAd ad : ((IAdvertiser) user).getActiveAds()) {

					// Future ads that contain this sign

					if (ad.getSigns().contains(sign)) {

						ad.setNotActive(); // Cancel;
						((IAdvertiser) user).sendNotification("Sign \""
								+ signName + "\" has been disabled. Ad \""
								+ ad.getName() + "\" has been canceled.");
					}
				}
			}
		}
	}
	
	
	public void enableSign(String signName) {
		ISign sign = SignsDB.getSignByName(signName);
		SignsDB.enableSignByName(signName);
	      // Notifying users
	      
				for (IUser user : UserDB.getUsers()) {
					if (user instanceof IAdvertiser) {
						((IAdvertiser) user).sendNotification("Sign \""
								+ sign.getName() + "\" has been enabled in "
								+ sign.getCity() + ".");
					}
				}

	}
}
