package il.co.technion.postnet.Objects;

import il.co.technion.postnet.Maintenance.PostNetMaintenance;
import il.co.technion.postnet.Objects.Interfaces.IPeriodicAd;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Types.AdType;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;

public class PeriodicAd extends Ad implements IPeriodicAd {

	// A list of 2 minute windows in which the ad will show
	ArrayList<Calendar> timesToShow;

	Calendar startDate, startTime, endTime;
	boolean[] days;
	int weeks;
	boolean constant; // Is this a constant official ad

	public PeriodicAd(String name, File file, ArrayList<ISign> signs,
			Calendar startDate, Calendar startTime, Calendar endTime,
			boolean[] days, int weeks, boolean constant) {
		super(name, file, signs);
		this.startDate = startDate;
		this.startTime = startTime;
		this.endTime = endTime;
		this.days = days;
		this.weeks = weeks;
		this.constant = constant;

		this.timesToShow = new ArrayList<Calendar>();
		calcTimesToShow();
	}

	@Override
	public synchronized boolean isConstant() {
		return constant;
	}

	@Override
	public synchronized ArrayList<Calendar> getTimesToShow() {
		return timesToShow;
	}

	@Override
	public boolean[] getDays() {
		return days;
	}

	@Override
	public int getWeeks() {
		return weeks;
	}

	@Override
	public Calendar getStartTime() {
		return startTime;
	}

	@Override
	public Calendar getEndTime() {
		return endTime;
	}

	@Override
	public Calendar getStartDate() {
		return startDate;
	}

	@Override
	public AdType getType() {
		return AdType.PERIODIC;
	}
	
	@Override
	public synchronized boolean isActive() {
		return (active && (Collections.max(timesToShow).after(Calendar.getInstance())));
	}

	// Creates a list of 2 minute windows in which the ad will show
	private void calcTimesToShow() {

		// First 2 minute frame of the periodic ad
		Calendar c = new GregorianCalendar(startDate.get(Calendar.YEAR),
				startDate.get(Calendar.MONTH),
				startDate.get(Calendar.DAY_OF_MONTH),
				startTime.get(Calendar.HOUR_OF_DAY),
				startTime.get(Calendar.MINUTE));

		// First 2 minute frame after the periodic ad is over
		Calendar end = new GregorianCalendar(startDate.get(Calendar.YEAR),
				startDate.get(Calendar.MONTH),
				startDate.get(Calendar.DAY_OF_MONTH),
				endTime.get(Calendar.HOUR_OF_DAY), endTime.get(Calendar.MINUTE));
		end.add(Calendar.DATE, (weeks * 7) - 1);

		// While 'c' is still in the time frame of the ad
		while (c.before(end)) {

			// Check if 'c' in a day in which the ad should show

			if (days[(c.get(Calendar.DAY_OF_WEEK)) - 1] == false) {
				// Ad doesn't show in the same week day as 'c'

				// Move to the next day

				c = new GregorianCalendar(c.get(Calendar.YEAR),
						c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH),
						startTime.get(Calendar.HOUR_OF_DAY),
						startTime.get(Calendar.MINUTE));
				c.add(Calendar.DATE, 1);

				continue;
			}

			// Day matched. Check time of day

			if ((c.get(Calendar.HOUR_OF_DAY) < startTime
					.get(Calendar.HOUR_OF_DAY))
					|| ((c.get(Calendar.HOUR_OF_DAY) == startTime
							.get(Calendar.HOUR_OF_DAY)) && (c
							.get(Calendar.MINUTE) < startTime
							.get(Calendar.MINUTE)))) {
				// 'c' is before the time frame of the ad

				// Move to the start of the frame

				int diff = startTime.get(Calendar.HOUR_OF_DAY)
						- c.get(Calendar.HOUR_OF_DAY);
				diff *= 60;
				diff += startTime.get(Calendar.MINUTE) - c.get(Calendar.MINUTE);

				c.add(Calendar.MINUTE, diff);

				continue;
			}
			if ((c.get(Calendar.HOUR_OF_DAY) > endTime
					.get(Calendar.HOUR_OF_DAY))
					|| ((c.get(Calendar.HOUR_OF_DAY) == endTime
							.get(Calendar.HOUR_OF_DAY)) && (c
							.get(Calendar.MINUTE) >= endTime
							.get(Calendar.MINUTE)))) {
				// 'c is after the time frame of the ad

				// Move to the next day

				c = new GregorianCalendar(c.get(Calendar.YEAR),
						c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH),
						startTime.get(Calendar.HOUR_OF_DAY),
						startTime.get(Calendar.MINUTE));
				c.add(Calendar.DATE, 1);

				continue;
			}

			// Ad should show in 'c'

			timesToShow.add(c);
			c = (Calendar) c.clone();
			c.add(Calendar.MINUTE, 2); // Next time frame
		}
	}
}
