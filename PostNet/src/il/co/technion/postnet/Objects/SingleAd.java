package il.co.technion.postnet.Objects;

import il.co.technion.postnet.Maintenance.PostNetMaintenance;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Objects.Interfaces.ISingleAd;
import il.co.technion.postnet.Types.AdType;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * 
 * Ad that will be show one time for a specipic duration.
 *
 */
public class SingleAd extends Ad implements ISingleAd {

	/**
	 * time to show the ad
	 */
	Calendar timeToShow;
	/**
	 * Is this a constant official ad?
	 */
	boolean constant; 

	/**
	 * initialize singleAd
	 * @param name ad name
	 * @param file is the sign file
	 * @param sign are all the sign the ad applys to
	 * @param timeToShow time to show the ad
	 * @param constant constant official ad?
	 */
	public SingleAd(String name, File file, ArrayList<ISign> signs,
			Calendar timeToShow, boolean constant) {
		super(name, file, signs);
		this.timeToShow = timeToShow;
		this.constant = constant;
	}

	
	
	@Override
	/**
	 * @return is the single ad constant?
	 */
	public synchronized boolean isConstant() {
		return constant;
	}

	@Override
	/**
	 * @return The method returns the time to show of the single ad
	 */
	public synchronized Calendar getTimeToShow() {
		return timeToShow;
	}
	
	@Override
	/**
	 * The method returns the state of the ad (active or inactive)
	 * based on state or if it was shown already.
	 */
	public synchronized boolean isActive() {
		return (active && (timeToShow.after(Calendar.getInstance())));
	}

  @Override
  /**
   * @return SINGLE
   */
  public AdType getType() {
    return AdType.SINGLE;
  }

}
