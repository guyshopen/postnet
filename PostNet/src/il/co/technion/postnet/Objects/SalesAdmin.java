package il.co.technion.postnet.Objects;

import java.util.ArrayList;

import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.ISalesAdmin;
import il.co.technion.postnet.Types.UserType;

/**
 * 
 *a class that represents the sales admin account
 *
 */
public class SalesAdmin extends Admin implements ISalesAdmin {

	/**
	 * initialize sales admin
	 * @param name
	 * @param password
	 */
	public SalesAdmin(String name, char[] password) {
		super(name, password);
	}

	/**
	 * @return user type
	 */
  @Override
  public UserType getType() {
    return UserType.SALES_ADMIN;
  }
  
  public void sendAdvertisersNotification(String notification){
      ArrayList<IAdvertiser> advertisers = PostNetDB.getUsersDB().getAdvertisers();
      for (IAdvertiser advertiser : advertisers)
        advertiser.sendNotification(notification);
  }

}
