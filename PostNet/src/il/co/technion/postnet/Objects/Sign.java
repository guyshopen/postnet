package il.co.technion.postnet.Objects;

import il.co.technion.postnet.Maintenance.PostNetMaintenance;
import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Types.SignContent;
import il.co.technion.postnet.Types.SignType;

import java.awt.EventQueue;
import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 *  This class represents a sign, and is an independent thread
 */
public class Sign implements ISign, Runnable {

	private String name; // Unique identifier
	private String city;
	private SignType type;
	private SignContent content;
	private Float dayPrice;
	private Float nightPrice;

	private boolean active; // Active currently
	private boolean started; // Thread started working
	private boolean dead; // Sign was removed, thread needs to die

	private ArrayList<IAd> newAds; // Ads left to show in the current cycle
	private ArrayList<IAd> newUnexpectedAds; // Unexpected ads to show
	private ArrayList<IAd> oldAds; // Ads shown since last statistics report
	private IAd currentAd; // The ad that is showing currently
	private Calendar lastUpdate;

	public static String defaultAd; // Text message when no ad available
	public static String disabledAd; // Text message when disabled

	private final Thread thread;

	private JFrame frmSign;
	private JLabel lblDisplay;

	/**
	 * initialize singleAd
	 * @param name ad name
	 * @param city is the sign city
	 * @param type if the content on the sign type
	 * @param dayprice is day price
	 * @param nightPrice is night price
	 */
	public Sign(String name, String city, SignType type, SignContent content, Float dayPrice, Float nightPrice) {

		this.name = name; // Unique
		this.city = city;
		this.type = type;
		this.content = content;
		this.dayPrice = dayPrice;
		this.nightPrice = nightPrice;
		this.active = false; // Need to enable() the sign
		this.dead = false;

		newAds = new ArrayList<IAd>();
		newUnexpectedAds = new ArrayList<IAd>();
		oldAds = new ArrayList<IAd>();
		currentAd = null; // Sign is empty
		lastUpdate = null;

		defaultAd = "<html><center>Your ad here:<br>www.PostNet.com</center></html>";
		disabledAd = "<html><center>Sign disabled</center></html>";

		thread = new Thread(this);

		// Sign GUI initialisation:

		frmSign = new JFrame();
		frmSign.setResizable(false);
		frmSign.setTitle("Sign Display: \"" + name + "\"");
		frmSign.setBounds(100, 100, 400, 450);
		frmSign.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frmSign.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(Color.BLACK);
		panel.setBounds(10, 11, 374, 399);
		frmSign.getContentPane().add(panel);
		panel.setLayout(null);

		lblDisplay = new JLabel(defaultAd);
		lblDisplay.setIcon(null);
		lblDisplay.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblDisplay.setHorizontalAlignment(SwingConstants.CENTER);
		lblDisplay.setForeground(Color.WHITE);
		lblDisplay.setBounds(10, 11, 354, 377);
		panel.add(lblDisplay);

	}

	/**
	 * Starts the sign thread
	 */
	private synchronized void start() {

		// Starts the thread

		if (started)
			return;

		// Start the GUI:

		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					frmSign.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		// Start accepting ads:

		started = true;
		thread.start();
	}

	@Override
	/**
	 * Sign thread method
	 */
	public void run() {
		// Main thread method

		while (!dead) {

			while (active) {
				// Ad switching
				if (isTimeToUpdateAd())
					updateAd();
			}

			// Sign disabled
			lblDisplay.setText(disabledAd);
			lblDisplay.setIcon(null);

		}
	}

	/**
	 * @return name
	 */
	@Override
	public synchronized String getName() {
		return name;
	}
	/**
	 * @return city
	 */
	@Override
	public synchronized String getCity() {
		return city;
	}
	/**
	 * @return sign type
	 */
	@Override
	public synchronized SignType getType() {
		return type;
	}

	/**
	 * @return sign content
	 */
	@Override
	public synchronized SignContent getContent() {
		return content;
	}

	/**
	 * @return day price
	 */
	@Override
	public synchronized Float getDayPrice() {
		return dayPrice;
	}

	/**
	 * @param price sets day price
	 */
	@Override
	public synchronized void setDayPrice(Float price) {
		dayPrice = price;

	}
	/**
	 * @return night price
	 */
	@Override
	public synchronized Float getNightPrice() {
		return nightPrice;
	}

	/**
	 * @param price sets night price
	 */
	@Override
	public synchronized void setNightPrice(Float price) {
		nightPrice = price;
	}
	/**
	 * @return true if sign is active
	 */
	@Override
	public synchronized boolean isActive() {
		return active;
	}

	@Override
	/** 
	 * Enables the sign
	 */
	public synchronized void enable() {

		if (active)
			return;

		// No ads will display until a new list is given

		active = true;

		lblDisplay.setText(defaultAd);
		lblDisplay.setIcon(null);

		// Start sign thread for the first time

		if (!started)
			start();
	}

	@Override
	/**
	 * disables the sign
	 */
	public synchronized void disable() {

		// Last ads will not be reported after disabling the sign

		active = false;

		newAds.clear();
		newUnexpectedAds.clear();
		oldAds.clear();
		currentAd = null;
		lastUpdate = null;

		lblDisplay.setText(disabledAd);
		lblDisplay.setIcon(null);
	}

	@Override
	/**
	 *  Gives the sign a new list of non-immediate ads to display
	 *  @param adList list of ads
	 */
	public synchronized void updateAdList(ArrayList<IAd> adList) {

		if (!active)
			return;
		newAds.clear(); // Some ads will not get to be displayed
		newAds.addAll(adList);

	}

	@Override
	
	/**
	 * Gives the sign a new immediate unexpected ad
	 * @param ad
	 */
	public synchronized void pushUnexpactedAd(IAd ad) {
		if (!active)
			return;
		newUnexpectedAds.add(ad);
	}

	@Override
	/**
	 *  @return Returns a report of ads displayed since last report
	 */
	public synchronized HashMap<String, Integer> reportStatistics() {
		if (!active)
			return new HashMap<String, Integer>(); // No report
		HashMap<String, Integer> report = new HashMap<String, Integer>();
		for (IAd ad : oldAds) {
			// Count the number of times an ad has been displayed since
			// the previous report
			if (report.containsKey(ad.getName())) {
				report.put(ad.getName(), report.get(ad.getName()) + 1);
			} else {
				report.put(ad.getName(), 1);
			}
		}
		oldAds.clear(); // Clear for the next report
		return report;
	}

	/**
	 * Switches to a new ad
	 */
	private synchronized void updateAd() {

		// Invoked every 10 seconds when the sign is active

		if (!newUnexpectedAds.isEmpty()) {
			// There are unexpected ads to show
			currentAd = newUnexpectedAds.get(0);
			oldAds.add(currentAd);
			newUnexpectedAds.remove(0);

		} else if (!newAds.isEmpty()) {
			// There aren't any unexpected ads, show regular ads
			currentAd = newAds.get(0);
			oldAds.add(currentAd);
			newAds.remove(0);

		} else {
			// Nothing to show
			currentAd = null;
		}

		// GUI changes:

		if (currentAd == null) {
			lblDisplay.setText(defaultAd);
			lblDisplay.setIcon(null);
		} else {

			// Checking extension

			int i = currentAd.getFileName().lastIndexOf('.');
			if (currentAd.getFileName().substring(i + 1).equals("txt")) {
				lblDisplay.setText(readFile(currentAd.getFileName()));
				lblDisplay.setIcon(null);
			} else if (currentAd.getFileName().substring(i + 1).equals("jpg")) {
				lblDisplay.setText(null);
				lblDisplay.setIcon(new ImageIcon(currentAd.getFileName()));
			}

		}
	}

	@Override
	
	/**
	 *  Disables the sign and kills the thread
	 */
	public synchronized void die() {
		disable();
		frmSign.setVisible(false);
		frmSign.dispose();
		dead = true;
	}

	
	/**
	 * @return Returns true when it's a new 10 seconds cycle
	 */
	private boolean isTimeToUpdateAd() {
		Calendar c = Calendar.getInstance();
		if ((c.get(Calendar.SECOND) % 10 == 0)
				&& PostNetMaintenance.differentTime(c, lastUpdate)) {
			lastUpdate = c; // Timestamp
			return true;
		} else
			return false;
	}

	/**
	 * @return Returns the content of a text file as string
	 */
	public static String readFile(String path) {
		byte[] encoded = null;
		try {
			encoded = Files.readAllBytes(Paths.get(path));
		} catch (IOException e) {
			return "";
		}
		return StandardCharsets.UTF_8.decode(ByteBuffer.wrap(encoded))
				.toString();
	}

	@Override
	public ArrayList<IAd> getUnexpactedAds() {
	return newUnexpectedAds;
	}

}
