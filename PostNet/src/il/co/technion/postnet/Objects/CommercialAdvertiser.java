package il.co.technion.postnet.Objects;

import il.co.technion.postnet.Objects.Interfaces.ICommercialAdvertiser;
import il.co.technion.postnet.Types.UserType;

public class CommercialAdvertiser extends Advertiser implements
		ICommercialAdvertiser {
	
	public CommercialAdvertiser(String name, char[] password, String company, String account) {
		super(name, password, company, account);
	}

  @Override
  public UserType getType() {
    return UserType.COMMERCIAL_ADVERTISER;
  }

}
