package il.co.technion.postnet;

import java.util.ArrayList;

import il.co.technion.postnet.DB.IUsersDB;
import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IOfficialAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IUser;

public class UsersDBStub implements IUsersDB {


	ArrayList<IUser> users;

	public UsersDBStub() {
		users = new ArrayList<>();
	}

	@Override
	public synchronized void addUser(IUser user) {
		users.add(user);
	}

	@Override
	public synchronized IUser getUser(String name) {
		for (int i = 0; i < users.size(); ++i)
			if (users.get(i).getName().equals(name))
				return users.get(i);
		return null;
	}

	@Override
	public synchronized ArrayList<IUser> getUsers() {
		return users;
	}

	@Override
	public synchronized ArrayList<IAd> getAllActiveAds() {
		ArrayList<IAd> adList = new ArrayList<IAd>();
		for (IUser user : users) {
			if (user.getType().isAdmin())
				continue;
			for (IAd ad : ((IAdvertiser) user).getActiveAds())
				adList.add(ad);
		}
		return adList;
	}

	@Override
	public ArrayList<IAdvertiser> getAdvertisers() {
		ArrayList<IAdvertiser> advertisers = new ArrayList<>();
		for (IUser user : users)
			if (user.getType().isAdvertiser())
				advertisers.add((IAdvertiser) user);
		return advertisers;
	}

	@Override
	public ArrayList<IOfficialAdvertiser> getOfficialAdvertisers() {
		ArrayList<IOfficialAdvertiser> officialAdvertisers = new ArrayList<>();
		for (IUser user : users)
			if (user.getType().isOfficialAdvertiser())
				officialAdvertisers.add((IOfficialAdvertiser) user);
		return officialAdvertisers;
	}

	@Override
	public void removeUser(String name) {
		for (int i = 0; i < users.size(); ++i)
			if (users.get(i).getName().equals(name)) {
				users.remove(i);
				return;
			}
	}

}
