package il.co.technion.postnet;

import static org.junit.Assert.*;

import java.util.ArrayList;



import junit.framework.Assert;
import il.co.technion.postnet.DB.IPostNetDB;
import il.co.technion.postnet.DB.IUsersDB;
import il.co.technion.postnet.DB.UsersDB;
import il.co.technion.postnet.GUI.Register.RegisterPageRegisterActionListener;
import il.co.technion.postnet.Objects.Admin;
import il.co.technion.postnet.Objects.CommercialAdvertiser;
import il.co.technion.postnet.Objects.OfficialAdvertiser;
import il.co.technion.postnet.Objects.User;
import il.co.technion.postnet.Objects.Interfaces.IAdmin;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IOfficialAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.IUser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UserDBTest {
	
	@Mock
	private IUsersDB userDB;
	
	@Before
	public void setUp() throws Exception {
		userDB = new UsersDB();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetUser() {
		char[] password = {'1','2','3','4','5','6'};
		User user = new User("omer",password);
		userDB.addUser(user);
		IUser returnedUser = userDB.getUser("omer");
		assertTrue(returnedUser.getName().contentEquals("omer"));
		assertTrue(returnedUser.getPassword().equals(password));
		assertNull(userDB.getUser("rotem"));
	}
	

	@Test
	public void testRemoveUser() {
		char[] password = {'1','2','3','4','5','6'};
		User user = new User("omer",password);
		userDB.addUser(user);
		IUser returnedUser = userDB.getUser("omer");
		assertTrue(returnedUser.getName().contentEquals("omer"));
		assertTrue(returnedUser.getPassword().equals(password));
		userDB.removeUser("omer");
		assertNull(userDB.getUser("omer"));
	}
	
	@Test
	public void testGetAllActiveAds() {
		
	}
	@Test
	public void testGetUsers() {
		char[] password = {'1','2','3','4','5','6'};
		String company = "lintel";
		String account = "account";
		IUser admin = new Admin("Omer", password);
		IUser official = new OfficialAdvertiser("Ran", password,"police",account);
		IUser commercial = new CommercialAdvertiser("Ronen", password,company , account);
		userDB.addUser(official);
		userDB.addUser(commercial);	
		userDB.addUser(admin);	
		ArrayList<IUser> users = userDB.getUsers();
		assertNotNull(users);
		assertTrue(users.size() == 3);
		assertTrue(users.contains(admin));
		assertTrue(users.contains(official));
		assertTrue(users.contains(commercial));
	}
	
	
	@Test
	public void testGetAdvertisers1() {
		assertNotNull(userDB.getAdvertisers());
		assertTrue(userDB.getAdvertisers().size() == 0);
		char[] password = {'1','2','3','4','5','6'};
		IUser admin = new Admin("Ran", password);
		userDB.addUser(admin);
		assertTrue(userDB.getAdvertisers().size() == 0);
	}
	
	@Test
	public void testgGetAdvertisers2() {
		assertNotNull(userDB.getAdvertisers());
		assertTrue(userDB.getAdvertisers().size() == 0);
		char[] password = {'1','2','3','4','5','6'};
		String company = "lintel";
		String account = "account";
		IUser admin = new Admin("Omer", password);
		userDB.addUser(admin);
		IUser official = new OfficialAdvertiser("Ran", password,"police",account);
		IUser commercial = new CommercialAdvertiser("Ronen", password,company , account);
		userDB.addUser(official);
		userDB.addUser(commercial);
		ArrayList<IAdvertiser> advertisers = userDB.getAdvertisers();
		assertTrue(advertisers.size() == 2);
		assertTrue(advertisers.contains(official));
		assertTrue(advertisers.contains(commercial));
		assertFalse(advertisers.contains(admin));	
	}
	
	public void testGetOfficialAdvertisers(){
		assertNotNull(userDB.getOfficialAdvertisers());
		assertTrue(userDB.getAdvertisers().size() == 0);
		char[] password = {'1','2','3','4','5','6'};
		String company = "lintel";
		String account = "account";
		IUser admin = new Admin("Omer", password);
		userDB.addUser(admin);
		IUser official = new OfficialAdvertiser("Ran", password,"police",account);
		IUser commercial = new CommercialAdvertiser("Ronen", password,company , account);
		userDB.addUser(official);
		userDB.addUser(commercial);
		ArrayList<IOfficialAdvertiser> advertisers = userDB.getOfficialAdvertisers();
		assertTrue(advertisers.size() == 1);
		assertTrue(advertisers.contains(official));
		assertFalse(advertisers.contains(commercial));
		assertFalse(advertisers.contains(admin));
	}

}
