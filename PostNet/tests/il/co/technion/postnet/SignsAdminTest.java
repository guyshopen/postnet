package il.co.technion.postnet;
import static org.junit.Assert.*;
import junit.framework.AssertionFailedError;
import il.co.technion.postnet.DB.ISignsDB;
import il.co.technion.postnet.DB.IUsersDB;
import il.co.technion.postnet.DB.UsersDB;
import il.co.technion.postnet.GUI.AddOfficialAdvertiser.AddOfficialAdvertiserException;
import il.co.technion.postnet.Objects.OfficialAdvertiser;
import il.co.technion.postnet.Objects.SignsAdmin;
import il.co.technion.postnet.Objects.Interfaces.IOfficialAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Objects.Interfaces.ISignsAdmin;
import il.co.technion.postnet.Types.SignContent;
import il.co.technion.postnet.Types.SignType;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.junit.After;
import org.junit.Before;

public class SignsAdminTest {
	ISignsAdmin signAdmin;
	ISign signTest1;
	ISign signTest2;
	IUsersDB userDB;
	ISignsDB signsDB;
	
	@Before
	public void setUp() throws Exception {
		userDB = new UsersDBStub();
		signsDB = new SignsDBStub();
		signAdmin = new SignsAdmin("admin",new char[]{'1'},userDB , signsDB);
	}
	@After
	public void tearDown() throws Exception {
		signAdmin.removeOfficialAdvertiser("police");
		signAdmin.removeSign("signTest1");
	}
	
	@Test
	public void testAddOficialAdvertiser() throws AddOfficialAdvertiserException{
		signAdmin.addOfficialAdvertiser("police",new char[]{'1'} , "police", "police");
		boolean found = false;
		for (IOfficialAdvertiser official: userDB.getOfficialAdvertisers())
			if (official.getName().contentEquals("police"))
				found = true;
		assertTrue(found);
		try{
			signAdmin.addOfficialAdvertiser("police",new char[]{'1'} , "police", "police");
			throw new AssertionFailedError();
		}catch(AddOfficialAdvertiserException e){
			//o.k
		}
	}
	
	@Test
	public void testRemoveOfficialAdvertiser() throws AddOfficialAdvertiserException{
		//testAddOficialAdvertiser ensure police was inserted to database
		signAdmin.addOfficialAdvertiser("police",new char[]{'1'} , "police", "police");
		signAdmin.removeOfficialAdvertiser("police");
		boolean found = false;
		for (IOfficialAdvertiser official: userDB.getOfficialAdvertisers())
			if (official.getName().contentEquals("police"))
				found = true;
		assertFalse(found);
	}
	@Test
	 public void testAddSign(){
		//the sign "signTest1" not exist in the signs database
		assertNull(signsDB.getSignByName("signTest1"));
		signAdmin.addSign("signTest1", "Holon", SignType.CITY,
				SignContent.TEXT);
		assertNotNull(signsDB.getSignByName("signTest1"));
	}
	
	@Test
	 public void testRemoveSign(){
		signAdmin.removeSign("signTest1"); //not causing any exception
		signAdmin.addSign("signTest1", "Holon", SignType.CITY,
				SignContent.TEXT);
		signAdmin.removeSign("signTest1");
		assertNull(signsDB.getSignByName("signTest1"));
		
	}
	@Test 
	public void testdisableSign(){
		signAdmin.addSign("signTest1", "Holon", SignType.CITY,
				SignContent.TEXT);
		ISign sign = signsDB.getSignByName("signTest1");
		assertFalse(sign.isActive());
		sign.enable();
		sign = signsDB.getSignByName("signTest1");
		assertTrue(sign.isActive());
		signAdmin.disableSign("signTest1");
		sign = signsDB.getSignByName("signTest1");
		assertFalse(sign.isActive());	
	 }
	
	@Test 
	public void testEnableSign(){
		signAdmin.addSign("signTest1", "Holon", SignType.CITY,
				SignContent.TEXT);
		ISign sign = signsDB.getSignByName("signTest1");
		assertFalse(sign.isActive());
		signAdmin.enableSign("signTest1");
		assertTrue(sign.isActive());
	 }
}
