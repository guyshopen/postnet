package il.co.technion.postnet;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import il.co.technion.postnet.DB.ISignsDB;
import il.co.technion.postnet.DB.IUsersDB;
import il.co.technion.postnet.DB.PostNetDB;
import il.co.technion.postnet.Maintenance.IPostNetMaintenance;
import il.co.technion.postnet.Maintenance.PostNetMaintenance;
import il.co.technion.postnet.Objects.Ad;
import il.co.technion.postnet.Objects.Advertiser;
import il.co.technion.postnet.Objects.Sign;
import il.co.technion.postnet.Objects.SingleAd;
import il.co.technion.postnet.Objects.UnexpectedAd;
import il.co.technion.postnet.Objects.User;
import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.IAdvertiser;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Objects.Interfaces.ISingleAd;
import il.co.technion.postnet.Objects.Interfaces.IUnexpectedAd;
import il.co.technion.postnet.Objects.Interfaces.IUser;
import il.co.technion.postnet.Types.SignContent;
import il.co.technion.postnet.Types.SignType;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PostNetMaintenanceTest {
	ISign sign;
	IUnexpectedAd unExpAd;
	ISingleAd sinAd;
	IAd sin1Ad;
	ISignsDB signsDB;
	IUsersDB usersDB;
	IPostNetMaintenance maintenance;
	IAdvertiser user;
	ArrayList<ISign> adSign;
	Calendar cal1;

	@Before
	public void setUp() {

		usersDB = new UsersDBStub();
		signsDB = new SignsDBStub();
		cal1 = new GregorianCalendar(2014, 04, 01);
		sign = new SignStub("myTestSign", "holon", SignType.CITY,
				SignContent.TEXT, new Float(100), new Float(50));
		adSign = new ArrayList<ISign>();
		user = new Advertiser("Rotem", new char[] { '1' }, "company", "account");
		sinAd = new SingleAd("secondAd", new File("file"), adSign, cal1, true);
		sin1Ad = new Ad("secondAd", new File("file"), adSign);
		unExpAd = new UnexpectedAd("firstAd", new File("file"), adSign);
		maintenance = new PostNetMaintenance(new PostNetDB(signsDB, usersDB),
				usersDB);

	}

	@After
	public void tearDown() {

	}

	@Test
	public void testhandleUnexpectedAds() {
		sign.enable();
		adSign.add(sign);
		user.addAd(sin1Ad);
		user.addAd(unExpAd);
		usersDB.addUser(user);
		assertTrue(unExpAd.isActive());
		maintenance.handleUnexpectedAds();
		assertFalse(unExpAd.isActive());
		assertTrue(sign.getUnexpactedAds().contains(unExpAd));
		assertFalse(sign.getUnexpactedAds().contains(sinAd));
	}

	@Test
	public void testGetAdList() {
		sign.enable();
		sinAd.setActive();
		adSign.add(sign);
		user.addAd(sinAd);
		user.addAd(unExpAd);
		usersDB.addUser(user);
		assertFalse(PostNetMaintenance.differentTime(cal1,
				sinAd.getTimeToShow()));
		ArrayList<IAd> list = maintenance.getAdList(sign, cal1);
		assertTrue(list.contains(sinAd));
		assertFalse(list.contains(unExpAd));
	}

}
