package il.co.technion.postnet;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import il.co.technion.postnet.Objects.Interfaces.IAd;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Types.SignContent;
import il.co.technion.postnet.Types.SignType;

public class SignStub implements ISign {
	private String name; // Unique identifier
	private String city;
	private SignType type;
	private SignContent content;
	private Float dayPrice;
	private Float nightPrice;

	private boolean active; // Active currently
	private boolean started; // Thread started working
	private boolean dead; // Sign was removed, thread needs to die

	private ArrayList<IAd> newAds; // Ads left to show in the current cycle
	private ArrayList<IAd> newUnexpectedAds; // Unexpected ads to show
	private ArrayList<IAd> oldAds; // Ads shown since last statistics report
	private IAd currentAd; // The ad that is showing currently
	private Calendar lastUpdate;

	public static String defaultAd; // Text message when no ad available
	public static String disabledAd; // Text message when disabled

	public SignStub(String name, String city, SignType type,
			SignContent content, Float dayPrice, Float nightPrice) {
		this.name = name; // Unique
		this.city = city;
		this.type = type;
		this.content = content;
		this.dayPrice = dayPrice;
		this.nightPrice = nightPrice;
		this.active = false; // Need to enable() the sign
		this.dead = false;

		newAds = new ArrayList<IAd>();
		newUnexpectedAds = new ArrayList<IAd>();
		oldAds = new ArrayList<IAd>();
		currentAd = null; // Sign is empty
		lastUpdate = null;
	}

	@Override
	public synchronized String getName() {
		return name;
	}

	@Override
	public synchronized String getCity() {
		return city;
	}

	@Override
	public synchronized SignType getType() {
		return type;
	}

	@Override
	public synchronized SignContent getContent() {
		return content;
	}

	@Override
	public synchronized Float getDayPrice() {
		return dayPrice;
	}

	@Override
	public synchronized void setDayPrice(Float price) {
		dayPrice = price;

	}

	@Override
	public synchronized Float getNightPrice() {
		return nightPrice;
	}

	@Override
	public synchronized void setNightPrice(Float price) {
		nightPrice = price;
	}

	@Override
	public synchronized boolean isActive() {
		return active;
	}

	@Override
	public void enable() {
	active = true;

	}

	@Override
	public void disable() {
		// Last ads will not be reported after disabling the sign

		active = false;

		newAds.clear();
		newUnexpectedAds.clear();
		oldAds.clear();
		currentAd = null;
		lastUpdate = null;

	}

	@Override
	public void updateAdList(ArrayList<IAd> adList) {
		if (!active)
			return;
		newAds.clear(); // Some ads will not get to be displayed
		newAds.addAll(adList);

	}

	@Override
	public void pushUnexpactedAd(IAd ad) {
		if (!active)
			return;
		newUnexpectedAds.add(ad);
	}

	@Override
	public HashMap<String, Integer> reportStatistics() {
		if (!active)
			return new HashMap<String, Integer>(); // No report
		HashMap<String, Integer> report = new HashMap<String, Integer>();
		for (IAd ad : oldAds) {
			// Count the number of times an ad has been displayed since
			// the previous report
			if (report.containsKey(ad.getName())) {
				report.put(ad.getName(), report.get(ad.getName()) + 1);
			} else {
				report.put(ad.getName(), 1);
			}
		}
		oldAds.clear(); // Clear for the next report
		return report;
	}

	@Override
	public void die() {
		disable();
		dead = true;

	}
	@Override
	public ArrayList<IAd> getUnexpactedAds() {
	return newUnexpectedAds;
	}
}
