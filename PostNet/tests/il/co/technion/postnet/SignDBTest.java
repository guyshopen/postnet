package il.co.technion.postnet;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.TreeSet;

import il.co.technion.postnet.DB.ISignsDB;
import il.co.technion.postnet.DB.IUsersDB;
import il.co.technion.postnet.DB.SignsDB;
import il.co.technion.postnet.Objects.Interfaces.ISign;
import il.co.technion.postnet.Types.SignContent;
import il.co.technion.postnet.Types.SignType;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SignDBTest {
	private ISignsDB signDB;
	ISign signTest1;
	ISign signTest2;
	int alreadExistSigns;
	int alreadyActiveSigns;

	@Before
	public void setUp() throws Exception {
		signDB = new SignsDB();
		alreadExistSigns = signDB.getSigns().size();
		alreadyActiveSigns = signDB.getActiveSigns().size();
		signTest1 = new SignStub("signTest1", "Holon", SignType.CITY,
				SignContent.TEXT, new Float(20), new Float(10));
		signTest2 = new SignStub("signTest2", "RamatGan", SignType.CITY,
				SignContent.TEXT, new Float(25), new Float(15));
	}

	@After
	public void tearDown() throws Exception {
		signDB.removeSignByName("signTest1");
		signDB.removeSignByName("signTest2");

	}

	@Test
	public void testAddSigns() {
		ArrayList<ISign> signs = signDB.getSigns();
		assertNotNull(signs);
		assertTrue(signs.size() == alreadExistSigns);
		signDB.add(signTest1);
		signDB.add(signTest2);
		signs = signDB.getSigns();
		assertNotNull(signs);
		assertTrue(signs.size() == alreadExistSigns + 2);
		assertTrue(signs.contains(signTest1));
		assertTrue(signs.contains(signTest2));
	}

	@Test
	public void testGetActiveSigns() {
		ArrayList<ISign> signs = signDB.getActiveSigns();
		assertNotNull(signs);
		assertEquals(signs.size(), alreadyActiveSigns);
		// sign active
		signDB.add(signTest1);
		// sign active
		signDB.add(signTest2);
		signs = signDB.getActiveSigns();
		assertNotNull(signs);
		assertEquals(signs.size(), alreadyActiveSigns + 2);
		assertTrue(signs.contains(signTest1));
		assertTrue(signs.contains(signTest2));
	}

	@Test
	public void testGetActiveSigns2() {
		ArrayList<ISign> signs = signDB.getActiveSigns();
		// sign still not active
		signDB.add(signTest1);
		signs = signDB.getActiveSigns();
		assertNotNull(signs);
		assertTrue(signs.size() == alreadyActiveSigns + 1);
		assertTrue(signs.contains(signTest1));
		// disable the sign
		signTest1.disable();
		signs = signDB.getActiveSigns();
		assertNotNull(signs);
		assertTrue(signs.size() == alreadyActiveSigns);
		assertTrue(!signs.contains(signTest1));
	}

	@Test
	public void testGetNotActiveSigns() {
		ArrayList<ISign> signs = signDB.getNotActiveSigns();
		assertEquals(signs.size(), alreadExistSigns - alreadyActiveSigns);
		// signTest1 is Active!
		signDB.add(signTest1);
		signs = signDB.getNotActiveSigns();
		// the number of non active sign should stay the same!
		assertEquals(signs.size(), alreadExistSigns - alreadyActiveSigns);
	}

	@Test
	public void testGetNotActiveSigns2() {
		ArrayList<ISign> signs = signDB.getNotActiveSigns();
		// signTest1 is Active!
		signDB.add(signTest1);
		signDB.disableSignByName("signTest1");
		signs = signDB.getNotActiveSigns();
		// the number of non active sign should stay the same!
		assertEquals(signs.size(), alreadExistSigns - alreadyActiveSigns + 1);
		assertTrue(signs.contains(signTest1));
	}

	@Test
	public void testGetSignByName() {
		ISign emptySign = signDB.getSignByName("signTest1");
		assertNull(emptySign);
		signDB.add(signTest1);
		ISign sign = signDB.getSignByName("signTest1");
		assertNotNull(sign);
		assertEquals(signTest1, sign);
	}

	@Test
	public void testDisableSignByName() {
		signDB.add(signTest1);
		ArrayList<ISign> signs = signDB.getActiveSigns();
		// signTest1 enable in the database
		assertTrue(signs.contains(signTest1));
		signDB.disableSignByName("signTest1");
		signs = signDB.getActiveSigns();
		assertTrue(!signs.contains(signTest1));
	}

	@Test
	public void testEnableSignByName() {
		signDB.add(signTest1);
		// Test testDisableSignByName ensure signTest1 disable
		signDB.disableSignByName("signTest1");
		signDB.enableSignByName("signTest1");
		ArrayList<ISign> signs = signDB.getActiveSigns();
		// signTest1 enable in the database
		assertTrue(signs.contains(signTest1));
	}

	@Test
	public void testRemoveSignByName() {
		// test AddSign ensure the signTest1 was added to the database
		signDB.add(signTest1);
		signDB.removeSignByName("signTest1");
		ArrayList<ISign> signs = signDB.getSigns();
		assertTrue(!signs.contains(signTest1));
	}

	// try to remove non existing sign will not do anything
	@Test
	public void testRemoveSignByName2() {
		signDB.removeSignByName("signTest1");
		ArrayList<ISign> signs = signDB.getSigns();
		assertTrue(!signs.contains(signTest1));
	}

	@Test
	public void testGetCities() {
		signDB.add(signTest1);
		signDB.add(signTest2);
		TreeSet<String> signs = signDB.getCities();
		assertTrue(signs.contains(signTest1.getCity()));
		assertTrue(signs.contains(signTest2.getCity()));
	}

}
