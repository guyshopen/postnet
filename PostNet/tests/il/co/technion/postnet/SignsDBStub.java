package il.co.technion.postnet;

import java.util.ArrayList;
import java.util.TreeSet;

import il.co.technion.postnet.DB.ISignsDB;
import il.co.technion.postnet.Objects.Interfaces.ISign;

public class SignsDBStub implements ISignsDB {
	ArrayList<ISign> signs; // All signs

	public SignsDBStub() {
		signs = new ArrayList<ISign>();
	}

	@Override
	// Returns all signs in the DB
	public synchronized ArrayList<ISign> getSigns() {
		return signs;
	}

	@Override
	// Returns only the active signs in the DB
	public synchronized ArrayList<ISign> getActiveSigns() {
		ArrayList<ISign> activeSigns = new ArrayList<>();
		for (ISign sign : signs)
			if (sign.isActive())
				activeSigns.add(sign);
		return activeSigns;
	}

	@Override
	public ISign getSignByName(String name) {
		for (int i = 0; i < signs.size(); ++i)
			if (signs.get(i).getName().equals(name))
				return signs.get(i);
		return null;
	}

	@Override
	public void enableSignByName(String name) {
		for (int i = 0; i < signs.size(); ++i)
			if (signs.get(i).getName().equals(name))
				signs.get(i).enable();

	}

	@Override
	public void disableSignByName(String name) {
		for (int i = 0; i < signs.size(); ++i)
			if (signs.get(i).getName().equals(name))
				signs.get(i).disable();
	}

	@Override
	public void removeSignByName(String name) {
		for (int i = 0; i < signs.size(); ++i)
			if (signs.get(i).getName().equals(name))
				signs.remove(i);

	}

	@Override
	public void setDayPriceByName(String name, Float price) {
		for (int i = 0; i < signs.size(); ++i)
			if (signs.get(i).getName().equals(name))
				signs.get(i).setDayPrice(price);

	}

	@Override
	public void setNightPriceByName(String name, Float price) {
		for (int i = 0; i < signs.size(); ++i)
			if (signs.get(i).getName().equals(name))
				signs.get(i).setNightPrice(price);

	}
	
	@Override
	// Returns all of the cities that have active signs in them
	public synchronized TreeSet<String> getCities() {
		TreeSet<String> cities = new TreeSet<String>();
		for (ISign sign : getActiveSigns()) {
			cities.add(sign.getCity());
		}
		return cities;
	}

	@Override
	public void add(ISign sign) {
		signs.add(sign);

	}

	@Override
	public ArrayList<ISign> getNotActiveSigns() {
		ArrayList<ISign> notActiveSigns = new ArrayList<>();
		for (ISign sign : signs)
			if (!sign.isActive())
				notActiveSigns.add(sign);
		return notActiveSigns;
	}

}
