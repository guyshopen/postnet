package il.co.technion.postnet;

import static org.junit.Assert.*;
import junit.framework.AssertionFailedError;
import il.co.technion.postnet.DB.ISignsDB;
import il.co.technion.postnet.DB.IUsersDB;
import il.co.technion.postnet.DB.UsersDB;
import il.co.technion.postnet.GUI.Register.RegisterException;
import il.co.technion.postnet.GUI.Welcome.WelcomeException;
import il.co.technion.postnet.Objects.MainComputer;
import il.co.technion.postnet.Objects.User;
import il.co.technion.postnet.Objects.Interfaces.IMainComputer;
import il.co.technion.postnet.Objects.Interfaces.IUser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MainComputerTest {
	IUsersDB userDB;
	ISignsDB signsDB;
	IMainComputer main;
	IUser user;

	@Before
	public void setUp() throws Exception {
		userDB = new UsersDBStub();
		signsDB = new SignsDBStub();
		main = new MainComputer(userDB, signsDB);
		user = new User("Rotem", new char[] { '1' });
	}

	@After
	public void tearDown() throws Exception {
	}

	// Login with username that not exist in the database
	@Test
	public void testLogin() {
		// Rotem doesn't exist in the database
		assertNull(userDB.getUser("Rotem"));
		try {
			main.login("Rotem", new char[] { '1' });
			throw new AssertionFailedError("login with not existing username");
		} catch (WelcomeException e) {
			assertEquals(e.getMessage(), "Bad username!");
			// O.K!
		}
	}

	// test Login with non valid password
	@Test
	public void testLogin2() {
		userDB.addUser(user);
		try {
			main.login("Rotem", new char[] { '1', '2' });
			throw new AssertionFailedError("login with not the user password");
		} catch (WelcomeException e) {
			assertEquals(e.getMessage(), "Bad password!");
			// O.K!
		}
	}

	@Test
	public void testLogin3() throws WelcomeException {
		userDB.addUser(user);
		IUser logedUser = main.login("Rotem", new char[] { '1' });
		assertEquals(logedUser, user);
	}

	// try to register with username that already exist
	@Test
	public void testRegister() {
		userDB.addUser(user);
		try {
			main.register("Rotem", new char[] { '1', '2' }, "lintel", "account");
			throw new AssertionFailedError(
					"register with already existing username!");
		} catch (RegisterException e) {
			assertEquals(e.getMessage(), "Username already exist");
		}
	}

	@Test
	public void testRegister2() throws RegisterException {
		assertNull(userDB.getUser("Rotem"));
		main.register("Rotem", new char[] { '1', '2' }, "lintel", "account");
		assertNotNull(userDB.getUser("Rotem"));
		assertEquals(main.getCurrentUser().getName(),"Rotem");
	}

}
